'use strict';

import Component from 'js/components/Component';

class ListDefault extends Component {
    constructor(node) {
        super(node);
    }

    _getItems() {
        return [...this.node.children];
    }

    getCurrentData() {
        return this._getItems().map((item, idx) => {
            const input = item.querySelector('input');

            return {
                name: input.getAttribute('name') || input.getAttribute('data-name'),
                value: input.value,
                text: item.textContent
            };
        });
    }

    getNode() {
        return this.node;
    }

    reset() {
        this.node.innerHTML = '';
    }
}

export default ListDefault;
