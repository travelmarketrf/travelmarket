'use strict';

import Layout from 'js/helpers/Layout';

class ProfileMenu {
    constructor(node) {
        this.node = node;

        if (!this.node) return;

        this.node['_component'] = this;

        this.onClick = this.onClick.bind(this);

        this.node.addEventListener('click', this.onClick, false);
    }

    onClick(e) {
        const item = e.target.closest('.profile-menu__group');

        if (!item) return;

        const parent = item.parentNode;

        if (this.isOpen(parent)) {
            this.close(parent);
        } else {
            this.open(parent);
        }
    }

    isOpen(parent) {
        return parent.classList.contains('profile-menu__item_selected');
    }

    close(parent) {
        parent.classList.remove('profile-menu__item_selected');
    }

    open(parent) {
        parent.classList.add('profile-menu__item_selected');
    }
}

export default ProfileMenu;
