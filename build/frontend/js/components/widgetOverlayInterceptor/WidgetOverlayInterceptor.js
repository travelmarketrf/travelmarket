'use strict';

import Layout from 'js/helpers/Layout';

/**
 * Class serves for intercepting the events from
 * the ui widgets that show a popup in their own container
 * so that the popup can overlay other widgets by increasing
 * z-index for certain parents of the intercepted widget.
 *
 * @class
 */
class WidgetOverlayInterceptor {
    constructor() {
        this.onCaneldarRangeOpen = this.onCaneldarRangeOpen.bind(this);
        this.onCaneldarRangeClose = this.onCaneldarRangeClose.bind(this);

        Layout.body.addEventListener('calendar-range:open', this.onCaneldarRangeOpen, false);
        Layout.body.addEventListener('calendar-range:close', this.onCaneldarRangeClose, false);
        Layout.body.addEventListener('calendar-single:open', this.onCaneldarRangeOpen, false);
        Layout.body.addEventListener('calendar-single:close', this.onCaneldarRangeClose, false);
    }

    onCaneldarRangeOpen(e) {
        const node = this.getNodeGroup(e.detail.component.getNode());

        if (!node) return;

        this.setOverlay(node);
    }

    onCaneldarRangeClose(e) {
        const node = this.getNodeGroup(e.detail.component.getNode());

        if (!node) return;

        this.disableOverlay(node);
    }

    getNodeGroup(element) {
        return element.closest('[data-behavior="overlay"]');
    }

    setOverlay(node) {
        node.classList.add('widget-open');
    }

    disableOverlay(node) {
        node.classList.remove('widget-open');
    }
}

export default WidgetOverlayInterceptor;
