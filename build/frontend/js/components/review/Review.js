'use strict';

import MD from 'js/helpers/MD';
import Layout from 'js/helpers/Layout';

class Review {
    constructor(node) {
        this.node = node;

        if (!this.node) return;

        this.node['_component'] = this;

        this.contentNode = this.node.querySelector('.review__body');
        this.headerNode = this.node.querySelector('.review__header');

        this._bindEvents();
        this.onResize();
    }

    _bindEvents() {
        this.onResize = this.onResize.bind(this);

        Layout.body.addEventListener('tab:change', this.onResize, false);
        window.addEventListener('resize', this.onResize, false);
    }

    getText() {
        return this.contentNode.innerHTML;
    }

    getImage() {
        return this.headerNode.querySelector('.review__img').getAttribute('src');
    }

    getDate() {
        return this.headerNode.querySelector('.review__date').innerHTML;
    }

    getName() {
        return this.headerNode.querySelector('.review__name').innerHTML;
    }

    onResize() {
        this.isOverflowed() ? this._showMoreBtn() : this._hideMoreBtn();

        if (MD.phone()) {
            this.node.setAttribute('data-action', 'review-popup');
        }
    }

    _showMoreBtn() {
        this.node.classList.add('review_overflow');
    }

    _hideMoreBtn() {
        this.node.classList.remove('review_overflow');
    }

    isOverflowed() {
        return this.contentNode.scrollHeight > this.contentNode.offsetHeight;
    }
}

export default Review;
