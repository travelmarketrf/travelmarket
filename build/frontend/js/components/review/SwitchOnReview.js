'use strict';

import Layout from 'js/components/tabs/Tabs';
import Widget from 'js/components/Widget';

class SwitchOnReview {
    constructor(node) {
        this.node = node;

        if (!this.node) return;

        this.node['_component'] = this;

        Widget.onReady([document.querySelector('[data-widget="tabs"]')]).then(widgets => this._init(widgets));
        this.reviewsNode = document.querySelector('.reviews-grid');
        this.review = this.reviewsNode.querySelector('.review');

        this.onClick = this.onClick.bind(this);

        this.node.addEventListener('click', this.onClick, false);
    }

    _init(widgets) {
        this.tabs = widgets[0];
        this.tabsNode = this.tabs.getNode();
    }

    onClick() {
        const anchorTab = this.tabsNode.querySelector('[data-anchor="reviews"]');
        const tabIndex = this.tabs.getTabIndex(anchorTab);

        this.tabs.unsetActiveTab();
        this.tabs.setActiveTab(anchorTab, tabIndex);

        window.scrollTo(0, this.reviewsNode.offsetTop + this.review.offsetHeight);
    }
}

export default SwitchOnReview;
