'use strict';

import Inputmask from 'Inputmask';
import BaseMask from 'js/components/mask/BaseMask';

class TimeMask extends BaseMask {
    constructor(node) {
        super(node);

        this._init();
    }

    /**
     * @inheritdoc
     */
    _init() {
        this.inputmask = Inputmask({
            placeholder: '--:--',
            insertMode: false,
            showMaskOnHover: false,
            regex: '([0-1][0-9]|2[0-3]):([0-5][0-9])'
        }).mask(this.node);
    }
}

export default TimeMask;
