'use strict';

import TimeMask from 'js/components/mask/TimeMask';
import PhoneMask from 'js/components/mask/PhoneMask';

export default {
    'time': TimeMask,
    'phone': PhoneMask
};
