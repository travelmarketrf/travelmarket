'use strict';

import Stringz from 'js/Helpers/Stringz';
import Component from 'js/components/Component';

/**
 * Base class for the mask widget.
 * 
 * @class
 * 
 * @abstract
 */
class BaseMask extends Component {
    constructor(node) {
        super(node);
    }

    /**
     * Initialize mask.
     * 
     * @abstract
     */
    _init() {}

    /**
     * Whether entered value is valid.
     * 
     * @returns {boolean}
     */
    isValid() {
        return this.inputmask.isValid();
    }

    /**
     * Get the input name attribute.
     * 
     * @returns {string} input name.
     */
    getName() {
        return this.node.getAttribute('name') || this.node.getAttribute('data-name');
    }

    /**
     * Get the input value.
     * 
     * @returns {string} input value.
     */
    getValue() {
        return this.node.value;
    }

    /**
     * Get the label value.
     * 
     * @returns {string} label value.
     */
    getLabelValue() {
        return this.node.getAttribute('placeholder');
    }

    /**
     * Get the selected data.
     * 
     * @returns {object} selected data.
     */
    getCurrentData() {
        return [{
            name: this.getName(),
            value: Stringz.stripTags( this.getValue() ),
            text: Stringz.stripTags( this.getValue() )
        }];
    }

    /**
     * Get the component node.
     * 
     * @returns {HTMLElement} root node.
     */
    getNode() {
        return this.node;
    }

    /**
     * Whether mask is not empty.
     * 
     * @returns {boolean}
     */
    isActive() {
        return this.node.value !== '';
    }

    /**
     * Clear the input value.
     */
    reset() {
        this.node.classList.remove('form-input_filled');
        this.node.value = '';
    }
}

export default BaseMask;
