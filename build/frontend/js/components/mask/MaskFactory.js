'use strict';

import masks from 'js/components/mask/masks';

class MaskFactory {
    static create(node) {
        const maskType = node.getAttribute('data-type');

        if (!maskType) return;

        if (MaskFactory.masks[maskType])
            return new MaskFactory.masks[maskType](node);
    }
}

MaskFactory.masks = masks;

export default MaskFactory;
