'use strict';

import Inputmask from 'Inputmask';
import BaseMask from 'js/components/mask/BaseMask';

class PhoneMask extends BaseMask {
    constructor(node) {
        super(node);

        this._init();
    }

    /**
     * @inheritdoc
     */
    _init() {
        this.inputmask = Inputmask('+9 (999) 999-99-99', {
            insertMode: false,
            showMaskOnHover: false
        }).mask(this.node);
    }
}

export default PhoneMask;
