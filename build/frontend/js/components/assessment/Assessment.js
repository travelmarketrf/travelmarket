'use strict';

class Assessment {
    constructor(node) {
        this.node = node;

        if (!this.node) return;

        this.node['_component'] = this;

        this.input = this.node.querySelector('input[type="hidden"]');

        this.onClick = this.onClick.bind(this);

        this.node.addEventListener('click', this.onClick, false);
    }

    onClick(e) {
        const item = e.target.closest('.assessment__item');

        if (!item) return;

        const value = this.getValue(item);

        this.unsetActiveItem();
        this.setActiveItem(item);
        this.setInputValue(value);
    }

    getValue(item) {
        return item.getAttribute('data-value');
    }

    unsetActiveItem() {
        [...this.node.querySelectorAll('.assessment__item')]
            .forEach(item => item.classList.remove('assessment__item_active'));
    }

    setActiveItem(item) {
        item.classList.add('assessment__item_active');
    }

    setInputValue(value) {
        this.input.value = value;
    }
}

export default Assessment;
