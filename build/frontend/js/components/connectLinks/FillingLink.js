'use strict';

class FillingLink {
    constructor(node) {
        this.node = node;

        if (!this.node) return;

        this.node['_component'] = this;

        this.linkLabel = this.node.querySelector('.connect-links__label');
        this.linkField = this.node.querySelector('[data-link="field"] > input[type="text"]');

        this.onInput = this.onInput.bind(this);

        this.linkField.addEventListener('input', this.onInput, false);
    }

    onInput() {
        this.linkLabel.textContent = this.linkField.value;
    }
}

export default FillingLink;
