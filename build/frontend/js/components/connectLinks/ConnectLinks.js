'use strict';

import Cookie from 'js/helpers/Cookie';
import Layout from 'js/helpers/Layout';
import EventHelper from 'js/helpers/EventHelper';

class ConnectLinks {
    constructor(node) {
        this.node = node;

        if (!this.node) return;

        this.node['_component'] = this;

        this.connectedLink = [...document.querySelectorAll('.connect-links__connect_complete')];
        this.addButton = this.node.querySelector('.connect-links__add-link');
        this.fieldsGroup = this.node.querySelector('.connect-links__fields-group');

        this.onAddButtonClick = this.onAddButtonClick.bind(this);

        this.addButton.addEventListener('click', this.onAddButtonClick, false);
    }

    onAddButtonClick() {
        this.generateFields();
    }

    generateFields() {
        const content = `
            <div class="connect-links__group">
                <div class="connect-links__icon">
                    <svg width="30" height="30" viewBox="0 0 30 30" fill="none" xmlns="http://www.w3.org/2000/svg">
                        <circle cx="15" cy="15" r="15" fill="url(#paint0_linear_01)" />
                        <path
                            d="M16.0572 13.2315C14.8857 12.0599 12.9862 12.0599 11.8146 13.2315L8.27907 16.767C7.1075 17.9386 7.1075 19.8381 8.27907 21.0097C9.45064 22.1812 11.3501 22.1812 12.5217 21.0097L13.2288 20.3026C13.6193 19.912 13.6193 19.2789 13.2288 18.8883C12.8383 18.4978 12.2051 18.4978 11.8146 18.8883L11.1075 19.5954C10.717 19.986 10.0838 19.986 9.69328 19.5954C9.30276 19.2049 9.30276 18.5718 9.69328 18.1812L13.2288 14.6457C13.6193 14.2552 14.2525 14.2552 14.643 14.6457L16.0572 13.2315Z"
                            fill="white" />
                        <path
                            d="M13.9359 16.7662C15.1075 17.9378 17.007 17.9378 18.1786 16.7662L21.7141 13.2307C22.8857 12.0591 22.8857 10.1596 21.7141 8.98805C20.5425 7.81648 18.643 7.81648 17.4715 8.98805L16.7644 9.69516C16.3738 10.0857 16.3738 10.7189 16.7644 11.1094C17.1549 11.4999 17.788 11.4999 18.1786 11.1094L18.8857 10.4023C19.2762 10.0117 19.9094 10.0117 20.2999 10.4023C20.6904 10.7928 20.6904 11.426 20.2999 11.8165L16.7643 15.352C16.3738 15.7425 15.7407 15.7425 15.3501 15.352L13.9359 16.7662Z"
                            fill="white" />
                        <defs>
                            <linearGradient id="paint0_linear_01" x1="1.19805e-06" y1="30" x2="40.2" y2="9.3" gradientUnits="userSpaceOnUse">
                                <stop stop-color="#FF6F61" />
                                <stop offset="1" stop-color="#FF9554" />
                            </linearGradient>
                        </defs>
                    </svg>
                </div>

                <div class="connect-links__label">coral.ru</div>
            </div>

            <div class="connect-links__group">
                <div class="form-group">
                    <div class="form-group_inline">
                        <div class="form__field">
                            <input id="link" type="text" class="form-input">
                            <label for="link" class="form__label">Ссылка</label>
                        </div>

                        <div class="form__field" data-widget="text-counter" data-counter-max="42" data-decl='["символ", "символа", "символов"]' data-link="field">
                            <input id="link-name" type="text" class="form-input form-input_filled" value="coral.ru">
                            <label for="link-name" class="form__label">Название ссылки</label>
                            <div class="form-hint">
                                Осталось

                                <div class="form-hint__label">42</div>
                                <div class="form-hint__disc">символа</div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        `

        const contentNode = document.createElement('div');
        contentNode.classList.add('connect-links__item');
        contentNode.setAttribute('data-widget', 'filling-link');
        contentNode.innerHTML = content;

        this.fieldsGroup.appendChild(contentNode);

        EventHelper.refreshUI();
    }
}

export default ConnectLinks;
