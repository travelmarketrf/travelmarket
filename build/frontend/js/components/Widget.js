'use strict';

import FilterWidgetInterface from 'js/components/FilterWidgetInterface';
import WidgetFactory from 'js/components/WidgetFactory';

class Widget {
    static get(node) {
        if (!node) return null;

        return node['_component'] || null;
    }

    static is(node) {
        return !!Widget.get(node);
    }

    static findNodes(container, name) {
        const selector = name ? `[data-widget="${name}"]` : '[data-widget]';
        return [...container.querySelectorAll(selector)];
    }

    static find(container, name) {
        const widgets = Widget.findNodes(container, name)
            .map(node => Widget.get(node))
            .filter(widget => widget);

        return widgets;
    }

    static isFilterable(widget) {
        return widget instanceof FilterWidgetInterface;
    }

    static instantiate(node) {
        return Widget.is(node) ? null : WidgetFactory.create(node);
    }

    static onReady(widgetNodes) {
        const readyWidgets = [];

        return new Promise(resolve => {
            function loop() {
                widgetNodes = widgetNodes.filter(node => {
                    const widget = Widget.get(node);

                    if (!widget)
                        return true;

                    readyWidgets.push(widget);

                    return false;
                });

                if (widgetNodes.length)
                    return requestAnimationFrame(loop);

                resolve(readyWidgets);
            }

            loop();
        });
    }
}

export default Widget;
