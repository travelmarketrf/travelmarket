'use strict';

import Layout from 'js/helpers/Layout';
import Widget from 'js/components/Widget';

class SelectRadioInteraction {
    constructor(node) {
        this.node = node;

        if (!this.node) return;

        this.input = this.node.querySelector('#city-input');

        this.node['_component'] = this;

        this._bindEvents();
    }

    _bindEvents() {
        this.onSelectChange = this.onSelectChange.bind(this);
        this.onRadioClick = this.onRadioClick.bind(this);
        this.onInit = this.onInit.bind(this);

        Layout.body.addEventListener('select-default:change', this.onSelectChange, false);
        Layout.body.addEventListener('custom-radio:click', this.onRadioClick, false);
        Layout.body.addEventListener('app:init', this.onInit, false);
    }

    onInit() {
        this.radio = Widget.get(this.node.querySelector('[data-widget="custom-radio"]'));
        this.select = Widget.get(this.node.querySelector('[data-widget="select-default"]'));
        this.setValue(this.radio.getValue());
    }

    onSelectChange(e) {
        this.radio.unchecked();
        this.setValue(this.select.getValue());
    }

    onRadioClick(e) {
        this.select.reset();
        this.setValue(this.radio.getValue());
    }

    setValue(value) {
        this.input.value = value;
    }
}

export default SelectRadioInteraction;
