'use strict';

import ScrollLocker from 'js/helpers/ScrollLocker';
import Layout from 'js/helpers/Layout';
import EventHelper from 'js/helpers/EventHelper';

class PopupManager {
    constructor() {
        this.actions = {
            'open-popup': 'open',
            'close-popup': 'close'
        };

        this.onClick = this.onClick.bind(this);
        this.onOpen = this.onOpen.bind(this);
        this.closeActive = this.closeActive.bind(this);

        Layout.body.addEventListener('click', this.onClick, false);
        Layout.body.addEventListener('popup-manager:open', this.onOpen, false);
        Layout.body.addEventListener('popup-manager:close', this.closeActive, false);
    }

    isPopupOpen() {
        return !!this.popupName;
    }

    closeActive() {
        this.close(this.popupName);
    }

    onClick(event) {
        const node = event.target.closest('[data-action]');

        if (event.target.closest('.popup__container') && !node) return;

        if (!node && this.isPopupOpen()) {
            return this.closeActive();
        }

        if (!node) return

        const name = node.getAttribute('data-name');
        const action = node.getAttribute('data-action');
        const attrs = node.getAttribute('data-pass-attrs');

        if (!action || !this.actions[action]) return;

        this[this.actions[action]](name, attrs);
    }

    open(name, attrs) {
        attrs = attrs ? JSON.parse(attrs) : null;

        const popup = document.querySelector(`.popup[data-name='${name}']`);

        if (!popup)
            throw new Error(`A popup with name ${name} doesn't exist on the current page`);

        const form = popup.querySelector('form');

        popup.classList.add('popup_open');

        EventHelper.refreshUI(null, popup);

        if (attrs && attrs.fields && form) {
            for (let field in attrs.fields) {
                const input = form.querySelector(`[name="${field}"]`);

                if (input) {
                    input.value = attrs.fields[field];
                }
            }
        }

        if (this.isPopupOpen())
            this.closeActive();

        this.popupName = name;

        ScrollLocker.lock();

        this._dispatchPopupOpenEvent(popup);
    }

    close(name) {
        const popup = document.querySelector(`.popup[data-name='${name}']`);
        popup.classList.remove('popup_open');
        this.popupName = null;

        ScrollLocker.unlock();

        this._dispatchPopupCloseEvent(popup);
    }

    onOpen(e) {
        this.open(e.detail.value);
    }

    _dispatchPopupCloseEvent(popup) {
        const event = new CustomEvent('popup:close', {
            bubbles: true,
            cancelable: true,
            detail: {
                component: this,
                popupNode: popup
            }
        });

        Layout.body.dispatchEvent(event);
    }

    _dispatchPopupOpenEvent(popup) {
        const event = new CustomEvent('popup:open', {
            bubbles: true,
            cancelable: true,
            detail: {
                component: this,
                popupNode: popup
            }
        });

        Layout.body.dispatchEvent(event);
    }
}

export default PopupManager;
