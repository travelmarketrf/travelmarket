'use strict';

import Layout from 'js/helpers/Layout';
import Widget from 'js/components/Widget';

class TabsManager {
    constructor() {
        this.onTabChange = this.onTabChange.bind(this);
        Layout.body.addEventListener('tab:change', this.onTabChange, false);

        this.widgetActions = {
            'minicard-slider': function(slider) {
                return slider && slider.update();
            },
            'gmap': function(map) {
                return map && map.fitMarkers();
            },
            'review': function(review) {
                review && review.onResize();
            }
        }
    }

    onTabChange(e) {
        const widgets = [...e.detail.container.querySelectorAll('[data-widget]')];

        widgets.forEach(node => {
                const widgetName = node.getAttribute('data-widget');
                this.widgetActions[widgetName] && this.widgetActions[widgetName](Widget.get(node))
            }
        );
    }
}

export default TabsManager;
