'use strict';

import Layout from 'js/helpers/Layout';

class FixableBlockManager {
    constructor() {
        this.fixableNodes = null;
        this._bindEvents();
        this.onScroll();
    }

    getFixableNodes(useCache = true) {
        if (!useCache) {
            this.fixableNodes = [...document.querySelectorAll('[data-behavior="fixable"]')];
            return this.fixableNodes;
        }

        if (this.fixableNodes)
            return this.fixableNodes;

        this.fixableNodes = [...document.querySelectorAll('[data-behavior="fixable"]')];
        return this.fixableNodes;
    }

    _bindEvents() {
        this.onScroll = this.onScroll.bind(this);
        window.addEventListener('scroll', this.onScroll, false);
        window.addEventListener('resize', this.onScroll, false);
    }

    getOffset() {
        return Layout.headerBottom ? Layout.headerBottom.offsetHeight : 0;
    }

    onScroll(e) {
        this.getFixableNodes().forEach(node => {
            if (node.parentElement.getBoundingClientRect().top - this.getOffset() < 0) {
                this.fix(node);
            } else {
                this.unfix(node);
            }
        });
    }

    fix(node) {
        const fixableNode = node.querySelector('[data-role="fixable"]');
        const offsetNode = fixableNode ? fixableNode : node;

        if (fixableNode) {
            node.style.height = `${node.offsetHeight}px`;
        }

        offsetNode.style.top = `${this.getOffset()}px`;
        node.classList.add('fixed');
    }

    unfix(node) {
        const fixableNode = node.querySelector('[data-role="fixable"]');
        const offsetNode = fixableNode ? fixableNode : node;

        if (fixableNode) {
            node.style.height = '';
        }

        node.classList.remove('fixed');
        offsetNode.style.top = '';
    }
}

export default FixableBlockManager;
