'use strict';

class FixableHeaderManager {
    constructor() {
        this.fixableNode = document.querySelector('[data-behavior="fixable-header"]');

        if (!this.fixableNode) return;

        this.containerNode = this.fixableNode.parentElement;

        this._bindEvents();
        this.onScroll();
    }

    _bindEvents() {
        this.onScroll = this.onScroll.bind(this);
        window.addEventListener('scroll', this.onScroll, false);
        window.addEventListener('resize', this.onScroll, false);
    }

    onScroll(e) {
        if (this.containerNode.getBoundingClientRect().top < 0) {
            this.fix();
        } else {
            this.unfix();
        }
    }

    fix() {
        this.containerNode.style.height = this.fixableNode.offsetHeight + 'px';
        this.fixableNode.classList.add('header-fixed');
    }

    unfix() {
        this.containerNode.style.height = 'auto';
        this.fixableNode.classList.remove('header-fixed');
    }
}

export default FixableHeaderManager;
