'use strict';

import Layout from 'js/helpers/Layout';
import ActionMapper from 'js/helpers/ActionMapper';
import EventHelper from 'js/helpers/EventHelper';

class CustomizablePopup {
    constructor() {
        this.actions = {};

        this._bindEvents();
    }

    _bindEvents() {
        this.onClose = this.onClose.bind(this);
        this.onClick = this.onClick.bind(this);

        Layout.body.addEventListener('popup:close', this.onClose, false);
        Layout.body.addEventListener('click', this.onClick, false);
    }

    onClick(e) {
        ActionMapper.callAction(this, this.actions, e.target);
    }

    onClose(e) {
        const popupNode = e.detail.popupNode;

        if (!popupNode.hasAttribute('data-removable'))
            return;

        popupNode.remove();
    }

    _createWindow(modifier, name) {
        const popupNode = document.createElement('div');

        popupNode.classList.add('popup', modifier);
        popupNode.setAttribute('data-name', name);
        popupNode.setAttribute('data-removable', '');

        return popupNode;
    }

    _dispatchPopupOpenEvent(name, attrs) {
        const data = {
            component: this,
            value: name
        };

        if (attrs) {
            data.attrs = attrs;
        }

        const event = new CustomEvent('popup-manager:open', {
            bubbles: true,
            cancelable: true,
            detail: data
        });

        Layout.body.dispatchEvent(event);

        EventHelper.refreshUI();
    }

    _dispatchPopupCloseEvent() {
        const event = new CustomEvent('popup-manager:close', {
            bubbles: true,
            cancelable: true,
            detail: {
                component: this
            }
        });

        Layout.body.dispatchEvent(event);
    }

    /**
     * Open popup.
     *
     * @param {HTMLElement} node Event target node.
     *
     * @override
     */
    open(node) {}

    /**
     * Create a new popup node in DOM.
     *
     * @override
     */
    _generatePopup() {}
}

export default CustomizablePopup;
