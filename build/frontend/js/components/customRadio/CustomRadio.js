'use strict';

import Layout from 'js/helpers/Layout';

class CustomRadio {
    constructor(node) {
        this.node = node;

        if (!this.node) return;

        this.node['_component'] = this;

        this.onRadioClick = this.onRadioClick.bind(this);

        this.label = this.node.querySelector('.radio__label');
        this.radioInput = this.node.querySelector('input[type="radio"]');

        if (this.label)
            this.label.addEventListener('click', this.onRadioClick, false);
    }

    onRadioClick() {
        const event = new CustomEvent('custom-radio:click', {
            bubbles: true,
            cancelable: true,
            detail: {
                component: this,
                value: this.getValue()
            }
        });

        this.node.dispatchEvent(event);
    }

    getValue() {
        return this.radioInput.value;
    }

    setValue(value) {
        this.radioInput.value = value;
    }

    unchecked() {
        this.radioInput.checked = false;
        this.radioInput.removeAttribute('checked');
    }

    checked() {
        this.radioInput.checked = true;
        this.radioInput.setAttribute('checked', '');
    }

    reset() {
        this.radioInput.value = '';
    }
}

export default CustomRadio;
