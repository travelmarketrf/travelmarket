'use strict';

import MD from 'js/helpers/MD';
import DomHelper from 'js/helpers/DomHelper';

class Wave {
    constructor(container, options) {
        this.container = container;
        this.width = this.container.offsetWidth;
        this.height = this.container.offsetHeight;
        this.points = 15;
        this.delta = 5;

        if (MD.betweenResolution()) {
            this.points = 11;
            this.delta = 3;
        }

        if (MD.mobile()) {
            this.points = 8;
            this.delta = 3;
        }

        if (MD.phone()) {
            this.points = 5;
            this.delta = 2;
        }

        this.wave = this.container.querySelector('.preloader__wave');
        this.waveSvg = this.container.querySelector('.preloader__item > svg');
        this.options = options || { waveWidth: this.width, waveHeight: 15, waveDelta: this.delta, speed: 1, wavePoints: this.points };
        this.lastUpdate;
        this.totalTime = 0;
        this._isRunning = false;

        this.tick = this.tick.bind(this);
    }

    calculateWavePoints(factor) {
        let points = [];

        for (let i = 0; i <= this.options.wavePoints; i++) {
            let xPos = i / this.options.wavePoints * this.width;
            let sinSeed = (factor + (i + i % this.options.wavePoints)) * this.options.speed * 100;
            let sinHeight = Math.sin(sinSeed / 100) * this.options.waveDelta;
            let yPos = Math.sin(sinSeed / 100) * sinHeight + this.options.waveHeight;
            points.push({ x: xPos, y: yPos });
        }

        return points;
    }

    buildPath(points) {
        let svgString = `M ${points[0].x} ${points[0].y}`;

        let cp0 = {
            x: (points[1].x - points[0].x) / 2,
            y: (points[1].y - points[0].y) + points[0].y + (points[1].y - points[0].y)
        };

        svgString += ` C ${cp0.x} ${cp0.y} ${cp0.x} ${cp0.y} ${points[1].x} ${points[1].y}`;

        let prevCp = cp0;
        let inverted = -1;

        for (let i = 1; i < points.length - 1; i++) {
            let cpLength = Math.sqrt(prevCp.x * prevCp.x + prevCp.y * prevCp.y);

            let cp1 = {
                x: (points[i].x - prevCp.x) + points[i].x,
                y: (points[i].y - prevCp.y) + points[i].y
            };

            svgString += ` C ${cp1.x} ${cp1.y} ${cp1.x} ${cp1.y} ${points[i + 1].x} ${points[i + 1].y}`;
            prevCp = cp1;
            inverted = -inverted;
        }

        svgString += ` L ${this.width} ${this.height}`;
        svgString += ` L 0 ${this.height} Z`

        return svgString;
    }

    tick() {
        let now = window.Date.now();

        if (this.lastUpdate) {
            let elapsed = (now - this.lastUpdate) / 1000;
            this.lastUpdate = now;

            this.totalTime += elapsed;

            let factor = this.totalTime * Math.PI;
            this.wave.setAttribute('d', this.buildPath(this.calculateWavePoints(factor)));
        } else {
            this.lastUpdate = now;
        }

        this.timer = requestAnimationFrame(this.tick);
    }

    pause() {
        if (!this.isRunning()) return;

        cancelAnimationFrame(this.timer);
        this._isRunning = false;
    }

    resume() {
        if (this.isRunning()) return;

        this.width = this.container.offsetWidth;
        this.height = this.container.offsetHeight;
        this._isRunning = true;

        this.tick();
    }

    isRunning() {
        return this._isRunning;
    }

    setHeight(height) {
        this.waveSvg.style.height = height + 'vh';
    }
}

export default Wave;
