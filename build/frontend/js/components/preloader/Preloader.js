'use strict';

import Layout from 'js/helpers/Layout';
import Wave from 'js/components/preloader/Wave';
import ScrollLocker from 'js/helpers/ScrollLocker';

class Preloader {
    constructor() {
        this.node = document.createElement('div');
        this.maxProgress = 59;
        this.minProgress = 46;

        this.node.classList.add('preloader');

        this.node.innerHTML = `
            <div class="preloader__container"></div>

            <div class="preloader__item">
                <svg width="100%" height="100%" version="1.1" xmlns="http://www.w3.org/2000/svg">
                    <path class="preloader__wave" d="" fill="#fff" />
                </svg>
            </div>
        `;

        Layout.root.appendChild(this.node);
        this.wave = new Wave(this.node);
    }

    on() {
        ScrollLocker.lock();
        this.node.classList.add('preloader_on');
        this.wave.resume();
    }

    off() {
        ScrollLocker.unlock();
        this.node.classList.remove('preloader_on');
        this.wave.pause();
    }

    // progress must be between 0 and 1
    setProgress(progress) {
        const height = this.minProgress + (this.maxProgress - this.minProgress) * progress;

        this.wave.setHeight(height);
    }
}

export default Preloader;
