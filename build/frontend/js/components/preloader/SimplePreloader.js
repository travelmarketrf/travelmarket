'use strict';

import Layout from 'js/helpers/Layout';

class SimplePreloader {
    constructor() {
        this.node = document.createElement('div');

        this.node.classList.add('simple-preloader');

        Layout.root.appendChild(this.node);
    }

    on() {
        this.node.classList.add('simple-preloader_on');
    }

    off() {
        this.node.classList.remove('simple-preloader_on');
    }

    // progress must be between 0 and 1
    setProgress(progress) {
        this.node.style.width = 100 * progress + '%';
    }
}

export default SimplePreloader;
