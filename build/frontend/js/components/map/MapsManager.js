/*!
 * @author Chistyakov I. V. <ichistyakovv@gmail.com>
 * Licensed under the MIT license
 */

'use strict';

import GoogleMapLoader from 'js/components/map/googleMap/GoogleMapLoader';

class MapsManager {
    constructor() {
        this.loaders = [new GoogleMapLoader()];
    }

    refresh() {
        for (let i = 0; i < this.loaders.length; i++) {
            this.loaders[i].load(() => this.loaders[i].refresh());
        }
    }
}

export default MapsManager;
