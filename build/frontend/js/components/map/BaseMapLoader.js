/*!
 * @author Chistyakov I. V. <ichistyakovv@gmail.com>
 * Licensed under the MIT license
 */

'use strict';

 class BaseMapLoader {
    constructor() {
        this.callbacks = [];
        this.state = {
            isLoaded: false,
            isInitialized: false
        };
        this._bindEvents();
    }

    _loop() {
        if (this.isLoaded())
            return this._onReady();

        requestAnimationFrame(this._loop);
    }

    /**
     * Method must be implemented in a child class.
     * 
     * @override
     */
    _bindEvents() {}

    /**
     * Method must be implemented in a child class.
     * 
     * @override
     */
    _attachMapScript() {}

    _onScriptLoaded() {
        this.setState({isLoaded: true});
    }

    setState(state = {}) {
        Object.assign(this.state, state);
    }

    /**
     * Method must be implemented in a child class.
     * 
     * @override
     */
    getMapNodesArray() {}

    isInitNeeded() {
        return !!this.getMapNodesArray().length;
    }

    isLoaded() {
        return this.state.isLoaded;
    }

    isInitialized() {
        return this.state.isInitialized;
    }

    /**
     * Method must be implemented in a child class.
     * 
     * @override
     */
    refresh() {}

    load(cb) {
        cb && this.callbacks.push(cb);
        
        if (!this.isLoaded() && !this.isInitialized() && this.isInitNeeded()) {
            this.setState({isInitialized: true});
            this._attachMapScript();
            this._loop();
            return;
        }

        if (this.isLoaded()) {
            this._onReady();
        }
    }

    _onReady() {
        this.callbacks.forEach(cb => cb());
        this.callbacks = [];
    }
}

export default BaseMapLoader;
