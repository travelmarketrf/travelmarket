'use strict';

import ScrollLocker from 'js/helpers/ScrollLocker';
import ActionMapper from 'js/helpers/ActionMapper';

class ResizeMap {
    constructor(node) {
        this.node = node;

        if (!this.node) return;

        this.node['_component'] = this;

        this.actions = {
            'full-map': 'increase',
            'tight-map': 'reduce'
        }

        this.onClick = this.onClick.bind(this);

        this.offsetTop;

        this.node.addEventListener('click', this.onClick, false);
    }

    onClick(e) {
        ActionMapper.callAction(this, this.actions, e.target);
    }

    increase() {
        this.offsetTop = this.node.offsetTop;
        window.scrollTo(0, 0);

        ScrollLocker.lock();

        this.node.classList.add('agency__map_full');
    }

    reduce() {
        window.scrollTo(0, this.offsetTop)

        ScrollLocker.unlock();

        this.node.classList.remove('agency__map_full');
    }
}

export default ResizeMap;
