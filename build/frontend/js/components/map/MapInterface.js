/*!
 * @author Chistyakov I. V. <ichistyakovv@gmail.com>
 * Licensed under the MIT license
 */

'use strict';

/**
 * Class represents an abstract base class for the map widget.
 * 
 * @class
 * @abstract
 */
class MapInterface {
    /**
     * Load the markers on the map from an array of locations.
     *
     * ```js
     * const markers = [{type: "someType", location: {lat: 0, lon: 0}}];
     * ```
     * @abstract
     */
    loadMarkersFromArray(markers) {}

    /**
     * Set an active marker on the map.
     * 
     * @param {Number} id Marker identifier.
     * @abstract
     */
    setActiveMarkerById(id) {}

    /**
     * Fit the map boundaries to all markers on the map.
     * 
     * @abstract
     */
    fitMarkers() {}

    /**
     * Fire the set marker event.
     * 
     * @param {Number} id Marker identifier.
     * @abstract
     */
    _dispatchSetMarkerEvent(id) {}

    /**
     * Get all markers in visible area.
     * 
     * @returns {Array} A set of markers.
     * @abstract
     */
    getMarkerIdsInViewport() {}

    /**
     * Remove all markers from the map.
     * 
     * @abstract
     */
    clearMarkers() {}

    /**
     * Put a marker to the centre of the map.
     * 
     * @param {Number} id Marker identifier.
     * @abstract
     */
    panToMarkerId(id) {}

    /**
     * Get all markers on the map.
     * 
     * @returns {Array} A set of markers.
     * @abstract
     */
    getMarkers() {}
}

export default MapInterface;
