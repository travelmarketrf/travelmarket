'use strict';

export default {
    geodesic: true,
    strokeColor: '#AEB8D5',
    strokeOpacity: 1.0,
    strokeWeight: 5
};
