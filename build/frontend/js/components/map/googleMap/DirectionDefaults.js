'use strict';

export default {
    provideRouteAlternatives: false,
    travelMode: 'DRIVING',
};
