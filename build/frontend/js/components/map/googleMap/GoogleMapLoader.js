/*!
 * @author Chistyakov I. V. <ichistyakovv@gmail.com>
 * Licensed under the MIT license
 */

'use strict';

import BaseMapLoader from 'js/components/map/BaseMapLoader';
import MapFactory from 'js/components/map/googleMap/MapFactory';
import MapKey from "js/components/map/googleMap/MapKey";
import Widget from 'js/components/Widget';

const MAP_SRC = `https://maps.googleapis.com/maps/api/js?key=${(new MapKey()).get()}&callback=$$GMap$$Init&libraries=places`;

class GoogleMapLoader extends BaseMapLoader {
    _bindEvents() {
        this._loop = this._loop.bind(this);
        this._onScriptLoaded = this._onScriptLoaded.bind(this);
        window.$$GMap$$Init = this._onScriptLoaded;
    }

    _attachMapScript() {
        const elem = document.createElement('script');
        const head = document.head || document.getElementsByTagName('head')[0];

        elem.src = MAP_SRC;
        head.appendChild(elem);
    }

    _applyLibraries(src) {

    }

    getMapNodesArray() {
        return [...document.querySelectorAll('[data-widget="gmap"]')];
    }

    refresh() {
        this.getMapNodesArray().forEach(node => !Widget.is(node) && MapFactory.create(node));
    }
}

export default GoogleMapLoader;
