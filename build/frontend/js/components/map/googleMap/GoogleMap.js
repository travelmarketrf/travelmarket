/*!
 * @author Chistyakov I. V. <ichistyakovv@gmail.com>
 * Licensed under the MIT license
 */

'use strict';

import MapInterface from 'js/components/map/MapInterface';
import mapDefaults from 'js/components/map/googleMap/mapDefaults';

class GoogleMap extends MapInterface {
    constructor(node, options = {}) {
        super();

        this.node = node;

        if (!this.node) return;

        this.node['_component'] = this;

        this.options = options;
        this.images = options.images || {};
        this.mapOpts = options.map ? Object.assign(mapDefaults, options.map) : mapDefaults;
        this.markers = {};
        this.markerImages = {};
        this.activeMarker = null;
        this.markersData = this.node.getAttribute('data-markers');
        this.directionsData = this.node.getAttribute('data-directions');
        this.composite = {
            popup: options.popup ? true : false,
            cluster: options.cluster ? true : false
        };

        this._clusterer = null;
        this._infowindow = null;
        this._directions = null;
        this._places = null;
        this._tokenManager = null;
        this._autocomplete = null;

        this._bindEvents();
        this._init();
    }

    _bindEvents() {
        this._onResize = this._onResize.bind(this);
        this._dispatchChangeViewEvent = this._dispatchChangeViewEvent.bind(this);
        this._dispatchClickEvent = this._dispatchClickEvent.bind(this);
        this._dispatchMarkerDragEvent = this._dispatchMarkerDragEvent.bind(this);
        this._dispatchMarkerDragEndEvent = this._dispatchMarkerDragEndEvent.bind(this);
        this._dispatchMarkerDragStartEvent = this._dispatchMarkerDragStartEvent.bind(this);
        window.addEventListener('resize', this._onResize, false);
    }

    _init() {
        this._createMarkerImages();

        this.map = new google.maps.Map(this.node, this.mapOpts);
        this.map.addListener('zoom_changed', this._dispatchChangeViewEvent);
        this.map.addListener('dragend', this._dispatchChangeViewEvent);
        this.map.addListener('click', this._dispatchClickEvent);

        if (this.options.style)
            this.stylize(this.options.style);

        if (this.hasCluster()) {
            this._clusterer = this.options.cluster.builder(this.map, []);
            this._clusterer.setMap(this.map);
        }

        if (this.hasPopup()) {
            this._infowindow = this.options.popup.builder();
        }

        if (this.hasTokenManager()) {
            this._tokenManager = this.options.tokenManager.tokenBuilder();
        }

        if (this.hasPlaces()) {
            this._places = this.options.places.placesBuilder(this.map);
        }

        if (this.hasAutocomplete()) {
            this._autocomplete = this.options.autocomplete.autocompleteBuilder();
        }

        if (this.hasGeocoder()) {
            this._geocoder= this.options.geocoder.geocoderBuilder();
        }

        if (this.hasDirections()) {
            this._directions = this.options.directions.directionsBuilder();
            this._directions.setMap(this.map);
        }

        if (this.markersData) {
            this._parsedMarkersData = JSON.parse(this.markersData);
            this.loadMarkersFromArray(this._parsedMarkersData);
        }

        if (this.hasDirections()) {
            this._parsedDirectionsData = JSON.parse(this.directionsData);
            this.loadDirectionsFromArray(this._parsedDirectionsData);
        }

        if (this.hasCluster()) {
            this._clusterer.addMarkers(this.getMarkers());
        }

        if (this._parsedMarkersData && this._parsedMarkersData.length === 1) {
            if (this.mapOpts && this.mapOpts.zoom)
                this.map.setZoom(this.mapOpts.zoom);

            const center = new google.maps.LatLng(this._parsedMarkersData[0].location.lat, this._parsedMarkersData[0].location.lon);

            this.map.setCenter(center);
        } else {
            this.fitMarkers();
        }

        this._clearAttrs();
        this._dispatchReadyEvent();
    }

    _clearAttrs() {
        this.node.removeAttribute('data-markers');
        this.node.removeAttribute('data-directions');
        this.node.removeAttribute('data-opts');
    }

    _onResize(e, details) {
        if (!this.map) return;
        this.fitMarkers();
    }

    _createMarkerImages() {
        for (let i in this.images) {
            this.markerImages[i] = {};
            for (let j in this.images[i]) {
                this.markerImages[i][j] = {
                    url: this.images[i][j].url,
                    // This marker is x pixels wide by y pixels high.
                    size: new google.maps.Size(this.images[i][j].width, this.images[i][j].height),
                    // The origin for this image is (0, 0).
                    origin: new google.maps.Point(0, 0),
                    // The anchor for this image is the base of the flagpole at (x, y).
                    anchor: new google.maps.Point(this.images[i][j].offsetX, this.images[i][j].offsetY)
                };
            }
        }
    }

    stylize(style) {
        const styledMap = new google.maps.StyledMapType(style, {name: "Styled Map"});

        this.map.mapTypes.set('styled_map', styledMap);
        this.map.setMapTypeId('styled_map');
    }

    fitMarkers() {
        const bounds = new google.maps.LatLngBounds();
        let markersCount = 0;

        for (let id in this.markers) {
            bounds.extend(this.markers[id].orig.getPosition());
            markersCount++;
        }

        if (!markersCount) return;

        this.map.fitBounds(bounds);
    }

    _getMarkerImage(type) {
        if (this.markerImages[type])
            return this.markerImages[type];

        return this.markerImages.default;
    }

    addMarker(data) {
        const id = data.id;
        const markerImage = this._getMarkerImage(data.type);
        const config = {
            position: { lat: data.location.lat, lng: data.location.lon },
            icon: markerImage.default,
            map: this.map
        }

        if (data.label) {
            config.label = Object.assign({}, this.options.label, {text: data.label});
        }

        if (this.isMarkerDraggable()) {
            config.draggable = true;
        }

        const marker = new google.maps.Marker(config);

        marker.addListener('click', e => this.setActiveMarkerById(id));

        if (this.isMarkerDraggable()) {
            marker.addListener('drag', e => this._dispatchMarkerDragEvent(e, id));
            marker.addListener('dragend', e => this._dispatchMarkerDragEndEvent(e, id));
            marker.addListener('dragstart', e => this._dispatchMarkerDragStartEvent(e, id));
        }

        if (this.markers[id])
            this.removeMarkerById(id);

        this.markers[id] = {
            markerImage: markerImage,
            data: data,
            orig: marker
        };

        if (this.hasCluster())
            this._clusterer.addMarker(marker);

        return marker;
    }

    getMarkers() {
        const markers = [];

        for (let key in this.markers) {
            markers.push(this.markers[key].orig)
        }

        return markers;
    }

    getMarkerDataById(id) {
        if (!this.markers[id]) return null;

        return this.markers[id].data;
    }

    loadMarkersFromArray(markers) {
        for (let i = 0; i < markers.length; i++) {
            this.addMarker(markers[i]);
        }
    }

    loadDirectionsFromArray(directions) {
        if (!this.hasDirections()) return;

        this._directions.setDirections(directions);
        this.loadMarkersFromArray(this._directions.getPoints());
    }

    moveMarkerById(id, lat, lon) {
        if (!this.markers[id]) return;

        this.markers[id].orig.setPosition(new google.maps.LatLng(lat, lon));
    }

    refreshToken(id) {
        if (!this.hasTokenManager()) return;

        this._tokenManager.refresh(id);
    }

    setLabelTextByMarkerId(id, text) {
        if (!this.markers[id]) return;

        const marker = this.markers[id].orig;
        const label = marker.getLabel();

        label.text = text;

        marker.setLabel(label);
    }

    hasMarker(id) {
        return !!this.markers[id];
    }

    removeMarkerById(id) {
        if (!this.markers[id]) return;

        this.markers[id].orig.setMap(null);
        this.markers[id] = null;
    }

    clearMarkers() {
        if (this.hasCluster()) {
            this._clusterer.clearMarkers();
        }

        for (let id in this.markers) {
            this.removeMarkerById(id);
        }
        this.markers = {};

        if (this.hasPopup())
            this._infowindow.close();
    }

    clearDirections() {
        if (!this.hasDirections()) return;

        this._directions.clear();
    }

    search(query, id) {
        if (!this.hasAutocomplete()) return;

        const request = Object.assign({}, {input: query});

        if (id && this.hasTokenManager()) {
            request.sessionToken = this._tokenManager.get(id);
        }

        return new Promise((resolve, reject) => {
            this._autocomplete.getPlacePredictions(request, (results, status) => {
                const data = [];

                for (let i = 0; i < results.length; i++) {
                    data.push({
                        description: results[i].description,
                        place_id: results[i].place_id
                    });
                }

                status === 'OK' ? resolve(data) : reject(new Error(status));
            });
        });
    }

    /**
     * Request details about a place.
     * 
     * @param {String} placeId Place identifier.
     * @param {Number} id Token identifier.
     * 
     * @returns {Promise}
     */
    getPlaceDetails(placeId, id) {
        if (!this.hasPlaces()) return;

        const request = Object.assign(this.options.places.request, {placeId});

        if (id && this.hasTokenManager()) {
            request.sessionToken = this._tokenManager.get(id);
            this._tokenManager.refresh(id);
        }

        return new Promise((resolve, reject) => {
            this._places.getDetails(request, (result, status) => {

                const data = {
                    lat: result.geometry.location.lat(),
                    lon: result.geometry.location.lng()
                };

                status === 'OK' ? resolve(data) : reject(new Error(status));
            });
        });
    }

    geocode(params) {
        if (!this.hasGeocoder()) return;

        const request = Object.assign(this.options.geocoder.request, params);

        return new Promise((resolve, reject) => {
            this._geocoder.geocode(request, (results, status) => {
                const data = [];

                for (let i = 0; i < results.length; i++) {
                    data.push({
                        lat: results[i].geometry.location.lat(),
                        lon: results[i].geometry.location.lng()
                    });
                }

                status === 'OK' ? resolve(data) : reject(new Error(status));
            });
        });
    }

    panToMarkerId(id) {
        if (!this.markers[id]) return;

        this.map.panTo(this.markers[id].orig.getPosition());
        // this.map.setZoom(this.zoom);
    }

    setActiveMarkerById(id) {
        if (!this.markers[id]) return;

        if (this.activeMarker && this.activeMarker.markerImage.active) {
            this.activeMarker.orig.setIcon(this.activeMarker.markerImage.default);
        }

        if (this.markers[id].markerImage.active) {
            this.markers[id].orig.setIcon(this.markers[id].markerImage.active);
        }

        this.activeMarker = this.markers[id];
        this.panToMarkerId(id);
        this.showPopup(id);
        this._dispatchSetMarkerEvent(id);
    }

    _getImage(type) {
        if (this.images[type])
            return this.images[type];

        return this.images.default;
    }

    showPopup(markerId) {
        if (!this.hasPopup() || !this.markers[markerId]) return;

        const data = this.markers[markerId].data;
        const content = this.options.popup.render ? this.options.popup.render(data) : '';
        const image = this._getImage(data.type);
        const size = image.active || image.default;
        const offset = new google.maps.Size(0, -size.height);

        this._infowindow.setContent(content);
        this._infowindow.pixelOffset = offset;
        this._infowindow.setPosition(this.markers[markerId].orig.getPosition());
        this._infowindow.open(this.map);
    }

    getMarkerIdsInViewport() {
        const ids = [];
        const mapBounds = this.map.getBounds();

        if (!mapBounds) return ids;

        for (let key in this.markers) {
            if (mapBounds.contains(this.markers[key].orig.getPosition()))
                ids.push(parseInt(key, 10));
        }

        return ids;
    }

    _dispatchChangeViewEvent() {
        const event = new CustomEvent('map:view-change', {
            bubbles: true,
            cancelable: true,
            detail: {
                component: this
            }
        });
        this.node.dispatchEvent(event);
    }

    _dispatchClickEvent(e) {
        const event = new CustomEvent('map:click', {
            bubbles: true,
            cancelable: true,
            detail: {
                component: this,
                coord: {
                    lat: e.latLng.lat(),
                    lon: e.latLng.lng()
                }
            }
        });
        this.node.dispatchEvent(event);
    }

    _dispatchMarkerDragEvent(e, id) {
        const event = new CustomEvent('map:marker-drag', {
            bubbles: true,
            cancelable: true,
            detail: {
                component: this,
                id: id,
                coord: {
                    lat: e.latLng.lat(),
                    lon: e.latLng.lng()
                }
            }
        });
        this.node.dispatchEvent(event);
    }

    _dispatchMarkerDragEndEvent(e, id) {
        const event = new CustomEvent('map:marker-drag-end', {
            bubbles: true,
            cancelable: true,
            detail: {
                component: this,
                id: id,
                coord: {
                    lat: e.latLng.lat(),
                    lon: e.latLng.lng()
                }
            }
        });
        this.node.dispatchEvent(event);
    }

    _dispatchMarkerDragStartEvent(e, id) {
        const event = new CustomEvent('map:marker-drag-start', {
            bubbles: true,
            cancelable: true,
            detail: {
                component: this,
                id: id,
                coord: {
                    lat: e.latLng.lat(),
                    lon: e.latLng.lng()
                }
            }
        });
        this.node.dispatchEvent(event);
    }

    _dispatchReadyEvent() {
        const event = new CustomEvent('map:ready', {
            bubbles: true,
            cancelable: true,
            detail: {
                component: this
            }
        });
        this.node.dispatchEvent(event);
    }

    _dispatchSetMarkerEvent(id) {
        const event = new CustomEvent('map:set-marker', {
            bubbles: true,
            cancelable: true,
            detail: {
                component: this,
                id: id
            }
        });
        this.node.dispatchEvent(event);
    }

    getMap() {
        return this.map;
    }

    hasCluster() {
        return this.composite.cluster;
    }

    hasPopup() {
        return this.composite.popup;
    }

    hasLabel() {
        return !!this.options.label;
    }

    hasDirections() {
        return !!this.options.directions;
    }

    hasPlaces() {
        return !!this.options.places;
    }

    hasAutocomplete() {
        return !!this.options.autocomplete;
    }

    hasGeocoder() {
        return !!this.options.geocoder;
    }

    hasTokenManager() {
        return !!this.options.tokenManager;
    }

    isMarkerDraggable() {
        return !!this.options.markerDraggable;
    }
}

export default GoogleMap;
