'use strict';

export default {
    // 1: {
    //     default: {
    //         url: '/img/fresh_marker.png',
    //         width: 50,
    //         height: 58,
    //         offsetX: 25,
    //         offsetY: 58
    //     },
    //     active: {
    //         url: '/img/red_marker.png',
    //         width: 54,
    //         height: 79,
    //         offsetX: 15,
    //         offsetY: 73
    //     }
    // },
    default: {
        default: {
            url: '/images/map/marker.png',
            width: 20,
            height: 20,
            offsetX: 10,
            offsetY: 10
        }
    }
};
