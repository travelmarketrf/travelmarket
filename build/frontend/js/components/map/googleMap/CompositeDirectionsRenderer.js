'use strict';

/**
 * Class for drawing composite direction paths.
 * 
 * @author Chistyakov Ilya <ichistyakovv@gmail.com>
 */
class CompositeDirectionsRenderer {
    constructor(options) {
        this.options = options;
        this.segments = [];
        this.map = null;
        this.directions = null;
        this._directionsService = this.options.serviceBuilder();
    }

    setMap(map) {
        this.map = map;
        
        for (let i = 0; i < this.segments.length; i++) {
            this.segments[i].setMap(this.map);
        }
    }

    _buildPolylineArray(i) {
        let points = this.directions[i].points;

        // if a previous segment is a 'curve', then prepend a last point
        // as a first point in a current segment.
        if (this.directions[i-1] && this.directions[i-1].mode === 'curve') {
            points = [this.directions[i-1].points[this.directions[i-1].points.length-1], ...points];
        }

        // append a first point of a next segment as a last point in a current segment.
        if (this.directions[i+1]) {
            points = [...points, this.directions[i+1].points[0]];
        }

        return points;
    }

    setDirections(directions) {
        this.directions = directions;

        if (this.directions.length > 1)
            throw new Error('Directions must have only one segment of points.');

        // TODO (Ilya): connect points by creating a separate segment
        // between a previous and a current segments
        for (let i = 0; i < this.directions.length; i++) {
            if (this.directions[i].mode === 'curve') {
                this._createCurve(this.directions[i].points, this.directions[i].config)
                    .then(segment => this.segments.push(segment))
                    .catch(err => console.log(err.message));
            } else {
                const points = this._buildPolylineArray(i);

                this._createPolyline(points)
                    .then(segment => this.segments.push(segment))
                    .catch(err => console.log(err.message));
            }
        }
    }

    _createCurve(points, config) {
        // Minimum possible points count is 2 (the origin and the destination)
        if (points.length < 2) return Promise.reject(new Error('Points must contain at least 2 points to render a path'));

        const request = Object.assign(this.options.config, config);

        request.origin = new google.maps.LatLng(points[0].location.lat, points[0].location.lon);
        request.destination = new google.maps.LatLng(points[points.length-1].location.lat, points[points.length-1].location.lon);

        if (points.length > 2) {
            request.waypoints = [];

            for (let i = 1; i < points.length - 1; i++) {
                request.waypoints.push({ location: new google.maps.LatLng(points[i].location.lat, points[i].location.lon) });
            }
        }

        return this._rqDirections(request)
            .then(result => {
                const directionsDisplay = this.options.directionsDisplayBuilder();
                directionsDisplay.setMap(this.map);
                directionsDisplay.setDirections(result);
                return directionsDisplay;
            });
    }

    _createPolyline(points) {
        // Minimum possible points count is 2 (the origin and the destination)
        if (points.length < 2) return Promise.reject(new Error('Points must contain at least 2 points to render a path'));

        const poly = this.options.polylineBuilder();

        poly.setMap(this.map);

        const path = poly.getPath();

        for (let i = 0; i < points.length; i++) {
            path.push(new google.maps.LatLng(points[i].location.lat, points[i].location.lon));
        }

        return Promise.resolve(poly);
    }

    getPoints() {
        let points = [];

        for (let i = 0; i < this.directions.length; i++) {
            points = [...points, ...this.directions[i].points];
        }

        return points;
    }

    clear() {
        for (let i = 0; i < this.segments.length; i++) {
            this.segments[i].setMap(null);
        }
        this.segments = [];
    }

    _rqDirections(request) {
        return new Promise((resolve, reject) => {
            this._directionsService.route(request, (result, status) => {
                status === 'OK' ? resolve(result) : reject(new Error(status));
            });
        });
    }
}

export default CompositeDirectionsRenderer;
