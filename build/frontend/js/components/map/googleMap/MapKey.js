'use strict';

import BaseMapKey from "js/components/map/BaseMapKey";

class MapKey extends BaseMapKey {
    constructor() {
        super();
    }

    get() {
        const key = super.get();
        return key ? key : "AIzaSyCt4Y5Yc-uMBTK3n1AlZBtNjk_GXQt5b1o";
    }
}

export default MapKey;
