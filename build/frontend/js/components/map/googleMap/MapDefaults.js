'use strict';

export default {
    center: { lat: 0, lng: 0 },
    zoom: 16,
    disableDefaultUI: false,
    mapTypeControl: false,
    streetViewControl: false,
    fullscreenControl: false
};
