'use strict';

import ClassHelper from 'js/helpers/ClassHelper';

/**
 * Class implements the InfoWindow and the OverlayView interfaces.
 */
class GooglePopup {
    constructor() {
        ClassHelper.extend(GooglePopup, google.maps.OverlayView);

        this.position = new google.maps.LatLng(0, 0);
        this.pixelOffset = new google.maps.Size(0, 0);

        // This zero-height div is positioned at the bottom of the tip.
        this.container = document.createElement('div');
        this.container.classList.add('map-popup');

        // Optionally stop clicks, etc., from bubbling up to the map.
        google.maps.OverlayView.preventMapHitsAndGesturesFrom(this.container);
    }

    /**
     * Called when the popup is added to the map.
     */
    onAdd() {
        this.getPanes().floatPane.appendChild(this.container);
    }

    /**
     * Called when the popup is removed from the map.
     */
    onRemove() {
        if (this.container.parentElement) {
            this.container.parentElement.removeChild(this.container);
        }
    }

    setPosition(position) {
        this.position = position;
    }

    /**
     * Called each frame when the popup needs to draw itself.
     */
    draw() {
        const divPosition = this.getProjection().fromLatLngToDivPixel(this.position);

        // Hide the popup when it is far out of view.
        const isDisplayed = Math.abs(divPosition.x) < 4000 && Math.abs(divPosition.y) < 4000;

        if (isDisplayed) {
            this.container.style.left = divPosition.x + this.pixelOffset.width + 'px';
            this.container.style.top = divPosition.y + this.pixelOffset.height + 'px';
            this.container.classList.remove('map-popup_hidden');
        } else {
            this.container.classList.add('map-popup_hidden');
        }
    }

    open(map) {
        this.setMap(map);
    }

    close() {
        this.setMap(null);
    }

    setContent(content) {
        this.container.innerHTML = content;
    }
}

export default GooglePopup;
