'use strict';

import mapStyle from 'js/components/map/googleMap/mapStyle';
import images from 'js/components/map/googleMap/images';
import directionDefaults from 'js/components/map/googleMap/directionDefaults';
import placesRequestDefaults from 'js/components/map/googleMap/placesRequestDefaults';
import MarkerСlusterer from 'js/components/map/googleMap/MarkerClusterer';
import GooglePopup from 'js/components/map/googleMap/GooglePopup';
import CompositeDirectionsRenderer from 'js/components/map/googleMap/CompositeDirectionsRenderer';
import polyLineOptions from 'js/components/map/googleMap/polyLineOptions';
import Tokenizer from 'js/components/map/googleMap/Tokenizer';

export default {
    images: {
        default: images
    },
    style: {
        default: mapStyle
    },
    tokenManager: {
        default: {
            tokenBuilder: function() {
                return new Tokenizer();
            }
        }
    },
    geocoder: {
        default: {
            geocoderBuilder: function() {
                return new google.maps.Geocoder();
            },
            request: {}
        }
    },
    places: {
        default: {
            placesBuilder: function(map) {
                return new google.maps.places.PlacesService(map);
            },
            request: placesRequestDefaults
        }
    },
    autocomplete: {
        default: {
            autocompleteBuilder: function() {
                return new google.maps.places.AutocompleteService();
            }
        }
    },
    directions: {
        default: {
            directionsBuilder: function() {
                return new CompositeDirectionsRenderer(
                    {
                        config: directionDefaults,
                        directionsDisplayBuilder: function() {
                            return new google.maps.DirectionsRenderer({
                                suppressMarkers: true,
                                polylineOptions: polyLineOptions
                            });
                        },
                        polylineBuilder: function() {
                            return new google.maps.Polyline(polyLineOptions)
                        },
                        serviceBuilder: function() {
                            return new google.maps.DirectionsService();
                        }
                    }
                );
            }
        }
    },
    cluster: {
        default: {
            builder: function(map, markers) {
                return new MarkerСlusterer(map, markers, {
                    // imagePath: '/images/map/cluster',
                    styles: [
                        {
                            url: '/images/map/cluster.png',
                            width: 35,
                            height: 36,
                            textColor: '#000000',
                            textSize: 12
                        }
                    ]
                });
            }
        }
    },
    popup: {
        extended: {
            builder: function() {
                // return new google.maps.InfoWindow(/*{ pixelOffset: new google.maps.Size(0, 0) }*/);
                // returned object must implement InfoWindow interface.
                return new GooglePopup();
            },
            render: function(data) {
                function renderStars(count) {
                    return (new Array(count)).fill(`<img src="/svg/star_orange.svg" alt="star">`).join('');
                }

                return `
                    <div class="map-card">
                        <div class="map-card__anchor">
                            <div class="map-card__content">
                                <div class="map-card__img-container">
                                    <img class="map-card__img" alt="" src="${data.content.img}">
                                    <div class="map-card__container">
                                        <div class="map-card__img-labels">
                                            <div class="map-card__stars">
                                                ${renderStars(data.content.stars)}
                                            </div>
                                            <div class="map-card__rating">
                                                ${data.content.rating}
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="map-card__container">
                                    <div class="map-card__desc">
                                        <span class="map-card__desc-text">${data.content.desc}<span>
                                        <span class="map-card__desc-price">${data.content.price}<span>
                                    </div>
                                </div>
                                <a href="${data.content.url}" class="map-card__link"></a>
                            </div>
                        </div>
                    </div>
                `;
            }
        },
        default: {
            builder: function() {
                // return new google.maps.InfoWindow(/*{ pixelOffset: new google.maps.Size(0, 0) }*/);
                // returned object must implement InfoWindow interface.
                return new GooglePopup();
            },
            render: function(data) {
                return `
                    <div class="map-bubble">
                        <div class="map-bubble__anchor">
                            <div class="map-bubble__content">
                                <div class="map-bubble__text${data.content.isMain ? ' map-bubble__text_icon' : ''}">
                                    ${data.content.isMain ? 
                                    `<div class="map-bubble__star">
                                        <img src="/images/map/star.svg" alt="">
                                    </div>`
                                    : '' }
                                    ${data.content.text}
                                </div>
                            </div>
                        </div>
                    </div>
                `;
            }
        }
    },
    label: {
        default: {'color': 'white', 'fontSize': '12'}
    }
};
