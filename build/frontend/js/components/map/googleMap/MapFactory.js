'use strict';

import GoogleMap from 'js/components/map/googleMap/GoogleMap';
import mapFactoryConfig from 'js/components/map/googleMap/MapFactoryConfig';

class MapFactory {
    static getAttributes(node) {
        return {
            popupType: node.getAttribute('data-with-popup'),
            cluster: node.hasAttribute('data-with-cluster'),
            labelType: node.getAttribute('data-with-label'),
            directions: node.hasAttribute('data-directions'),
            places: node.hasAttribute('data-with-places'),
            autocomplete: node.hasAttribute('data-with-autocomplete'),
            geocoder: node.hasAttribute('data-with-geocoder'),
            markerDraggable: node.hasAttribute('data-marker-draggable'),
            tokenizer: node.hasAttribute('data-with-tokenizer'),
            map: node.getAttribute('data-opts')
        };
    }

    static create(node) {
        const opts = {};
        const attributes = MapFactory.getAttributes(node);

        opts.images = mapFactoryConfig.images.default;
        opts.style = mapFactoryConfig.style.default;

        if (attributes.map) {
            opts.map = JSON.parse(attributes.map);
        }

        if (attributes.popupType)
            opts.popup = mapFactoryConfig.popup[attributes.popupType];

        if (attributes.cluster)
            opts.cluster = mapFactoryConfig.cluster.default;

        if (attributes.labelType)
            opts.label = mapFactoryConfig.label[attributes.labelType];

        if (attributes.directions)
            opts.directions = mapFactoryConfig.directions.default;

        if (attributes.places) {
            opts.places = mapFactoryConfig.places.default;
        }

        if (attributes.autocomplete) {
            opts.autocomplete = mapFactoryConfig.autocomplete.default;
        }

        if (attributes.geocoder) {
            opts.geocoder = mapFactoryConfig.geocoder.default;
        }

        if (attributes.tokenizer) {
            opts.tokenManager = mapFactoryConfig.tokenManager.default;
        }

        if (attributes.markerDraggable) {
            opts.markerDraggable = true;
        }

        return new GoogleMap(node, opts);
    }
}

export default MapFactory;
