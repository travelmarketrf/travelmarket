'use strict';

/**
 * Class for generating autocomplete session tokens.
 * 
 * @author Chistyakov Ilya <ichistyakovv@gmail.com>
 */
class Tokenizer {
    constructor() {
        this._currentId = 0;
        this._duration = 60 * 2 * 1000; // 120 seconds
        this._stamp = null;
        this._token = null;
    }

    refresh(id) {
        if (!id) throw new Error('The id paramater is required.');

        this._currentId = id;
        this._stamp = Date.now();
        this._token = new google.maps.places.AutocompleteSessionToken();

        return this._token;
    }

    get(id) {
        if (!id) throw new Error('The id paramater is required.');

        if (this._token && id === this._currentId && !this.isExpired())
            return this._token;

        return this.refresh(id);
    }

    isExpired() {
        return Date.now() > (this._stamp + this._duration);
    }
}

export default Tokenizer;
