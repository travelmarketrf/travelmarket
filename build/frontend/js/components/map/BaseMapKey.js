'use strict';

class BaseMapKey {
    constructor() {
        this.key = document.querySelector('meta[name="map-key"]');
    }

    get() {
        return this.key ? this.key.getAttribute('content') : null;
    }
}

export default BaseMapKey;
