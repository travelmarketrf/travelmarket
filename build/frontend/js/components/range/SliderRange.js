'use strict';

import NoUiSlider from 'noUiSlider';
import WNumb from 'wnumb';
import FilterWidgetInterface from 'js/components/FilterWidgetInterface';

class SliderRange extends FilterWidgetInterface {
    constructor(node) {
        super();

        this.node = node;

        if (!this.node) return;

        this.node['_component'] = this

        this.min = parseInt(this.node.getAttribute('data-min'), 10);
        this.max = parseInt(this.node.getAttribute('data-max'), 10);

        if(isNaN(this.min) || isNaN(this.max)) return;

        this.minInput = this.node.querySelector('.slider-range__input_min');
        this.maxInput = this.node.querySelector('.slider-range__input_max');
        this.minLabel = this.node.querySelector('.slider-range__value-start');
        this.maxLabel = this.node.querySelector('.slider-range__value-end');

        this.minInitial = this.minInput.value || this.min;
        this.maxInitial = this.maxInput.value || this.max;

        this.slider = NoUiSlider.create(this.node, {
            start: [this.minInitial, this.maxInitial],
            range: {
                'min': this.min,
                'max': this.max
            },
            connect: true,
            behavior: 'tap-drag',
            format: WNumb({
                decimals: 0
            })
        });

        this._init();
        this._bindEvents();
    }

    _bindEvents() {
        this.onSliderUpdate = this.onSliderUpdate.bind(this);
        this.onSliderSet = this.onSliderSet.bind(this);

        this.slider.on('set', this.onSliderSet, false);
        this.slider.on('update', this.onSliderUpdate, false);
    }

    _init() {
        this.minLabel.innerHTML = this.minInitial;
        this.maxLabel.innerHTML = this.maxInitial;
    }

    getValues() {
        return [+this.minInput.value, +this.maxInput.value];
    }

    onSliderSet(values) {
        this.minLabel.innerHTML = values[0];
        this.maxLabel.innerHTML = values[1];
        this.minInput.value = values[0];
        this.maxInput.value = values[1];

        this._dispatchChangeEvent();
    }

    onSliderUpdate(values) {
        this.minLabel.innerHTML = values[0];
        this.maxLabel.innerHTML = values[1];
    }

    _dispatchChangeEvent() {
        const event = new CustomEvent('range:change', {
            bubbles: true,
            cancelable: true,
            detail: {
                component: this,
                value: this.getValues()
            }
        });

        this.node.dispatchEvent(event);
    }

    /**
     * @inheritdoc
     */
    isActive() {
        const values = this.getValues();
        return values[0] !== this.min || values[1] !== this.max;
    }

    /**
     * @inheritdoc
     */
    getNode() {
        return this.node;
    }

    /**
     * @inheritdoc
     */
    reset() {
        if (!this.isActive())
            return false;

        this.slider.setHandle(0, this.min, false);
        this.slider.setHandle(1, this.max, false);
        this.minInput.value = this.min;
        this.maxInput.value = this.max;
        this.minLabel.innerHTML = this.min;
        this.maxLabel.innerHTML = this.max;

        return true;
    }
}

export default SliderRange;
