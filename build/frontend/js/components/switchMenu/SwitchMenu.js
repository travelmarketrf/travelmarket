'use strict';

class SwitchMenu {
    constructor(node) {
        this.node = node;

        if (!this.node) return;

        this.node['_component'] = this;

        this.bodyes = [...this.node.querySelectorAll('.menu-mobile__body')];

        this.onClick = this.onClick.bind(this);

        this.node.addEventListener('click', this.onClick, false);
    }

    onClick(e) {
        const item = e.target.closest('.menu-mobile__switch');

        if (!item) return;

        const name = item.getAttribute('data-name');

        this.unsetActiveBody();
        this.setActiveBody(name);
    }

    unsetActiveBody() {
        this.bodyes.forEach(body => body.classList.remove('menu-mobile__body_active'));
    }

    setActiveBody(name) {
        this.node.querySelector(`.menu-mobile__body[data-name="${name}"]`).classList.add('menu-mobile__body_active');
    }
}

export default SwitchMenu;
