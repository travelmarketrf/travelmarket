'use strict';

import Component from 'js/components/Component';
import Layout from 'js/helpers/Layout';
import Cookie from 'js/helpers/Cookie';

class ChatDialog extends Component {
    constructor(node) {
        super(node);

        this.onClick = this.onClick.bind(this);
        this._init = this._init.bind(this);

        this.node.addEventListener('click', this.onClick, false);
        Layout.body.addEventListener('app:init', this._init, false);
    }

    _init() {
        if (!Cookie.get('dialog')) return;

        this.setActiveDialog(this.getDialogById(Cookie.read('dialog')));
    }

    onClick(e) {
        const dialog = e.target.closest('.dialog');

        if (!dialog) return;

        this.unsetActiveDialog();
        this.setActiveDialog(dialog);
    }

    unsetActiveDialog() {
        [...this.node.querySelectorAll('.dialog_selected')]
            .forEach(dialog => dialog.classList.remove('dialog_selected'));
    }

    setActiveDialog(dialog) {
        dialog.classList.add('dialog_selected');

        Cookie.set(`dialog=${this.getDialogId(dialog)}`);
        this._dispatchSelectDialogEvent(dialog);
    }

    getDialogById(id) {
        return this.node.querySelector(`[data-id='${id}']`);
    }

    getDialogId(dialog) {
        return dialog.getAttribute('data-id');
    }

    _dispatchSelectDialogEvent(dialog) {
        const event = new CustomEvent('dialog:select', {
            bubbles: true,
            cancelable: true,
            detail: {
                component: this,
                id: this.getDialogId(dialog)
            }
        });

        Layout.body.dispatchEvent(event);
    }
}

export default ChatDialog;
