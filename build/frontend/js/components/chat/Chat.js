'use strict';

import Layout from 'js/helpers/Layout';
import Ajax from 'js/helpers/Ajax';

class Chat {
    constructor() {
        this.chatField = Layout.body.querySelector('[data-field="chat-field"]');

        this.onDialogSelect = this.onDialogSelect.bind(this);

        Layout.body.addEventListener('dialog:select', this.onDialogSelect, false);
    }

    // _init() {
    //     setTimeout(() => {
    //         this.chatWindow.scrollTop = this.chatWindow.scrollHeight;
    //     }, 100);
    // }

    onDialogSelect(e) {
        const id = e.detail.id; // for indentify dialog

        Ajax.get('/json/chat.json')
            .then(data => {
                this.removeActiveChat();
                this.generateChat(data);
            });
    }

    removeActiveChat() {
        this.chatField.innerHTML = '';
    }

    createChatWindow() {
        const chatNode = document.createElement('div');

        chatNode.classList.add('chat');
        chatNode.setAttribute('data-widget', 'chat');

        return chatNode;
    }

    scrollChat(chat) {
        const chatWindow = chat.querySelector('.chat__window');

        chatWindow.scrollTop = chatWindow.scrollHeight;
    }

    generateMessage(message) {
        switch (message.type) {
            case 'outgoing':
                return `
                    <div class="chat-message__message chat-message__message_outgoing">
                        <div class="chat-message__content">
                            <div class="chat-message__content-wrapper">${message.message}</div>
                        </div>

                        <div class="chat-message__group">
                            <div class="chat-message__photo">
                                <img src="${message.avatar}" alt="${message.avatarDesc}">
                            </div>

                            <div class="chat-message__time">${message.time}</div>
                        </div>
                    </div>
                `
                break;
            case 'incoming':
                return `
                    <div class="chat-message__message chat-message__message_incoming">
                        <div class="chat-message__content">
                            <div class="chat-message__content-wrapper">${message.message}</div>
                        </div>

                        <div class="chat-message__group">
                            <div class="chat-message__photo">
                                <img src="${message.avatar}" alt="${message.avatarDesc}">
                            </div>

                            <div class="chat-message__time">${message.time}</div>
                        </div>
                    </div>
                `
                break;
        }
    }

    generateChat(data) {
        const content = `
            <div class="chat-grid__group">
                <div class="chat-grid__title">Чат</div>

                <div class="chat-grid__back">
                    <svg width="16" height="14" viewBox="0 0 16 14" fill="none" xmlns="http://www.w3.org/2000/svg">
                        <path d="M9.8 12.3231C11.8 12.323 15 12.6872 15 8.68225C15 4.67725 11.4 5.04134 9.8 5.04134H1M1 5.04134L3.8 1.85547M1 5.04134L3.8 8.68225" stroke="#AEB8D5" stroke-width="2" stroke-linecap="round"
                            stroke-linejoin="round" />
                    </svg>

                    Вернуться назад
                </div>
            </div>

            <div class="chat__wrapper">
                <div class="chat__window">
                    <div class="chat-message">
                        ${data.messages.map(message => this.generateMessage(message)).join('')}
                    </div>

                    <div class="chat-field">
                        <div class="chat-field__wrapper">
                            <form action="#" class="form" autocomplete="off">
                                <textarea type="text" class="chat-field__textarea" placeholder="Введите сообщение"></textarea>

                                <div class="chat-field__send">
                                    <svg width="20" height="20" viewBox="0 0 20 20" fill="none" xmlns="http://www.w3.org/2000/svg">
                                        <g clip-path="url(#clip0)">
                                            <path
                                                d="M18.9724 0.464068L0.54879 10.1252C-0.20908 10.5224 -0.177988 11.1046 0.617193 11.4194L2.96931 12.349C3.76294 12.6631 4.9662 12.4928 5.6409 11.965L15.8562 3.90985C16.5278 3.38051 16.6001 3.45902 16.0163 4.08397L7.94016 12.7299C7.35485 13.3525 7.52897 14.1158 8.32493 14.426L8.60009 14.534C9.39605 14.8418 10.6973 15.3595 11.4893 15.6829L14.098 16.7447C14.8908 17.0673 15.5391 17.3346 15.5391 17.3393C15.5391 17.344 15.5437 17.3587 15.5476 17.3595C15.5515 17.3611 15.7388 16.6879 15.9627 15.8624L19.9409 1.2437C20.1648 0.41743 19.7303 0.0676436 18.9724 0.464068Z"
                                                fill="#AEB8D5" />
                                            <path
                                                d="M8.02337 15.4062L6.21847 14.6678C5.42718 14.3436 5.04786 14.7237 5.37588 15.5142C5.37666 15.5142 7.05174 19.5516 7.0051 19.6938C6.95691 19.8353 8.61257 17.2982 8.61257 17.2982C9.0805 16.5815 8.81467 15.7303 8.02337 15.4062Z"
                                                fill="#AEB8D5" />
                                        </g>
                                        <defs>
                                            <clipPath id="clip0">
                                                <rect width="20" height="20" fill="white" />
                                            </clipPath>
                                        </defs>
                                    </svg>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        `

        const chat = this.createChatWindow();
        chat.innerHTML = content;
        this.chatField.appendChild(chat);

        this.scrollChat(chat);

        this._dispatchCreateChatEvent();
    }

    _dispatchCreateChatEvent() {
        const event = new CustomEvent('chat:open', {
            bubbles: true,
            cancelable: true,
            detail: {
                component: this
            }
        });

        Layout.body.dispatchEvent(event);
    }
}

export default Chat;
