'use strict';

import MD from 'js/helpers/MD';
import Layout from 'js/helpers/Layout';

class ChatMobile {
    constructor(node) {
        this.node = node;

        if (!this.node || !MD.mobile()) return;

        this.node['_component'] = this;

        this._bindEvents();
        this._init();
    }

    _bindEvents() {
        this.items = this.node.querySelectorAll('.chat-grid__item');

        this.onDialogSelect = this.onDialogSelect.bind(this);
        this.onChatOpen = this.onChatOpen.bind(this);

        Layout.body.addEventListener('dialog:select', this.onDialogSelect, false);
        Layout.body.addEventListener('chat:open', this.onChatOpen, false);
    }

    _init() {
        this.setActiveItem(0);
    }

    onDialogSelect() {
        this.unsetActiveItem();
        this.setActiveItem(1);
    }

    onChatOpen() {
        this.onBackClick = this.onBackClick.bind(this);

        this.back = this.node.querySelector('.chat-grid__back');

        this.back.addEventListener('click', this.onBackClick, false);
    }

    onBackClick() {
        this.unsetActiveItem();
        this.setActiveItem(0);
    }

    unsetActiveItem() {
        [...this.items].forEach(item => item.classList.remove('chat-grid__item_select'));
    }

    setActiveItem(number) {
        this.items[number].classList.add('chat-grid__item_select');
    }
}

export default ChatMobile;
