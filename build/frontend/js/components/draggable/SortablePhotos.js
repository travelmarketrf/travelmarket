'use strict';

import { Sortable } from '@shopify/draggable/lib/draggable.bundle.legacy';
import Component from 'js/components/Component';
import MD from 'js/helpers/MD';

const ITEM_WIDTH = 134;

class SortablePhotos extends Component {
    constructor(node) {
        super(node);

        this._sortable = new Sortable(this.node, {
            draggable: '.draggable__item_source',
            classes: {
                'body:dragging': 'draggable-is-dragging',
                'mirror': 'draggable__item_mirror',
                'container:dragging': 'draggable__container_dragging',
                'source:dragging': 'draggable__item_dragging',
                'draggable:over': 'draggable__item_over',
                'container:over': 'draggable__container_over',
                'source:original': 'draggable__item_original',
                'source:placed': 'draggable__item_placed',
                'container:placed': 'draggable__container_placed'
            }
        });
    }

    _getItems() {
        return [...this.node.querySelectorAll('.draggable__item_source')];
    }

    getNode() {
        return this.node;
    }

    getCurrentData() {
        return this._getItems().map((item, idx) => {
            const input = item.querySelector('input');
            const img = item.querySelector('img');

            return {
                name: input.getAttribute('name') || input.getAttribute('data-name'),
                value: input.value,
                text: !idx ? `${img.outerHTML}` : ``
            };
        });
    }

    reset() {
        this._getItems().forEach(item => item.remove());
        this.node.classList.remove('draggable_active');
    }
}

export default SortablePhotos;
