'use strict';

import ActionMapper from 'js/helpers/ActionMapper';

const MAX_COUNT = 12;

/**
 * Class represents the slider graph component.
 * 
 * @class
 * 
 * @implements FilterOuterWidget interface
 * 
 * @fires events 'slider-graph:next', 'slider-graph:prev', 'slider-graph:change'
 * 
 * Data can be loaded dynamically in two ways. Either using
 * the setContent method, passing the raw html to be
 * inserted into the .slider-graph__columns node directly or
 * using the loadFromArray method, passing an array
 * in the format described below.
 *
 *  ```json
 *  [
 *      {
 *          "date": "01",
 *          "percent": 50.253,
 *          "value": "01.01.2019",
 *          "hint": "От 700$ на двоих",
 *          "isHighlighted": true,
 *          "month": "март"
 *      },
 *      {
 *          "date": "02",
 *          "percent": 75,
 *          "value": "02.01.2019",
 *          "hint": "От 700$ на двоих",
 *      }
 *  ]
 *  ```
 * 
 * @author Chistyakov Ilya <ichistyakovv@gmail.com>
 */
class SliderGraph {
    constructor(node) {
        this.node = node;

        if (!this.node) return;

        this.node['_component'] = this;

        this.columnsNode = this.node.querySelector('.slider-graph__columns');
        this.inputNode = this.node.querySelector('.slider-graph__input');

        this.actions = {
            'graph-prev': 'prev',
            'graph-next': 'next',
            'graph-set': 'onItemClick'
        };

        this._bindEvents();
        this._init();
    }

    _init() {
        const value = this.getValue();

        if (value)
            this._set(value);
    }

    _bindEvents() {
        this.onClick = this.onClick.bind(this);
        this.node.addEventListener('click', this.onClick, false);
    }

    onClick(e) {
        ActionMapper.callAction(this, this.actions, e.target);
    }

    prev() {
        const event = new CustomEvent('slider-graph:prev', {
            bubbles: true,
            cancelable: true,
            detail: {
                component: this
            }
        });

        this.node.dispatchEvent(event);
    }

    next() {
        const event = new CustomEvent('slider-graph:next', {
            bubbles: true,
            cancelable: true,
            detail: {
                component: this
            }
        });

        this.node.dispatchEvent(event);
    }

    onItemClick(node) {
        const value = node.getAttribute('data-value');

        this.setValue(value);
    }

    setValue(value) {
        if (this._set(value))
            this._dispathcChangeEvent();
    }

    _set(value) {
        const itemNode = this.getItemNodeByValue(value);

        if (!itemNode)
            return false;

        this.reset();

        itemNode.classList.add('slider-graph__column_selected');
        this.inputNode.value = value;

        return true;
    }

    _dispathcChangeEvent() {
        const event = new CustomEvent('slider-graph:change', {
            bubbles: true,
            cancelable: true,
            detail: {
                component: this
            }
        });

        this.node.dispatchEvent(event);
    }

    getValue() {
        return this.inputNode.value;
    }

    reset() {
        this.inputNode.value = '';

        const activeItemNode = this.getSecletedItem();

        if (activeItemNode)
            activeItemNode.classList.remove('slider-graph__column_selected');
    }

    getSecletedItem() {
        return this.columnsNode.querySelector('.slider-graph__column_selected');
    }

    setContent(content) {
        this.reset();

        this.columnsNode.innerHTML = content;

        this._dispathcChangeEvent();
    }

    loadFromArray(data) {
        let content = '';

        for (let i = 0; i < data.length; i++) {
            content += this._generateItem(data[i]);
        }

        this.setContent(content);
    }

    _generateItem(data) {
        return `
            <div class="slider-graph__column${data.isHighlighted ? ` slider-graph__column_highlight` : ``}" data-action="graph-set" data-value="${data.value}">
                <div class="slider-graph__rect" style="height: ${data.percent ? data.percent : 0}%;">
                    ${this._generateHint(data.hint)}
                </div>
                <div class="slider-graph__label">${data.date}</div>
                ${this._generateMonth(data.month)}
            </div>
        `;
    }

    _generateMonth(text) {
        if (!text) return ``;

        return `
            <div class="slider-graph__text">${text}</div>
        `;
    }

    _generateHint(text) {
        if (!text) return ``;

        return `
            <div class="slider-graph__hint hint">
                <div class="hint__content">
                    ${text}
                    <div class="hint__arrow"></div>
                </div>
            </div>
        `;
    }

    getItemNodeByValue(value) {
        return this.node.querySelector(`[data-value="${value}"]`);
    }

    getValue() {
        return this.inputNode.value;
    }

    /**
     * Method implements FilterOuterWidget interface.
     * 
     * @returns {Object} The name-value pairs of the widget hidden inputs.
     */
    getInputsData() {
        const inputsData = {};
        const name = this.inputNode.getAttribute('name');
        const value = this.getValue();

        inputsData[name] = value;

        return inputsData;
    }
}

export default SliderGraph;
