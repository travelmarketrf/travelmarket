'use strict';

import Layout from 'js/helpers/Layout';
import Widget from 'js/components/Widget';

class TourFilter {
    constructor() {
        this._bindEvents();
        this._init();
    }

    _bindEvents() {
        this.onWidgetChange = this.onWidgetChange.bind(this);
        Layout.body.addEventListener('slider-graph:change', this.onWidgetChange, false);
        Layout.body.addEventListener('sort-list:change', this.onWidgetChange, false);
    }

    _init() {
        const widgets = this.getOuterWidgets();
        const filter = this.getFilter();

        filter.copyInputsFrom(widgets);
    }

    onWidgetChange(e) {
        const widget = e.detail.component;
        const filter = this.getFilter();

        filter.copyInputsFrom([widget]);
    }

    getFilter() {
        const node = document.querySelector('[data-filter="tour"]');
        return Widget.get(node);
    }

    getOuterWidgets() {
        return [...document.querySelectorAll('[data-filter="outer"]')]
                .map(node => Widget.get(node))
                .filter(widget => widget);
    }
}

export default TourFilter;
