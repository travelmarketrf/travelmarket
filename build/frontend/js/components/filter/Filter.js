'use strict';

import ActionMapper from 'js/helpers/ActionMapper';
import Widget from 'js/components/Widget';

class Filter {
    constructor(node) {
        this.node = node;

        if (!this.node) return;

        this.node['_component'] = this;

        this.formNode = this.node.querySelector('form');

        this.actions = {
            'filter-reset': 'resetGroup',
            'filter-reset-all': 'resetAll',
            'choose-all': 'chooseGroup',
            'accept-filter': 'acceptFilter'
        };

        this._bindEvents();
        Widget.onReady(Widget.findNodes(this.node)).then(this._init);
    }

    _init() {
        this.getAllGroupNodes().forEach(groupNode => this._determineGroupState(groupNode));
    }

    _bindEvents() {
        this._init = this._init.bind(this);
        this.onClick = this.onClick.bind(this);
        this.onWidgetChange = this.onWidgetChange.bind(this);

        this.node.addEventListener('click', this.onClick, false);
        this.node.addEventListener('select-default:change', this.onWidgetChange, false);
        this.node.addEventListener('range:change', this.onWidgetChange, false);
        this.node.addEventListener('checkbox:change', this.onWidgetChange, false);
    }

    _determineGroupState(node) {
        this.isGroupActive(node) ? this.activateGroup(node) : this.disactivateGroup(node);
    }

    onWidgetChange(e) {
        const widget = e.detail.component;

        if (!Widget.isFilterable(widget))
            return;

        this._dispatchChangeEvent([widget]);

        const node = widget.getNode();
        const groupNode = this._getGroup(node);

        if (!groupNode) return;

        this._determineGroupState(groupNode);
    }

    onClick(e) {
        ActionMapper.callAction(this, this.actions, e.target);
    }

    isGroupActive(node) {
        const widgets = this.getActiveWidgetsInGroup(node);

        return !!widgets.length;
    }

    getAllGroupNodes() {
        return [...this.node.querySelectorAll('.filter__group')];
    }

    getActiveWidgetsInGroup(node) {
        return this._getFilterWidgets(node)
                .filter(widget => widget.isActive());
    }

    activateGroup(node) {
        const resetNode = this._getResetGroupButton(node);

        if (!resetNode) return;

        resetNode.classList.add('filter__reset_active');
    }

    acceptFilter() {
        // ---

        this._dispatchAcceptFilterEvent();
    }

    disactivateGroup(node) {
        const resetNode = this._getResetGroupButton(node);

        if (!resetNode) return;

        resetNode.classList.remove('filter__reset_active');
    }

    _getResetGroupButton(node) {
        return node.querySelector('.filter__reset');
    }

    _dispatchChangeEvent(widgets) {
        const event = new CustomEvent('filter:change', {
            bubbles: true,
            cancelable: true,
            detail: {
                component: this,
                widgets: widgets
            }
        });
        this.node.dispatchEvent(event);
    }

    _dispatchAcceptFilterEvent() {
        const event = new CustomEvent('filter:accept', {
            bubbles: true,
            cancelable: true,
            detail: {
                component: this
            }
        });
        this.node.dispatchEvent(event);
    }

    _getFilterWidgets(node) {
        return Widget
            .find(node)
            .filter(widget => Widget.isFilterable(widget));
    }

    _getGroupsByWidgets(widgets) {
        const groups = [];

        widgets
            .map(widget => this._getGroup(widget.getNode()))
            .forEach(group => group && !~groups.indexOf(group) && groups.push(group));

        return groups;
    }

    _reset(node) {
        const widgets = this._getFilterWidgets(node)
                            .filter(widget => widget.reset());

        if (!widgets.length) return;

        const groups = this._getGroupsByWidgets(widgets);

        groups.forEach(group => this.disactivateGroup(group));

        this._dispatchChangeEvent(widgets);
    }

    _getGroup(node) {
        return node.closest('.filter__group');
    }

    resetGroup(node) {
        const groupNode = this._getGroup(node);

        if (!groupNode) return;

        this._reset(groupNode);
    }

    resetAll() {
        this._reset(this.node);
    }

    chooseGroup(node) {
        const groupNode = this._getGroup(node);

        if (!groupNode) return;

        const widgets = this._getFilterWidgets(groupNode)
                            .filter(widget => widget.choose());

        if (!widgets.length) return;

        this.activateGroup(groupNode);
        this._dispatchChangeEvent(widgets);
    }

    removeOuterInputs() {
        [...this.formNode.querySelectorAll('[data-outer]')].forEach(node => node.remove());
    }

    /**
     * Copy inputs from the widgets placed outside the filter.
     *
     * @param {Array} widgets The widgets implementing the FilterOuterWidget interface.
     */
    copyInputsFrom(widgets) {
        for (let i = 0; i < widgets.length; i++) {
            const inputsData = widgets[i].getInputsData();

            for (let name in inputsData) {
                this._copyValueTo(name, inputsData[name]);
            }
        }

        this._dispatchChangeEvent(widgets);
    }

    _copyValueTo(name, value) {
        let inputNode = this.node.querySelector(`[name="${name}"]`);

        if (!inputNode) {
            inputNode = this._createHiddenInput(name);
        }

        if (value)
            return inputNode.value = value;

        inputNode.remove();
    }

    _createHiddenInput(name) {
        const input = document.createElement('input');

        input.setAttribute('type', 'hidden');
        input.setAttribute('data-outer', '');
        input.setAttribute('name', name);

        this.formNode.insertBefore(input, this.formNode.firstChild);

        return input;
    }
}

export default Filter;
