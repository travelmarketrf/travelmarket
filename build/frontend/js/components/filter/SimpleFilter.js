'use strict';

class SimpleFilter {
    constructor(node) {
        this.node = node;

        if (!this.node) return;

        this.node['_component'] = this;

        this.input = this.node.querySelector('input[type="hidden"]');

        this.onClick = this.onClick.bind(this);

        this.node.addEventListener('click', this.onClick, false);
    }

    onClick(e) {
        const item = e.target.closest('.simple-filter__item');

        if (!item) return;

        this.unsetActiveItem();
        this.setActiveItem(item);
        this.setInputValue(item);
    }

    unsetActiveItem() {
        [...this.node.querySelectorAll('.simple-filter__item_selected')]
            .forEach(node => node.classList.remove('simple-filter__item_selected'));
    }

    setActiveItem(item) {
        item.classList.add('simple-filter__item_selected');
    }

    setInputValue(item) {
        this.input.value = item.getAttribute('data-value');
    }
}

export default SimpleFilter;
