'use strict';

const SORT_ASC = 'asc';
const SORT_DESC = 'desc';

/**
 * Class represents the sort list widget.
 * 
 * @implements FilterOuterWidget interface
 */
class SortList {
    constructor(node) {
        this.node = node;

        if (!this.node) return;

        this.node['_component'] = this;

        this.inputType = this.node.querySelector('.sorting-list__input_type');
        this.inputOrder = this.node.querySelector('.sorting-list__input_desc');

        this._bindEvents();
        this._init();
    }

    _init() {
        if (this.inputType.value)
            this._set(this.inputType.value, this.inputOrder.value);
    }

    _bindEvents() {
        this.onClick = this.onClick.bind(this);

        this.node.addEventListener('click', this.onClick, false);
    }

    onClick(e) {
        const itemNode = e.target.closest('[data-value]');

        if (!itemNode) return;

        const type = itemNode.getAttribute('data-value');
        let order;

        if (this.isSelected(type)) {
            order = itemNode.classList.contains('sorting-list__item_order_desc') ? SORT_ASC : SORT_DESC;
        } else {
            order = itemNode.classList.contains('sorting-list__item_order_desc') ? SORT_DESC : SORT_ASC;
        }

        this.set(type, order);
    }

    set(type, order) {
        if (this._set(type, order))
            this._dispatchChangeEvent();
    }

    _set(type, order) {
        const itemNode = this.getItemNodeByValue(type);

        if (!itemNode || (this.isSelected(type) && order === this.getSortOrder()))
            return false;

        if (!this._isAvailableSortOrder(order))
            order = SORT_ASC;

        this.reset();

        itemNode.classList.add('sorting-list__item_selected');

        if (order === SORT_DESC)
            itemNode.classList.add('sorting-list__item_order_desc');

        this.inputType.value = type;
        this.inputOrder.value = order;

        return true;
    }

    getType() {
        return this.inputType.value;
    }

    getSortOrder() {
        return this.inputOrder.value;
    }

    reset() {
        const activeItemNode = this.node.querySelector('.sorting-list__item_selected');

        if (!activeItemNode) return;

        activeItemNode.classList.remove('sorting-list__item_selected');
        activeItemNode.classList.remove('sorting-list__item_order_desc');

        this.inputType.value = '';
        this.inputOrder.value = '';
    }

    _isAvailableSortOrder(order) {
        order = order.toLowerCase();
        return order === SORT_ASC || order === SORT_DESC;
    }

    _dispatchChangeEvent() {
        const event = new CustomEvent('sort-list:change', {
            bubbles: true,
            cancelable: true,
            detail: {
                component: this
            }
        });

        this.node.dispatchEvent(event);
    }

    isSelected(type) {
        const itemNode = this.getItemNodeByValue(type);

        if (!itemNode)
            return false;

        return itemNode.classList.contains('sorting-list__item_selected');
    }

    getItemNodeByValue(value) {
        return this.node.querySelector(`[data-value="${value}"]`);
    }

    /**
     * Method implements FilterOuterWidget interface.
     * 
     * @returns {Object} The name-value pairs of the widget hidden inputs.
     */
    getInputsData() {
        const inputsData = {};
        const typeName = this.inputType.getAttribute('name');
        const orderName = this.inputOrder.getAttribute('name');

        inputsData[typeName] = this.getType();
        inputsData[orderName] = this.getSortOrder();

        return inputsData;
    }
}

export default SortList;
