'use strict';

import EventHelper from 'js/helpers/EventHelper';

class Resize {
    constructor() {
        this.onRefreshUI = this.onRefreshUI.bind(this);

        window.addEventListener('resize', this.onRefreshUI, false);
    }

    onRefreshUI() {
        EventHelper.refreshUI();
    }
}

export default Resize;
