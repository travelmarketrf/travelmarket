'use strict';

import tinymce from 'tinymce';
import uuidv4 from 'uuid/v4';
import Component from 'js/components/Component';
import tinyDefaults from 'js/components/editors/tinyDefaults';

/**
 * Class implements the editor component using the tinymce plugin.
 * 
 * @class
 * 
 * @see https://www.tiny.cloud/docs/configure/
 * 
 * @author Chistyakov Ilya <ichistyakovv@gmail.com>
 */
class TinyEditor extends Component {
    constructor(node) {
        super(node);

        this.options = Object.assign({ selector: this._generateTextarea() }, tinyDefaults);

        tinymce.init(this.options)
               .then(editors => this._init(editors))
               .catch(err => alert(err.message));
    }

    /**
     * Initialize the component.
     * 
     * @private
     * @param {Editor[]} editors Instances of the editor.
     * @throws {Error} Throw the error if the instance of tinymce is not defined.
     */
    _init(editors) {
        this._tinymce = editors[0];

        if (!this._tinymce)
            throw new Error('The TinyMCE instance is undefined.');

        // Tinymce doesn't save changes automatically.
        // So we need to do it by ourselves.
        this._tinymce.on('Change', () => this._tinymce.save());
        this._tinymce.on('Undo', () => this._tinymce.save());
        this._tinymce.on('Redo', () => this._tinymce.save());
    }

    /**
     * Create a textarea node with unique id.
     * 
     * @private
     * @returns {string} Textarea id selector.
     */
    _generateTextarea() {
        const textarea = this.node.querySelector('textarea') || document.createElement('textarea');
        const id = `textarea-${uuidv4()}`;

        textarea.id = id;

        if (!textarea.parentElement)
            this.node.appendChild(textarea);

        return `#${id}`;
    }
}

export default TinyEditor;
