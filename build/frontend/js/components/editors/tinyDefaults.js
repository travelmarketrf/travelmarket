'use strict';

export default {
    height: 233,
    theme_url: '/tiny/themes/silver/theme.min.js',
    skin_url: '/tiny/skins/oxide/ui/oxide',
    content_css: '/tiny/skins/oxide/ui/oxide/content.min.css',
    branding: false,
    external_plugins: {
        "table": "/tiny/plugins/table/plugin.js",
        "lists": "/tiny/plugins/lists/plugin.js",
        "link": "/tiny/plugins/link/plugin.js"
    },
    toolbar: 'undo redo | styleselect | bold | numlist bullist | h1 h2 h3 h4 h5 h6 | link | forecolor',
    menubar: "file | edit | table",
    custom_colors: false,
    color_map: [
        "FF6F61", "Red"
    ],
    language_url: '/tiny/langs/ru.js',
    language: 'ru',
    mobile: {
        theme_url: '/tiny/themes/mobile/theme.min.js'
    }
};
