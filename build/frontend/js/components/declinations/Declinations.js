'use strict';

const DECLINATIONS_COUNT = 3;

class Declinations {
    constructor(values) {
        this.values = values;
    }

    trans(number) {
        if (!this.values || this.values.length !== DECLINATIONS_COUNT) return '';

        number = Math.abs(number) % 100;

        if (number >= 5 && number <= 20) {
            return this.values[2];
        }

        number %= 10;

        if (number == 1) {
            return this.values[0];
        }

        if (number >= 2 && number <= 4) {
            return this.values[1];
        }

        return this.values[2];
    }
}

export default Declinations;
