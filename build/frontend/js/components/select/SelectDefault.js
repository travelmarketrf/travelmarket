'use strict';

import Stringz from 'js/Helpers/Stringz';
import Layout from 'js/helpers/Layout';
import DomHelper from 'js/helpers/DomHelper';
import FilterWidgetInterface from 'js/components/FilterWidgetInterface';
import DetectVisibleArea from 'js/helpers/DetectVisibleArea';

class SelectDefault extends FilterWidgetInterface {
    constructor(node) {
        super();

        this.node = node;

        if (!this.node) return;

        this.node['_component'] = this;

        this._autoOpen = this.node.getAttribute('data-auto-open') === 'false' ? false : true;
        this._isValueEditable = this.node.hasAttribute('data-editable');
        this.field = this.node.querySelector('.select__field');
        this.list = this.node.querySelector('.select__list');
        this.listContainer = this.list.parentElement;
        this.dropdown = this.node.querySelector('.select__dropdown');
        this.label = this.node.querySelector('.select__field-text');
        this.input = this.node.querySelector('input[type="hidden"]:not([data-foreign])');
        this._counterNode = this.node.querySelector('.select__field-counter') || null;
        this._crossNode = this.node.querySelector('.select__field-icon_cross') ||  null;
        this._isExternal = this.node.hasAttribute('data-select-external');

        this.keys = {};

        this._init();
        this._bindEvents();
    }

    _init() {
        this._onDetect();

        if (!this.isActive()) return;

        this.label.value = this.getValue();
        this.node.classList.remove('select_empty');
    }

    _bindEvents() {
        this.onFieldClick = this.onFieldClick.bind(this);
        this.onListClick = this.onListClick.bind(this);
        this.onLabelChange = this.onLabelChange.bind(this);
        this.remove = this.remove.bind(this);
        this._onDetect = this._onDetect.bind(this);

        this.field.addEventListener('click', this.onFieldClick, false);
        this.list.addEventListener('click', this.onListClick, false);
        this.label.addEventListener('input', this.onLabelChange, false);
        window.addEventListener('scroll', this._onDetect, false);

        if (this.isRemovable())
            this._crossNode.addEventListener('click', this.remove, false);
    }

    isAutoOpenable() {
        return this._autoOpen;
    }

    disableAutoOpen() {
        this._autoOpen = false;

        return this;
    }

    makeEditable() {
        this._isValueEditable = true;

        return this;
    }

    _onDetect() {
        if (DetectVisibleArea.detect(this.node, this.list)) {
            this.list.classList.add('select__list_top');
        } else {
            DetectVisibleArea.refresh(this.list);
            this.list.classList.remove('select__list_top');
        }
    }

    isEditable() {
        return this._isValueEditable;
    }

    onLabelChange() {
        if (this.label.value.length === 0) {
            this.node.classList.add('select_empty');
        } else {
            this.node.classList.remove('select_empty');
        }

        if (this.isEditable())
            this.input.value = this.getLabelValue();

        this._dispatchInputEvent();
    }

    onListClick(event) {
        const item = event.target.closest('.select__item');

        if (!item) return;

        this._clearSelections();

        const value = item.getAttribute('data-value');
        const text = item.getAttribute('data-text');

        this.label.value = text;
        this.input.value = value;
        this.node.classList.remove('select_empty');
        item.classList.add('select__item_active');

        this._dispatchChangeEvent();
        this.close();
    }

    getActiveItem() {
        return this.dropdown.querySelector('.select__item_active');
    }

    _clearSelections() {
        [...this.dropdown.querySelectorAll('.select__item_active')].forEach(node => node.classList.remove('select__item_active'));
    }

    isSelected() {
        return this.node.classList.contains('select_selected');
    }

    onFieldClick() {
        if (!this.isAutoOpenable()) return;

        this.label.focus();
        this.open();
    }

    load(data) {
        this.dropdown.innerHTML = '';

        data.items.forEach(item => {
            this.dropdown.innerHTML += `
                <li class="select__item" data-value="${item.value}" data-text="${item.name}"${item.data ? this._buildDataAttrs(item.data) : ''}>
                ${
                    item.type ?
                    `<span class="select__item-icon">
                        ${this.getIconBytype(item.type)}
                    </span>` :
                    ''
                }
                    <span class="select__item-text">${item.name}</span>
                </li>
            `;
        });
    }

    _buildDataAttrs(data) {
        const attrs = [];

        for (let name in data) {
            attrs.push(`data-${name}="${data[name]}"`);
        }

        return attrs.join(' ');
    }

    open() {
        if (this.isOpen()) return;

        if (this._isExternal)
            this._detachList();

        DetectVisibleArea.detect(this.node, this.list) ? this.list.classList.add('select__list_top') : null;

        this.node.classList.add('select_selected');

        const event = new CustomEvent('select:open', {
            bubbles: true,
            cancelable: true,
            detail: {
                component: this
            }
        });

        this.node.dispatchEvent(event);
    }

    _detachList() {
        const wrapper = document.createElement('div');
        const coords = this.node.getBoundingClientRect();
        const offsetTop = DomHelper.scrollTop() + coords.top;
        const offsetRight = DomHelper.windowWidth() - (coords.left + this.node.offsetWidth);

        this.list.style.width = `${this.list.offsetWidth}px`;
        this.list.style.left = 'auto';
        this.list.style.marginTop = `${getComputedStyle(this.list).marginTop}`;

        wrapper.classList.add('select-wrapper');
        wrapper.appendChild(this.list);
        Layout.root.appendChild(wrapper);

        wrapper.style.top = `${offsetTop}px`;
        wrapper.style.right = `${offsetRight}px`;
    }

    close() {
        setTimeout(() => { DetectVisibleArea.refresh(this.list) }, 300);

        this.node.classList.remove('select_selected');
        this.list.classList.remove('select__list_top');

        if (this._isExternal)
            this._attachList();

        const event = new CustomEvent('select:close', {
            bubbles: true,
            cancelable: true,
            detail: {
                component: this
            }
        });

        this.node.dispatchEvent(event);
    }

    _attachList() {
        const container = this.list.parentElement;

        this.list.style.width = '';
        this.list.style.left = '';
        this.list.style.marginTop = '';

        this.listContainer.appendChild(this.list);
        container.remove();
    }

    isOpen() {
        return this.node.classList.contains('select_selected');
    }

    isRemovable() {
        return !!this._crossNode;
    }

    isCountable() {
        return !!this._counterNode;
    }

    makeRemovable() {
        if (this.isRemovable()) return;

        this._crossNode = document.createElement('div');

        this._crossNode.classList.add('select__field-icon', 'select__field-icon_cross');
        this._crossNode.innerHTML = `<svg width="26" height="26" viewBox="0 0 26 26" fill="none" xmlns="http://www.w3.org/2000/svg">
        <path fill-rule="evenodd" clip-rule="evenodd" d="M6.94307 6.47686C7.33359 6.08633 7.96675 6.08633 8.35728 6.47686L13.2103 11.3299L18.0718 6.46828C18.4624 6.07775 19.0955 6.07775 19.4861 6.46827C19.8766 6.85879 19.8766 7.49196 19.4861 7.88249L14.6245 12.7441L19.4776 17.5972C19.8681 17.9877 19.8681 18.6209 19.4776 19.0114C19.0871 19.4019 18.4539 19.4019 18.0634 19.0114L13.2103 14.1583L8.3494 19.0192C7.95887 19.4097 7.32571 19.4097 6.93519 19.0192C6.54467 18.6286 6.54468 17.9955 6.9352 17.605L11.7961 12.7441L6.94307 7.89107C6.55254 7.50055 6.55254 6.86738 6.94307 6.47686Z" fill="#AEB8D5"/>
    </svg>`;

        this.field.classList.add('select__field_cross');
        this.field.appendChild(this._crossNode);
        this._crossNode.addEventListener('click', this.remove, false);

        return this;
    }

    makeCountable() {
        if (this.isCountable()) return;

        this._counterNode = document.createElement('div');

        this._counterNode.classList.add('select__field-counter');
        this.field.classList.add('select__field_counter');
        this.field.appendChild(this._counterNode);

        return this;
    }

    setCounter(value) {
        if (!this.isCountable())
            this.makeCountable();

        this._counterNode.innerHTML = value;

        return this;
    }

    getCounter() {
        if (!this.isCountable())
            return null;

        return this._counterNode.innerHTML;
    }

    remove() {
        this._dispatchRemoveEvent();
    }

    hideArrow() {
        this.node.classList.add('select_no-select_no-arrow');

        return this;
    }

    clone() {
        return this.node.outerHTML;
    }

    setInputName(name) {
        this.input.setAttribute('name', name);

        return this;
    }

    setInputValueByName(name, value) {
        const input = this.node.querySelector(`[name="${name}"]`);

        if (!input) {
            this._appendInput(name, value);
            return this;
        }

        input.value = value;

        return this;
    }

    getInputValueByName(name) {
        const input = this.node.querySelector(`[name="${name}"]`);

        if (!input) return null;

        return input.value;
    }

    getCurrentData() {
        return [{
            name: this.getName(),
            value: Stringz.stripTags( this.getValue() ),
            text: Stringz.stripTags( this.getLabelValue() )
        }];
    }

    _appendInput(name, value) {
        const input = document.createElement('input');

        input.setAttribute('name', name);
        input.setAttribute('type', 'hidden');
        input.setAttribute('data-foreign', '');
        input.value = value;

        this.node.appendChild(input);
    }

    setKey(name, value) {
        this.keys[name] = value;

        return this;
    }

    getKey(name) {
        return this.keys[name];
    }

    getLabelValue() {
        return this.label.value;
    }

    getValue() {
        return this.input.value;
    }

    getName() {
        return this.input.getAttribute('name') || this.input.getAttribute('data-name');
    }

    _dispatchChangeEvent() {
        const event = new CustomEvent('select-default:change', {
            bubbles: true,
            cancelable: true,
            detail: {
                component: this,
                value: this.getValue()
            }
        });
        this.node.dispatchEvent(event);
    }

    _dispatchInputEvent() {
        const event = new CustomEvent('select-default:input', {
            bubbles: true,
            cancelable: true,
            detail: {
                component: this,
                value: this.getValue()
            }
        });
        this.node.dispatchEvent(event);
    }

    _dispatchRemoveEvent() {
        const event = new CustomEvent('select:remove', {
            bubbles: true,
            cancelable: true,
            detail: {
                component: this
            }
        });
        this.node.dispatchEvent(event);
    }

    isTargetInside(target) {
        return target.closest('.select') === this.node;
    }

    /**
     * @inheritdoc
     */
    isActive() {
        return this.input.value !== '';
    }

    /**
     * @inheritdoc
     */
    getNode() {
        return this.node;
    }

    /**
     * @inheritdoc
     */
    reset() {
        if (!this.isActive())
            return false;

        this.input.value = '';
        this.label.value = '';

        this.node.classList.add('select_empty');

        return true;
    }

    static createEmpty() {
        return `
            <div class="select" data-widget="select-default">
                <div class="select__container">
                    <div class="select__field">
                        <input type="text" class="select__field-text">
                    </div>

                    <div class="select__list">
                        <div class="select__content">
                            <ul class="select__dropdown">

                            </ul>
                        </div>

                        <span class="select__list-arrow"></span>
                    </div>
                </div>

                <input type="hidden">
            </div>
        `;
    }
}

export default SelectDefault;
