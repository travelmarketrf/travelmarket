'use strict';

import Ajax from 'js/helpers/Ajax';
import DetectVisibleArea from 'js/helpers/DetectVisibleArea';
import Layout from 'js/helpers/Layout';
import Component from 'js/components/Component';

class SelectCheckbox extends Component {
    constructor(node) {
        super(node);

        this.field = this.node.querySelector('.select__field');
        this.list = this.node.querySelector('.select__list');
        this.dropdown = this.node.querySelector('.select__dropdown');
        this.label = this.node.querySelector('.select__field-text');
        this.input = this.node.querySelector('input[type="hidden"]');
        this.languages = [];

        this._bindEvents();
        this._init();
    }

    _init() {
        if (!this.isActive()) return;

        this.label.value = this.getValue();
    }

    _bindEvents() {
        this.onFieldClick = this.onFieldClick.bind(this);
        this.onListClick = this.onListClick.bind(this);
        this._onDetect = this._onDetect.bind(this);

        this.field.addEventListener('click', this.onFieldClick, false);
        this.list.addEventListener('click', this.onListClick, false);
        window.addEventListener('scroll', this._onDetect, false);
    }

    _toEmpty() {
        this.node.classList.add('select_empty');
    }

    _toFilled() {
        this.node.classList.remove('select_empty');
    }

    isLabelEmpty() {
        return this.label.value.length === 0;
    }

    _onDetect() {
        if (DetectVisibleArea.detect(this.node, this.list)) {
            this.list.classList.add('select__list_top');
        } else {
            DetectVisibleArea.refresh(this.list);
            this.list.classList.remove('select__list_top');
        }
    }

    onListClick(event) {
        const item = event.target.closest('.select__item');

        if (!item) return;

        const value = item.getAttribute('data-value');
        const checkbox = item.querySelector('.checkbox__input');

        if (this.isCheckboxChosen(value)) {
            this._removeCheckbox(value, checkbox);
        } else {
            this._addCheckbox(value, checkbox);
        }
    }

    isCheckboxChosen(value) {
        return ~this.languages.indexOf(value);
    }

    load(data) {
        this.dropdown.innerHTML = '';

        data.items.forEach((item, index) => {
            this.dropdown.innerHTML += `
                <li class="select__item" data-value="${item.name}" data-text="${item.name}">
                    <div class="checkbox" data-widget="checkbox">
                        <input id="custom-checkbox-${index}" type="checkbox" class="checkbox__input" name="${item.name}">
                        <label for="custom-checkbox-${index}" class="checkbox__label">
                            <span class="checkbox__checkbox-custom"></span>
                        </label>
                    </div>

                    <div class="select__item-text">${item.name}</div>
                </li>
            `;
        });
    }

    isSelected() {
        return this.node.classList.contains('select_selected');
    }

    _removeCheckbox(value, checkbox) {
        checkbox.removeAttribute('checked');

        this.languages.splice(this.languages.indexOf(value), 1);

        this.label.value = this.languages.join(', ');

        this.isLabelEmpty() ? this._toEmpty() : this._toFilled();
    }

    _addCheckbox(value, checkbox) {
        checkbox.setAttribute('checked', true);
        this.languages.push(value);

        this.label.value = this.languages.join(', ');

        this._toFilled();
    }

    onFieldClick() {
        if (this.isSelected()) return;

        Ajax.get('/json/select-checkbox.json')
            .then(data => {
                this.load(data);
                this.open();
            });
    }

    getValue() {
        return this.input.value;
    }

    isTargetInside(target) {
        return target.closest('.select') === this.node;
    }

    open() {
        if (this.isSelected()) return;

        DetectVisibleArea.detect(this.node, this.list) ? this.list.classList.add('select__list_top') : null;

        this.node.classList.add('select_selected');

        const event = new CustomEvent('select-checkbox:open', {
            bubbles: true,
            cancelable: true,
            detail: {
                component: this
            }
        });

        this.node.dispatchEvent(event);
    }

    close() {
        setTimeout(() => { DetectVisibleArea.refresh(this.list) }, 300);

        this.node.classList.remove('select_selected');
        this.list.classList.remove('select__list_top');

        const event = new CustomEvent('select-checkbox:close', {
            bubbles: true,
            cancelable: true,
            detail: {
                component: this
            }
        });

        this.node.dispatchEvent(event);
    }

    isActive() {
        return this.input.value !== '';
    }
}

export default SelectCheckbox;
