'use strict';

import Ajax from 'js/helpers/Ajax';
import DetectVisibleArea from 'js/helpers/DetectVisibleArea';
import Layout from 'js/helpers/Layout';

const ITEM_COUNTRY = 'country';
const ITEM_CITY = 'city';

class Select {
    constructor(node) {
        this.node = node;

        if (!this.node) return;

        this.node['_component'] = this;

        this.field = this.node.querySelector('.select__field');
        this.list = this.node.querySelector('.select__list');
        this.dropdown = this.node.querySelector('.select__dropdown');
        this.label = this.node.querySelector('.select__field-text');
        this.input = this.node.querySelector('input[type="hidden"]');

        this._init();
        this._bindEvents();
    }

    _init() {
        this._onDetect();

        if (!this.isActive()) return;

        this.label.value = this.getValue();
    }

    _bindEvents() {
        this.onFieldClick = this.onFieldClick.bind(this);
        this.onListClick = this.onListClick.bind(this);
        this.onLabelChange = this.onLabelChange.bind(this);
        this._onDetect = this._onDetect.bind(this);

        this.label.addEventListener('input', this.onLabelChange, false);
        this.field.addEventListener('click', this.onFieldClick, false);
        this.list.addEventListener('click', this.onListClick, false);
        window.addEventListener('scroll', this._onDetect, false);
    }

    // fetch
    onLabelChange() {
        if (this.label.value.length === 0) {
            this.node.classList.add('select_empty');
            this.close();
            return;
        } else {
            this.node.classList.remove('select_empty');
        }

        Ajax.get('/json/select.json')
            .then(data => {
                this.load(data);
                this.open();
            });
    }

    _onDetect() {
        if (DetectVisibleArea.detect(this.node, this.list)) {
            this.list.classList.add('select__list_top');
        } else {
            DetectVisibleArea.refresh(this.list);
            this.list.classList.remove('select__list_top');
        }
    }

    load(data) {
        this.dropdown.innerHTML = '';

        data.items.forEach(item => {
            this.dropdown.innerHTML += `
                <li class="select__item" data-value="${item.name}" data-text="${item.name}">
                    ${
                        item.type ?
                        `<span class="select__item-icon">
                            ${this.getIconBytype(item.type)}
                        </span>` :
                        ''
                    }

                    <span class="select__item-text">${item.name}</span>
                </li>
            `;
        });
    }

    getIconBytype(type) {
        let iconHTML;

        switch (type) {
            case ITEM_COUNTRY: iconHTML = `
                            <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" xml:space="preserve" width="4.7828mm" height="4.7828mm" version="1.1" shape-rendering="geometricPrecision"
                                text-rendering="geometricPrecision" image-rendering="optimizeQuality" fill-rule="evenodd" clip-rule="evenodd" viewBox="0 0 476 476">
                                <g id="Layer_x0020_1">
                                    <path fill="#E5E5E5"
                                        d="M383 374c1,-1 -2,-1 -4,-1 -2,0 -3,-1 -4,-1 -4,-2 -7,-1 -7,-2l-3 -7 -11 -8c0,0 -1,-1 -2,-1l-9 0 -3 3 -17 -11c-1,-1 -2,-1 -3,0l-9 -1c-2,0 -3,1 -4,1l-7 5 -6 4 -4 7 -7 -4 -9 76 0 -1c43,-5 80,-26 109,-59zm-271 -211c1,2 0,5 -3,6l-6 -4 -1 33c0,0 1,0 1,0l9 17 33 51 -2 0 10 8c0,1 0,2 1,3l-9 -24 -3 -9 8 3 14 21 19 34 12 9 63 23c0,0 0,1 0,2l23 26 -11 2 -22 -45 0 4c-3,0 -5,0 -5,0l6 -21 -19 7c-18,18 -38,-51 -15,-51 0,0 1,-1 1,-2 0,0 -1,0 -1,-1 -1,-1 -1,-1 -1,-1 16,16 26,-18 43,-1 18,0 23,42 23,2 -8,-3 -8,-17 1,-17l9 -4 8 -14 -4 -3 35 -36c33,-17 5,-13 -8,-13 0,0 1,-1 1,-2l3 -5c6,-6 30,-15 39,-6l8 -8c19,0 4,-12 4,-19l-25 -20 -2 -4 -6 4c-1,1 -1,2 -1,2 -1,0 -1,-1 -2,0l-2 1 -8 3 -6 -12c0,0 1,0 2,0 0,-50 -36,41 -36,41 0,0 0,1 -1,1l-5 3c-31,16 -51,-62 -51,-62 18,3 31,-24 31,-24 -11,11 -47,-7 -47,-7 1,1 2,1 3,2l6 7c-3,3 -24,13 -34,3 -2,0 -62,-10 -12,-10 7,7 -4,13 -8,18l-8 -6c-25,-25 -97,75 -78,75l5 5 21 15zm365 76c0,133 -107,238 -238,238 -133,0 -238,-107 -238,-238 0,-133 107,-238 238,-238 131,0 238,106 238,238z" />
                                </g>
                            </svg>
                        `;
                        break;
            default: iconHTML = `
                        <svg width="18" height="16" viewBox="0 0 18 16" fill="none" xmlns="http://www.w3.org/2000/svg">
                            <path
                                d="M2.27609 0.39061C2.01566 0.130361 1.70137 0 1.33342 0C0.965323 0 0.651077 0.130471 0.39061 0.39061C0.130288 0.651077 0 0.96536 0 1.33342C0 1.83338 0.222121 2.21538 0.666619 2.47924V15.6668C0.666619 15.7571 0.699529 15.8352 0.76564 15.9012C0.831569 15.9671 0.90972 16 0.999984 16H1.6666C1.75687 16 1.83498 15.9673 1.90095 15.9012C1.96695 15.8352 1.99989 15.7571 1.99989 15.6668V2.47924C2.44447 2.21523 2.66651 1.83334 2.66651 1.33342C2.66651 0.965469 2.53637 0.651223 2.27609 0.39061Z"
                                fill="#E5E5E5" />
                            <path
                                d="M17.8021 1.53146C17.6701 1.3995 17.5137 1.3335 17.3332 1.3335C17.243 1.3335 17.0537 1.40636 16.7655 1.55226C16.4774 1.69816 16.1717 1.86129 15.8488 2.04182C15.5262 2.22238 15.1439 2.38547 14.7032 2.53138C14.2622 2.67728 13.8541 2.75014 13.4793 2.75014C13.1319 2.75014 12.8264 2.68414 12.5623 2.55224C11.6873 2.14248 10.9272 1.83688 10.2812 1.63548C9.63542 1.43412 8.94103 1.3335 8.19794 1.3335C6.91315 1.3335 5.45138 1.75008 3.81254 2.5834C3.40963 2.7848 3.13537 2.93413 2.98954 3.03133C2.77421 3.19114 2.6665 3.38199 2.6665 3.60426V11.3333C2.6665 11.5139 2.73261 11.6703 2.86458 11.8021C2.99637 11.9341 3.15274 12.0001 3.33331 12.0001C3.44437 12.0001 3.55878 11.9689 3.67696 11.9064C5.56587 10.8925 7.17698 10.3856 8.51032 10.3856C9.01721 10.3856 9.51199 10.462 9.99447 10.6147C10.4771 10.7675 10.8782 10.9342 11.1976 11.1146C11.5172 11.2951 11.8938 11.4619 12.3278 11.6147C12.7621 11.7672 13.1909 11.8437 13.6144 11.8437C14.6839 11.8437 15.9652 11.4411 17.4582 10.6354C17.6458 10.5382 17.7828 10.4428 17.8696 10.349C17.9564 10.2553 17.9999 10.1217 17.9999 9.94789V1.99997C17.9999 1.81948 17.934 1.66336 17.8021 1.53146Z"
                                fill="#E5E5E5" />
                        </svg>
                    `;
        }

        return iconHTML;
    }

    onListClick(event) {
        const item = event.target.closest('.select__item');

        if (!item) return;

        const value = item.getAttribute('data-value');
        const text = item.getAttribute('data-text');

        this.label.value = text;
        this.input.value = value;
        this.close();
    }

    isSelected() {
        return this.node.classList.contains('select_selected');
    }

    onFieldClick(event) {
        this.label.focus();
    }

    isTargetInside(target) {
        return target.closest('.select') === this.node;
    }

    getValue() {
        return this.input.value;
    }

    open() {
        if (this.isSelected()) return;

        DetectVisibleArea.detect(this.node, this.list) ? this.list.classList.add('select__list_top') : null;

        this.node.classList.add('select_selected');

        const event = new CustomEvent('select:open', {
            bubbles: true,
            cancelable: true,
            detail: {
                component: this
            }
        });

        this.node.dispatchEvent(event);
    }

    close() {
        setTimeout(() => { DetectVisibleArea.refresh(this.list) }, 300);

        this.node.classList.remove('select_selected');
        this.list.classList.remove('select__list_top');

        const event = new CustomEvent('select:close', {
            bubbles: true,
            cancelable: true,
            detail: {
                component: this
            }
        });

        this.node.dispatchEvent(event);
    }

    isActive() {
        return this.input.value !== '';
    }
}

export default Select;
