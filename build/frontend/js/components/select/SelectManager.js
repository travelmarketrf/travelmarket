'use strict';

import Layout from 'js/helpers/Layout';

class SelectManager {
    constructor() {
        this.currentSelect = null;

        this.onSelectOpen = this.onSelectOpen.bind(this);
        this.onSelectClose = this.onSelectClose.bind(this);
        this.onClick = this.onClick.bind(this);
        Layout.body.addEventListener('select:open', this.onSelectOpen, false);
        Layout.body.addEventListener('select:close', this.onSelectClose, false);
        Layout.body.addEventListener('select-checkbox:open', this.onSelectOpen, false);
        Layout.body.addEventListener('select-checkbox:close', this.onSelectClose, false);
        Layout.body.addEventListener('click', this.onClick,false);
    }

    onClick(e) {
        if (!this.hasActive()) return;

        if (this.currentSelect.isTargetInside(e.target))
            return;

        this.closeActive();
    }

    hasActive() {
        return !!this.currentSelect;
    }

    closeActive() {
        if (!this.hasActive()) return;

        this.currentSelect.close();
    }

    onSelectOpen(e) {
        this.closeActive();

        this.currentSelect = e.detail.component;
    }

    onSelectClose(e) {
        this.currentSelect = null;
    }
}

export default SelectManager;
