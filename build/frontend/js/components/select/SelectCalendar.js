'use strict';

import Component from 'js/components/Component';
import Widget from 'js/components/Widget';
import ElementFaker from 'js/helpers/ElementFaker';

const calendar_types = ['range', 'single'];

class SelectCalendar extends Component {
    constructor(node) {
        super(node);

        this._field = this.node.querySelector('.select__field');
        this._fieldText = this.node.querySelector('.select__field-text');
        this._crossNode = this.node.querySelector('.select__field-icon_cross') ||  null;
        [this._calendarNode] = calendar_types.map(type => this.node.querySelector(`[data-widget="calendar-${type}"]`)).filter(node => !!node);
        this._calendar = null;

        this._promise = Widget.onReady([this._calendarNode]);

        this._promise.then(widgets => this._init(widgets));
        this._bindEvents();
    }

    _init(widgets) {
        this._calendar = widgets[0];

        if (this.isActive()) {
            this.node.classList.remove('select_empty');
        }
    }

    _bindEvents() {
        this.remove = this.remove.bind(this);
        this.onLabelChange = this.onLabelChange.bind(this);

        if (this.isRemovable())
            this._crossNode.addEventListener('click', this.remove, false);

        this._fieldText.addEventListener('input', this.onLabelChange, false);
    }

    isRemovable() {
        return !!this._crossNode;
    }

    onLabelChange() {
        if (this._fieldText.value.length === 0) {
            this.node.classList.add('select_empty');
        } else {
            this.node.classList.remove('select_empty');
        }
    }

    makeRemovable() {
        if (this.isRemovable()) return;

        this._crossNode = document.createElement('div');

        this._crossNode.classList.add('select__field-icon', 'select__field-icon_cross');
        this._crossNode.innerHTML = `<svg width="26" height="26" viewBox="0 0 26 26" fill="none" xmlns="http://www.w3.org/2000/svg">
        <path fill-rule="evenodd" clip-rule="evenodd" d="M6.94307 6.47686C7.33359 6.08633 7.96675 6.08633 8.35728 6.47686L13.2103 11.3299L18.0718 6.46828C18.4624 6.07775 19.0955 6.07775 19.4861 6.46827C19.8766 6.85879 19.8766 7.49196 19.4861 7.88249L14.6245 12.7441L19.4776 17.5972C19.8681 17.9877 19.8681 18.6209 19.4776 19.0114C19.0871 19.4019 18.4539 19.4019 18.0634 19.0114L13.2103 14.1583L8.3494 19.0192C7.95887 19.4097 7.32571 19.4097 6.93519 19.0192C6.54467 18.6286 6.54468 17.9955 6.9352 17.605L11.7961 12.7441L6.94307 7.89107C6.55254 7.50055 6.55254 6.86738 6.94307 6.47686Z" fill="#AEB8D5"/>
    </svg>`;

        this._field.classList.add('select__field_cross');
        this._field.appendChild(this._crossNode);
        this._crossNode.addEventListener('click', this.remove, false);

        return this;
    }

    clone() {
        const clonedNode = this.node.cloneNode(true);
        const listNode = clonedNode.querySelector('.flatpickr-calendar');

        if (listNode) listNode.remove();

        const html = ElementFaker.getHtml(clonedNode);

        return html;
    }

    remove() {
        this._dispatchRemoveEvent();
    }

    _dispatchRemoveEvent() {
        const event = new CustomEvent('select:remove', {
            bubbles: true,
            cancelable: true,
            detail: {
                component: this
            }
        });
        this.node.dispatchEvent(event);
    }

    isActive() {
        return this._calendar.isActive();
    }

    reset() {
        this._promise.then(() => {
            this._calendar.reset();
            this.node.classList.add('select_empty');
        });
    }

    getNode() {
        return this.node;
    }
}

export default SelectCalendar;
