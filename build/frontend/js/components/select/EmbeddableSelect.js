'use strict';

import Declinations from 'js/components/declinations/Declinations';
import Widget from 'js/components/Widget';
import Layout from 'js/helpers/Layout';
import DetectVisibleArea from 'js/helpers/DetectVisibleArea';

class EmbeddableSelect {
    constructor(node) {
        this.node = node;

        if (!this.node) return;

        this.node['_component'] = this;

        this.field = this.node.querySelector('.select__field');
        this.label = this.node.querySelector('.select__field-text');
        this.dropdown = this.node.querySelector('.select__dropdown');
        this.input = this.node.querySelector('.select__input');
        this.list = this.node.querySelector('.select__list');

        this._init();
        this._bindEvents();
    }

    _init() {
        this.declText = this.node.getAttribute('data-decl') ? JSON.parse(this.node.getAttribute('data-decl')) : null;
        this.node.removeAttribute('data-decl');
        this.declinations = new Declinations(this.declText);
    }

    _bindEvents() {
        this.onFieldClick = this.onFieldClick.bind(this);
        this.onCheckedInput = this.onCheckedInput.bind(this);
        this.onCounterChange = this.onCounterChange.bind(this);
        this._onDetect = this._onDetect.bind(this);
        this.onInit = this.onInit.bind(this);
        this.field.addEventListener('click', this.onFieldClick, false);
        this.node.addEventListener('counter:change', this.onCounterChange, false);
        this.node.addEventListener('input:change', this.onCheckedInput, false);
        Layout.body.addEventListener('app:init', this.onInit, false);
        window.addEventListener('scroll', this._onDetect, false);
    }

    onInit() {
        this.onCheckedInput();

        const counters = this.getAllCounters();
        const sum = this._calcSum(counters);

        this.setValue(sum);
    }

    onCounterChange(e) {
        const sum = this._calcSum(this.getAllCounters());

        this.setValue(sum);
    }

    _calcSum(counters) {
        return counters.reduce((sum, c) => sum + c.getValue(), 0);
    }

    _onDetect() {
        if (DetectVisibleArea.detect(this.node, this.list)) {
            this.list.classList.add('select__list_top');
        } else {
            DetectVisibleArea.refresh(this.list);
            this.list.classList.remove('select__list_top');
        }
    }

    setValue(sum) {
        this._setText(`${sum ? sum : ''} ${this.declinations.trans(sum)}`);
        this._setValue(sum);
    }

    onCheckedInput(e) {
        const counter = this.getFirstCounter();
        const switchWidget = this.getFirstSwitch();

        if (!counter || !switchWidget) return;

        switchWidget.isChecked() ? counter.setStep(2) : counter.setStep(1);
    }

    getFirstCounter() {
        return Widget.get(this.node.querySelector('[data-widget="counter"]'));
    }

    getFirstSwitch() {
        return Widget.get(this.node.querySelector('[data-widget="switch"]'));
    }

    getAllCounters() {
        return [...this.node.querySelectorAll('[data-widget="counter"]')].map(node => Widget.get(node));
    }

    isSelected() {
        return this.node.classList.contains('select-embeddable_selected');
    }

    onFieldClick() {
        this.isSelected() ? this.close() : this.open();
    }

    _setText(text) {
        this.label.innerHTML = text;
    }

    _getText() {
        return this.getFirstCounter().getText();
    }

    _setValue(value) {
        this.input.value = value;
    }

    getName() {
        return this.input.getAttribute('name') || this.input.getAttribute('data-name');
    }

    isTargetInside(target) {
        return target.closest('.select-embeddable') === this.node;
    }

    getValue() {
        return this.input.value;
    }

    open() {
        DetectVisibleArea.detect(this.node, this.node.querySelector('.select__list')) ? this.node.querySelector('.select__list').classList.add('select__list_top') : null;

        this.node.classList.add('select-embeddable_selected');

        const event = new CustomEvent('select:open', {
            bubbles: true,
            cancelable: true,
            detail: {
                component: this
            }
        });

        this.node.dispatchEvent(event);
    }

    close() {
        setTimeout(() => { DetectVisibleArea.refresh(this.node.querySelector('.select__list')); }, 300);

        this.node.classList.remove('select-embeddable_selected');
        this.node.querySelector('.select__list').classList.remove('select__list_top');

        const event = new CustomEvent('select:close', {
            bubbles: true,
            cancelable: true,
            detail: {
                component: this
            }
        });

        this.node.dispatchEvent(event);
    }

    isActive() {
        return this.input.value !== '';
    }

    getCurrentData() {
        return [{
            name: this.getName(),
            value: this.getValue(),
            text: this._getText()
        }];
    }

    getNode() {
        return this.node;
    }

    reset() {
        this.setValue(0);
    }
}

export default EmbeddableSelect;
