'use strict';

import Widget from 'js/components/Widget';
import EventHelper from 'js/helpers/EventHelper';
import ActionMapper from 'js/helpers/ActionMapper';
import Layout from 'js/helpers/Layout';
import patterns from 'js/components/addingFields/patterns';

const CELL_WIDE_LEN_THRESHOLD = 150;

class AddingFields {
    constructor() {
        this.actions = {
            'add-block': 'addBlock',
            'clone-widget': 'cloneWidget',
            'gather-list': 'gatherList',
            'gather-table': 'gatherTable',
            'remove-list-item': 'removeListItem',
            'remove-table': 'removeTable'
        };

        this._bindEvents();
    }

    _bindEvents() {
        this.onClick = this.onClick.bind(this);
        this.onRemove = this.onRemove.bind(this);

        Layout.body.addEventListener('click', this.onClick, false);
        Layout.body.addEventListener('select:remove', this.onRemove, false);
        Layout.body.addEventListener('input:remove', this.onRemove, false);
    }

    onClick(e) {
        ActionMapper.callAction(this, this.actions, e.target);
    }

    gatherTable(node) {
        const isPrevented = this._dispatchGatherTableEvent(node);

        if (isPrevented) return;

        const table = node.getAttribute('data-target-table');
        const fields = node.getAttribute('data-target-fields');

        if (!table || !fields) return;

        const tableContainer = document.querySelector(`[data-table="${table}"]`);
        const fieldsNode = document.querySelector(`[data-fields="${fields}"]`);

        if (!tableContainer || !fieldsNode) return;

        const widgets = Widget.find(fieldsNode);
        const cells = this._getTableCells(widgets);

        if (!cells.length) return;

        for (let i = 0; i < widgets.length; i++) {
            widgets[i].reset && widgets[i].reset();
        }

        const tableNode = this._createTable({cells});

        tableContainer.appendChild(tableNode);

        this._updateInputIndices([...tableContainer.children]);

        EventHelper.refreshUI(null, tableContainer);
    }

    _updateInputIndices(items = []) {
        for (let i = 0; i < items.length; i++) {
            const inputs = [...items[i].querySelectorAll('input')];

            for (let j = 0; j < inputs.length; j++) {
                let name = inputs[j].getAttribute('name');
                let matchedPattern;

                for (let pattern in patterns) {
                    if (patterns[pattern].test(name)) {
                        matchedPattern = patterns[pattern];
                        break;
                    }
                }

                if (matchedPattern) {
                    name = name.replace(matchedPattern, `[${i}][]`);
                } else {
                    name += `[${i}][]`;
                }

                inputs[j].setAttribute('name', name);
            }
        }
    }

    _dispatchGatherTableEvent(node) {
        const event = new CustomEvent('add-fields:table', {
            bubbles: true,
            cancelable: true,
            detail: {
                target: node
            }
        });
        return !node.dispatchEvent(event);
    }

    _getTableCells(widgets) {
        const cells = [];

        for (let i = 0; i < widgets.length; i++) {
            if (!widgets[i].getNode || !widgets[i].getCurrentData)
                continue;

            const node = widgets[i].getNode();
            const separator = node.getAttribute('data-table-separator');
            const isSkip = node.hasAttribute('data-table-skip');

            if (isSkip) continue;

            // take either the defined col number or the last number. If neither then take 1.
            const col = parseInt(node.getAttribute('data-table-col'), 10) || cells.length || 1;
            const order = parseInt(node.getAttribute('data-table-order'), 10) - 1;

            if (!cells[col]) cells[col] = [];

            const cellData = {parts: [], separator};

            cellData.parts = widgets[i].getCurrentData();

            // if (!cellData.name || !cellData.value || !cellData.text)
            //     return [];

            if (!isNaN(order) && cells[col][order]) {
                cells[col].splice(order, 0, cellData);
            } else if (!isNaN(order)) {
                cells[col][order] = cellData;
            } else {
                cells[col].push(cellData);
            }
        }

        return cells;
    }

    _createTable(data) {
        const node = document.createElement('div');

        node.classList.add('section-table');

        node.innerHTML = `
            <div class="section-table__container">
                <div class="section-table__scroll" data-widget="scrollbar-compensator">
                    <div class="section-table__body">
                        ${this._renderTableCells(data.cells)}
                    </div>
                </div>
            </div>

            <div class="section-table__delete" data-action="remove-table">
                <svg width="26" height="26" viewBox="0 0 26 26" fill="none" xmlns="http://www.w3.org/2000/svg">
                    <path fill-rule="evenodd" clip-rule="evenodd"
                        d="M6.94307 6.47686C7.33359 6.08633 7.96675 6.08633 8.35728 6.47686L13.2103 11.3299L18.0718 6.46828C18.4624 6.07775 19.0955 6.07775 19.4861 6.46827C19.8766 6.85879 19.8766 7.49196 19.4861 7.88249L14.6245 12.7441L19.4776 17.5972C19.8681 17.9877 19.8681 18.6209 19.4776 19.0114C19.0871 19.4019 18.4539 19.4019 18.0634 19.0114L13.2103 14.1583L8.3494 19.0192C7.95887 19.4097 7.32571 19.4097 6.93519 19.0192C6.54467 18.6286 6.54468 17.9955 6.9352 17.605L11.7961 12.7441L6.94307 7.89107C6.55254 7.50055 6.55254 6.86738 6.94307 6.47686Z"
                        fill="#AEB8D5" />
                </svg>
            </div>
        `;

        return node;
    }

    _renderTableCells(cells) {
        return cells.map(cell => {
            return cell.reduce((result, item, idx) => {
                if (item.separator === null)
                    item.separator = ', ';

                // check merely first item and index
                if (!(item.parts[0] && item.parts[0].text) || !idx)
                    item.separator = '';

                result.content += item.separator;

                for (let i = 0; i < item.parts.length; i++) {
                    const part = item.parts[i];
                    const isImg = ~part.text.indexOf('<img');
                    const text = part.text.trim();

                    result.len += isImg ? 0 : text.length;
                    result.content += `<input type="hidden" name="${part.name}" value="${part.value}">${ isImg ? `<div class="section-table__img">${text}</div>` : !i || !text ? text : `, ${text}` }`;
                }

                return result;
            }, {len: 0, content: ''});
        }).map(cell => `<div class="section-table__cell${cell.len > CELL_WIDE_LEN_THRESHOLD ? ' section-table__cell_wide' : ''}">${cell.content}</div>`)
          .join('');
    }

    gatherList(node) {
        const isPrevented = this._dispatchGatherListEvent(node);

        if (isPrevented) return;

        const list = node.getAttribute('data-target-list');
        const fields = node.getAttribute('data-target-fields');

        if (!list || !fields) return;

        const listNode = document.querySelector(`[data-list="${list}"]`);
        const fieldsNode = document.querySelector(`[data-fields="${fields}"]`);

        if (!listNode || !fieldsNode) return;

        const widgets = Widget.find(fieldsNode)
                              .filter(widget => widget.getNode && !widget.getNode().hasAttribute('data-list-skip'));
        const data = [];

        for (let i = 0; i < widgets.length; i++) {
            if (!widgets[i].getCurrentData) continue;

            const widgetData = widgets[i].getCurrentData()[0];

            // if (!widgetData.name || !widgetData.value || !widgetData.text)
            //     return;

            data.push(widgetData);
        }

        if (!data.length) return;

        for (let i = 0; i < widgets.length; i++) {
            widgets[i].reset && widgets[i].reset();
        }

        const isListFake = listNode.hasAttribute('data-list-fake');
        const listItemNode = this._createListItem(data, isListFake);

        listNode.appendChild(listItemNode);
    }

    _dispatchGatherListEvent(node) {
        const event = new CustomEvent('add-fields:list', {
            bubbles: true,
            cancelable: true,
            detail: {
                target: node
            }
        });
        return !node.dispatchEvent(event);
    }

    _createListItem(data, isFakeName) {
        const node = document.createElement('li');

        node.classList.add('simple-list__item');

        node.innerHTML = `
            ${this._renderListItemText(data)}

            <svg width="26" height="26" viewBox="0 0 26 26" fill="none" xmlns="http://www.w3.org/2000/svg" data-action="remove-list-item">
                <path fill-rule="evenodd" clip-rule="evenodd"
                    d="M6.94307 6.47686C7.33359 6.08633 7.96675 6.08633 8.35728 6.47686L13.2103 11.3299L18.0718 6.46828C18.4624 6.07775 19.0955 6.07775 19.4861 6.46827C19.8766 6.85879 19.8766 7.49196 19.4861 7.88249L14.6245 12.7441L19.4776 17.5972C19.8681 17.9877 19.8681 18.6209 19.4776 19.0114C19.0871 19.4019 18.4539 19.4019 18.0634 19.0114L13.2103 14.1583L8.3494 19.0192C7.95887 19.4097 7.32571 19.4097 6.93519 19.0192C6.54467 18.6286 6.54468 17.9955 6.9352 17.605L11.7961 12.7441L6.94307 7.89107C6.55254 7.50055 6.55254 6.86738 6.94307 6.47686Z"
                    fill="#AEB8D5" />
            </svg>
            ${this._renderListItemInputs(data, isFakeName)}
        `;

        return node;
    }

    _renderListItemText(data) {
        return data.map(item => item.text).join(' ');
    }

    _renderListItemInputs(data, isFakeName) {
        return data.map(item => `<input type="hidden" ${isFakeName ? 'data-' : ''}name="${item.name}" value="${item.value}">`).join('');
    }

    addBlock(node) {
        const target = node.getAttribute('data-target');

        if (!target) return;

        const btnStateClassName = node.getAttribute('data-state');

        if (btnStateClassName)
            node.classList.add(btnStateClassName);

        const anchor = document.querySelector(`[data-anchor="${target}"]`);

        if (!anchor) return;

        const anchorStateClassName = anchor.getAttribute('data-state');

        anchor.classList.remove(anchorStateClassName);
    }

    cloneWidget(node) {
        const target = node.getAttribute('data-target');
        const container = node.getAttribute('data-container');

        if (!target || !container) return;

        const widgetNode = [...document.querySelectorAll(`[data-clonable="${target}"]`)][0];
        const containerNode = document.querySelector(`[data-clonable-container="${container}"]`);

        if (!widgetNode || !containerNode) return;

        const widget = Widget.get(widgetNode);

        if (!widget || !widget.clone) return;

        const formGroupNode = this._createFormGroup();

        formGroupNode.innerHTML = widget.clone();
        containerNode.appendChild(formGroupNode);

        EventHelper.refreshUI(null, containerNode)
                    .then(() => {
                        const clonedWidget = Widget.find(formGroupNode)[0];

                        if (!clonedWidget) return;

                        clonedWidget.makeRemovable && clonedWidget.makeRemovable();
                        clonedWidget.reset && clonedWidget.reset();
                    });
    }

    _createFormGroup() {
        const node = document.createElement('div');

        node.classList.add('form-group', 'created-group');
        node.setAttribute('data-behavior', 'overlay');
        return node;
    }

    onRemove(e) {
        if (e.defaultPrevented) return;

        const node = e.detail.component.getNode();
        const groupNode = node.closest('.form-group');

        groupNode ? groupNode.remove() : node.remove();
    }

    removeListItem(node) {
        const itemNode = node.closest('.simple-list__item');

        if (itemNode)
            itemNode.remove();
    }

    removeTable(node) {
        const tableNode = node.closest('.section-table');
        const containerNode = node.closest('[data-table]');

        if (tableNode)
            tableNode.remove();

        if (containerNode)
            this._updateInputIndices([...containerNode.children]);
    }
}

export default AddingFields;
