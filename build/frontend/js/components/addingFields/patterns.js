'use strict';

export default {
    'indexed': /\[\d\]\[\]/,
    'unindexed': /\[\]/
};
