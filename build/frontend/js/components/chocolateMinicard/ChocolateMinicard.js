'use strict';

import Widget from 'js/components/Widget';
import Component from 'js/components/Component';

class ChocolateMinicard extends Component {
    constructor(node) {
        super(node);

        this.toursNode = this.node.querySelector('.chocolate-minicard__tours');
        this.tableNode = this.node.querySelector('[data-widget="creative-table"]');

        Widget.onReady([this.tableNode]).then(widgets => this._init(widgets));

        this._bindEvents();
    }

    _bindEvents() {
        this.onToursClick = this.onToursClick.bind(this);

        this.toursNode.addEventListener('click', this.onToursClick, false);
    }

    _init(widgets) {
        this._table = widgets[0];
    }

    onToursClick() {
        if (!this.isOpen()) {
            this.activeButton();
            this._table.showTable();
        } else {
            this.disableButton();
            this._table.hideTable();
        }
    }

    isOpen() {
        return this.toursNode.classList.contains('chocolate-minicard__tours_active');
    }

    activeButton() {
        this.toursNode.classList.add('chocolate-minicard__tours_active');
    }

    disableButton() {
        this.toursNode.classList.remove('chocolate-minicard__tours_active');
    }
}

export default ChocolateMinicard;
