'use strict';

import ActionMapper from 'js/helpers/ActionMapper';

class FoldableList {
    constructor(node) {
        this.node = node;

        if (!this.node) return;

        this.node['_component'] = this;

        this.actions = {
            'list-open': 'unfold',
            'list-close': 'fold'
        };

        this.container = this.node.querySelector('.foldable-list__container');
        this.hidden = this.node.querySelector('.foldable-list__hidden');

        this._bindEvents();
    }

    _bindEvents() {
        this.onClick = this.onClick.bind(this);
        this.node.addEventListener('click', this.onClick, false);
    }

    onClick(e) {
        ActionMapper.callAction(this, this.actions, e.target);
    }

    unfold() {
        this.hidden.style.maxHeight = this.hidden.scrollHeight + 'px';
        this.node.classList.add('foldable-list_open');
    }

    fold() {}

    isOpen() {
        return this.container.classList.contains('foldable-list_open');
    }
}

export default FoldableList;
