'use strict';

import Component from 'js/components/Component';
import DomHelper from 'js/helpers/DomHelper';

class ScrollbarCompensator extends Component {
    constructor(node) {
        super(node);

        this._bindEvents();
        this.onResize();
    }

    _bindEvents() {
        this.onResize = this.onResize.bind(this);

        window.addEventListener('resize', this.onResize, false);
    }

    onResize() {
        const sizes = DomHelper.getScrollbarSizes(this.node);
        
        this.node.style.marginBottom = '';

        const marginBottom = parseInt(getComputedStyle(this.node).marginBottom, 10) || 0;

        this.node.style.marginBottom = `${marginBottom - sizes.height}px`;
    }
}

export default ScrollbarCompensator;
