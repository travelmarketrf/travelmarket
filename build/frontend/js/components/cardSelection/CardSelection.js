'use strict';

class CardSelection {
    constructor(node) {
        this.node = node;

        if (!this.node) return;

        this.node['_component'] = this;

        this.onClick = this.onClick.bind(this);

        this.hiddenInput = this.node.querySelector('input[type="hidden"]');

        this.node.addEventListener('click', this.onClick, false);
    }

    onClick(e) {
        const item = e.target.closest('.card-selection__item');

        if (!item) return;

        const itemValue = item.getAttribute('data-value');

        this.unsetActiveItem();
        this.setActiveItem(item);
        this.setInputValue(itemValue);
    }

    unsetActiveItem() {
        [...this.node.querySelectorAll('.card-selection__item_selected')]
            .forEach(card => card.classList.remove('card-selection__item_selected'));
    }

    setActiveItem(item) {
        item.classList.add('card-selection__item_selected');

        this._dispatchSelectedItemEvent();
    }

    setInputValue(itemValue) {
        this.hiddenInput.value = itemValue;
    }

    _dispatchSelectedItemEvent() {
        const event = new CustomEvent('card-selection:select', {
            bubbles: true,
            cancelable: true,
            detail: {
                component: this
            }
        });

        this.node.dispatchEvent(event);
    }
}

export default CardSelection;
