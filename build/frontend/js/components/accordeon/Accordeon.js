class Accordeon {
  constructor(node) {
    this.node = node;

    if (!this.node) return;

    this.node['_component'] = this;

    this.onAccordeonItemClick = this.onAccordeonItemClick.bind(this);

    this.node.addEventListener('click', this.onAccordeonItemClick, false);
  }

  onAccordeonItemClick(e) {
    const header = e.target.closest('.accordeon__header');

    if (!header) return;

    const parent = header.parentNode;

    if(this.isItemOpen(parent)) {
      this.unsetActiveItem();
    } else {
      this.unsetActiveItem();
      this.setActiveItem(parent);
    }
  }

  isItemOpen(parent) {
    return parent.classList.contains('accordeon__item_open');
  }

  unsetActiveItem() {
    [...this.node.querySelectorAll('.accordeon__item')].forEach(item => {
      item.classList.remove('accordeon__item_open');
      item.querySelector('.accordeon__content').style.maxHeight = null;
    });
  }

  setActiveItem(item) {
    item.classList.add('accordeon__item_open');

    this.slideToggle(item.querySelector('.accordeon__content'));

    this._dispatchAccordeonOpenEvent(item.scrollHeight);
  }

  slideToggle(item) {
    item.style.maxHeight = item.scrollHeight + 'px';
  }

  _dispatchAccordeonOpenEvent(height) {
      const event = new CustomEvent('accordeon:open', {
          bubbles: true,
          cancelable: true,
          detail: {
              component: this,
              height: height
          }
      });

      this.node.dispatchEvent(event);
  }
}

export default Accordeon;
