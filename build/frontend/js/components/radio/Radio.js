'use strict';

import Layout from 'js/helpers/Layout';

class Radio {
    constructor(node) {
        this.node = node;

        if (!this.node) return;

        this.node['_component'] = this;

        this.onRadioClick = this.onRadioClick.bind(this);

        this.hiddenInput = this.node.querySelector('input[type="hidden"]');

        Layout.body.addEventListener('custom-radio:click', this.onRadioClick, false);
    }

    onRadioClick(e) {
        this.hiddenInput.value = e.detail.value;
    }
}

export default Radio;
