'use strict';

class StarsRating {
    constructor(node) {
        this.node = node;

        if (!this.node) return;

        this.node['_component'] = this;

        this.onStarClick = this.onStarClick.bind(this);

        this.hiddenInput = this.node.querySelector('input[type="hidden"]');

        this.node.addEventListener('click', this.onStarClick, false);

        this._init();
    }

    _init() {
        this.setNodeDataValue(this.hiddenInput.value);
    }

    onStarClick(e) {
        const item = e.target.closest('.hotel-rating__star');

        if (!item) return;

        const rating = this.getRatingValue(item);

        this.setNodeDataValue(rating);
        this.setHiddenInputValue(rating);
    }

    setNodeDataValue(rating) {
        this.node.setAttribute('data-stars', rating);
    }

    setHiddenInputValue(rating) {
        this.hiddenInput.value = rating;
    }

    getRatingValue(item) {
        return item.getAttribute('data-rating');
    }
}

export default StarsRating;
