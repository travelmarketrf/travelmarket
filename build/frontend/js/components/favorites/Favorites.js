'use strict';

class Favorites {
    constructor(node) {
        this.node = node;

        if (!this.node) return;

        this.node['_component'] = this;

        this.onClick = this.onClick.bind(this);

        this.node.addEventListener('click', this.onClick, false);
    }

    onClick() {
        if (this.isActive()) {
            this.unsetActiveItem();
        } else {
            this.setActiveItem();
        }
    }

    isActive() {
        return this.node.classList.contains('agency-aside__favorites_active');
    }

    unsetActiveItem() {
        this.node.classList.remove('agency-aside__favorites_active');
    }

    setActiveItem() {
        this.node.classList.add('agency-aside__favorites_active');
    }
}

export default Favorites;
