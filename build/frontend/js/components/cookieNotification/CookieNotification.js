'use strict';

import Cookie from 'js/helpers/Cookie';

class CookieNotification {
    constructor() {
        this.node = document.querySelector('.cookie-minipopup');

        this.onCloseClick = this.onCloseClick.bind(this);

        this.close = this.node.querySelector('.cookie-minipopup__close');

        Cookie.get('cookie-accept') ? null : this.showCookieNotification();

        this.close.addEventListener('click', this.onCloseClick, false);
    }

    onCloseClick() {
        Cookie.set('cookie-accept=true');

        this.node.classList.add('cookie-minipopup_close');
    }

    showCookieNotification() {
        this.node.classList.remove('cookie-minipopup_close');
    }
}

export default CookieNotification;
