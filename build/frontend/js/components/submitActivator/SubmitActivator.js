'use strict';

class SubmitActivator {
    constructor(node) {
        this.node = node;

        if (!this.node) return;

        this.node['_component'] = this;

        this.onActiveButton = this.onActiveButton.bind(this);

        this.nextButton = this.node.querySelector('.form-button');

        this.node.addEventListener('card-selection:select', this.onActiveButton, false);
    }

    onActiveButton() {
        this.nextButton.classList.remove('form-button_disabled');
    }
}

export default SubmitActivator;
