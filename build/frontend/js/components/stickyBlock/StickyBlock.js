'use strict';

import DomHelper from 'js/helpers/DomHelper';

class StickyBlock {
    constructor(node) {
        this.node = node;

        if (!this.node) return;

        this.node['_component'] = this;

        this.block = this.node.querySelector('.sticky-block_fixed');

        this._bindEvents();
    }

    _bindEvents() {
        this.onScroll = this.onScroll.bind(this);

        window.addEventListener('scroll', this.onScroll, false);
    }

    onScroll() {
        const scrollTop = DomHelper.scrollTop();
        const containerBottom = this.node.getBoundingClientRect().top + scrollTop + this.node.offsetHeight;
        const imgBottom = this.node.getBoundingClientRect().top + 2 * scrollTop + this.block.offsetHeight;

        if (imgBottom > containerBottom) {
            this.block.classList.remove('sticky-block_fixed');

            this.block.classList.add('sticky-block_bottom');
        } else {
            this.block.classList.add('sticky-block_fixed');

            this.block.classList.remove('sticky-block_bottom');
        }
    }
}

export default StickyBlock;
