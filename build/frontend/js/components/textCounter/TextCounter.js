'use strict';

import Declinations from 'js/components/declinations/Declinations';
import Layout from 'js/helpers/Layout';

class TextCounter {
    constructor(node) {
        this.node = node;

        if (!this.node) return;

        this.node['_component'] = this;

        this.input = this.node.querySelector('.form-input');
        this.hint = this.node.querySelector('.form-hint');
        this.hintLabel = this.hint.querySelector('.form-hint__label');
        this.hintDisc = this.hint.querySelector('.form-hint__disc');
        this.maxCounter = this.node.getAttribute('data-counter-max') || 40;

        this.onInput = this.onInput.bind(this);

        this.input.addEventListener('input', this.onInput, false);

        this._init();
    }

    _init() {
        this.declText = this.node.getAttribute('data-decl') ? JSON.parse(this.node.getAttribute('data-decl')) : null;
        this.node.removeAttribute('data-decl');
        this.declinations = new Declinations(this.declText);

        this.refreshCounter(this.getDifference());
        this.refreshDisc(this.getDifference());
    }

    getDifference() {
        return this.maxCounter - this.input.value.length;
    }

    onInput() {
        this.input.setAttribute('maxlength', this.maxCounter);
        const valLength = this.input.value.length;
        const difference = this.maxCounter - valLength;

        if (valLength <= this.maxCounter) {
            this.refreshCounter(difference);
            this.refreshDisc(difference);
        }
    }

    refreshCounter(difference) {
        this.hintLabel.textContent = difference;
    }

    refreshDisc(number) {
        this.hintDisc.textContent = this.declinations.trans(number);
    }
}

export default TextCounter;
