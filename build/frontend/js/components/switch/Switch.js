'use strict';

import Layout from 'js/helpers/Layout';

class Switch {
    constructor(node) {
        this.node = node;

        if(!this.node) return;

        this.node['_component'] = this

        this.onInputChange = this.onInputChange.bind(this);
        this.unchecked = this.unchecked.bind(this);

        this.input = this.node.querySelector('input[type="checkbox"]');

        this.input.addEventListener('input', this.onInputChange, false);
        Layout.body.addEventListener('map:hide', this.unchecked, false);
    }

    onInputChange() {
        this._dispatchChangeEvent();
    }

    _dispatchChangeEvent() {
        const event = new CustomEvent('input:change', {
            bubbles: true,
            cancelable: true,
            detail: {
                component: this,
                value: this.isChecked()
            }
        });

        this.node.dispatchEvent(event);
    }

    unchecked() {
        this.input.checked = false;
    }

    isChecked() {
        return this.input.checked;
    }
}

export default Switch;
