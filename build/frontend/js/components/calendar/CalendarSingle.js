'use strict';

import flatpickr from 'flatpickr';
import { Russian } from 'flatpickr/dist/l10n/ru.js';
import DetectVisibleArea from 'js/helpers/DetectVisibleArea';
import BaseCalendar from 'js/components/calendar/BaseCalendar';

class CalendarSingle extends BaseCalendar {
    constructor(node) {
        super(node);
    }

    _init() {
        this.calendar = flatpickr(this.node, {
            mode: 'single',
            position: 'auto',
            onOpen: (selectedDates, dateStr, instance) => {
                instance.calendarContainer.classList.add('calendar-single');

                this.calendarOpen();
            },
            onClose: () => {
                this.calendarClose();
            },
            locale: Russian,
            static: true,
            disableMobile: true
        });
    }

    calendarOpen() {
        // setTimeout(() => {
        //     DetectVisibleArea.isOutOfScreen(this.node, this.calendar.calendarContainer) ?
        //         this.calendar.calendarContainer.classList.add('calendar-range__direction_top') : null;
        //
        //     this.calendar.calendarContainer.style.top = '';
        // }, 0);

        const event = new CustomEvent('calendar-single:open', {
            bubbles: true,
            cancelable: true,
            detail: {
                component: this
            }
        });

        this.node.dispatchEvent(event);
    }

    calendarClose() {
        this.lastClosed = Date.now();
        this.setState({ isOpen: false});
        // this.calendar.calendarContainer.classList.remove('calendar-single__direction_top')

        const event = new CustomEvent('calendar-single:close', {
            bubbles: true,
            cancelable: true,
            detail: {
                component: this
            }
        });

        this.node.dispatchEvent(event);
    }
}

export default CalendarSingle;
