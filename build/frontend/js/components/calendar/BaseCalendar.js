'use strict';

import Stringz from 'js/Helpers/Stringz';
import Component from 'js/components/Component';

class BaseCalendar extends Component {
    constructor(node) {
        super(node);

        this.lastClosed = 0;
        this.delay = 500;

        // lame fix due to flaw in the component design
        this.btnNode = this.node.closest('.select__field');

        if (this.btnNode) {
            this.btnNode.addEventListener('click', (e) => {
                if (e.target.closest('.flatpickr-calendar')) return;

                setTimeout(() => {
                    this.isOpen() ? this.close() : this.open();
                }, 10);
            });
        }

        this._init();
    }

    /**
     * @override
     */
    _init() {}

    close() {
        this.lastClosed = Date.now();
        this.setState({ isOpen: false});
        this.calendar.close();
    }

    open() {
        if ((Date.now() - this.lastClosed) < this.delay)
            return;

        this.setState({ isOpen: true});
        this.calendar.open();
    }

    isOpen() {
        return !!this._state.isOpen;
    }

    getName() {
        return this.node.getAttribute('name') || this.node.getAttribute('data-name');
    }

    getValue() {
        return this.node.value;
    }

    getLabelValue() {
        return this.node.getAttribute('placeholder');
    }

    getCurrentData() {
        return [{
            name: this.getName(),
            value: Stringz.stripTags( this.getValue() ),
            text: Stringz.stripTags( this.getValue() )
        }];
    }

    getNode() {
        return this.node;
    }

    isActive() {
        return this.node.value !== '';
    }

    reset() {
        this.calendar.clear(false);
    }
}

export default BaseCalendar;
