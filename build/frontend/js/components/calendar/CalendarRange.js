'use strict';

import flatpickr from 'flatpickr';
import { Russian } from 'flatpickr/dist/l10n/ru.js';
import DetectVisibleArea from 'js/helpers/DetectVisibleArea';
import BaseCalendar from 'js/components/calendar/BaseCalendar';

class CalendarRange extends BaseCalendar {
    constructor(node) {
        super(node);
    }

    _init() {
        this.calendar = flatpickr(this.node, {
            mode: 'range',
            position: 'auto',
            onOpen: (selectedDates, dateStr, instance) => {
                instance.calendarContainer.classList.add('calendar-range');

                this.calendarOpen();
            },
            onClose: () => {
                this.calendarClose();
            },
            // wrap: true,
            locale: Russian,
            static: true,
            disableMobile: true
        });
    }

    calendarOpen() {
        // setTimeout(() => {
        //     DetectVisibleArea.isOutOfScreen(this.node, this.calendar.calendarContainer) ?
        //         this.calendar.calendarContainer.classList.add('calendar-range__direction_top') : null;
        //
        //     this.calendar.calendarContainer.style.top = '';
        // }, 0);

        const event = new CustomEvent('calendar-range:open', {
            bubbles: true,
            cancelable: true,
            detail: {
                component: this
            }
        });

        this.node.dispatchEvent(event);
    }

    calendarClose() {
        this.lastClosed = Date.now();
        this.setState({ isOpen: false });
        // this.calendar.calendarContainer.classList.remove('calendar-range__direction_top');

        const event = new CustomEvent('calendar-range:close', {
            bubbles: true,
            cancelable: true,
            detail: {
                component: this
            }
        });

        this.node.dispatchEvent(event);
    }
}

export default CalendarRange;
