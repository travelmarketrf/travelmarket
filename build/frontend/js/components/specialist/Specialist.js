'use strict';

import MD from 'js/helpers/MD';

class Specialist {
    constructor(node) {
        this.node = node;

        this.state = {
            open: true
        }

        if (!this.node && !MD.mobile()) return;

        this.foldBtn = this.node.querySelector('.specialist__fold');
        this.foldBtnState = this.foldBtn.querySelector('.specialist__fold-state');

        this.onFoldBtnClick = this.onFoldBtnClick.bind(this);

        this.foldBtn.addEventListener('click', this.onFoldBtnClick, false);
    }

    setState(state = {}) {
        Object.assign(this.state, state);
    }

    onFoldBtnClick() {
        this.setState({ open: !this.state.open });

        this.update();
    }

    update() {
        this.state.open ? this.close() : this.open();
    }

    open() {
        this.node.classList.remove('specialist_state_hidden');
    }

    close() {
        this.node.classList.add('specialist_state_hidden');
    }
}

export default Specialist;
