'use strict';

class Component {
    constructor(node) {
        this.node = node;

        if (!this.node) throw new Error(`Missing required parameter 'node'.`);

        this.node['_component'] = this;

        this._state = {};
    }

    setState(state = {}) {
        Object.assign(this._state, state);
    }
}

export default Component;
