'use strict';

class CreativeTable {
    constructor(node) {
        this.node = node;

        if (!this.node) return;

        this.node['_component'] = this;
    }

    showTable() {
        this.node.classList.remove('creative-table_hidden');
    }

    hideTable() {
        this.node.classList.add('creative-table_hidden');
    }
}

export default CreativeTable;
