'use strict';

import Swiper from 'swiper';

class SuggestionSlider {
    constructor(node) {
        this.node = node;

        if (!this.node) return;

        this.node['_component'] = this

        this.swiperContainer = this.node.querySelector('.swiper-container');

        this.swiper = new Swiper(this.swiperContainer, {
            direction: 'horizontal',
            spaceBetween: 30,
            navigation: {
                nextEl: this.node.querySelector('.suggestion__arrow-next'),
            },
            pagination: {
                el: '.swiper-pagination',
                type: 'bullets',
            },
            autoplay: {
                delay: 5000,
            },
        });
    }
}

export default SuggestionSlider;
