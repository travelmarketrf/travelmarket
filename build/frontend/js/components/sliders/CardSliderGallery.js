'use strict';

import Swiper from 'swiper';
import MD from 'js/helpers/MD';
import Component from 'js/components/Component';

class CardSliderGallery extends Component {
    constructor(node) {
        super(node);

        this.direction = 'horizontal';

        if (MD.deskLow() && !MD.phone()) {
            this.direction = 'vertical';
        }

        this.thumbs = new Swiper(this.node.querySelector('.slider-gallery__bottom'), {
            spaceBetween: 0,
            slidesPerView: 4,
            freeMode: true,
            freeMode: true,
            watchSlidesVisibility: true,
            watchSlidesProgress: true,
            spaceBetween: 1,
            direction: this.direction
        });

        this.swiper = new Swiper(this.node.querySelector('.slider-gallery__top'), {
            navigation: {
                nextEl: this.node.querySelector('.slider-gallery__bottom_arrow_next'),
                prevEl: this.node.querySelector('.slider-gallery__bottom_arrow_prev'),
            },
            thumbs: {
                swiper: this.thumbs
            }
        });
    }
}

export default CardSliderGallery;
