'use strict';

import Swiper from 'swiper';

class MinicardSlider {
    constructor(node) {
        this.node = node;

        if (!this.node) return;

        this.node['_component'] = this;

        this.swiperContainer = this.node.querySelector('.swiper-container');

        this.onResize = this.onResize.bind(this);

        window.addEventListener('resize', this.onResize, false);

        this.swiper = new Swiper(this.swiperContainer, {
            direction: 'horizontal',
            navigation: {
                nextEl: this.node.querySelector('.swiper-arrow-next'),
                prevEl: this.node.querySelector('.swiper-arrow-prev')
            }
        });
    }

    onResize() {
        this.update();
    }

    update() {
        this.swiper.update();
    }
}

export default MinicardSlider;
