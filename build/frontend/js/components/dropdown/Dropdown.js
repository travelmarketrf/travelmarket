'use strict';

import Layout from 'js/helpers/Layout';
import DomHelper from 'js/helpers/DomHelper';
import Component from 'js/components/Component';
import DetectVisibleArea from 'js/helpers/DetectVisibleArea';

class Dropdown extends Component {
    constructor(node) {
        super(node);

        this.list = this.node.querySelector('.dropdown-list');
        this.listContainer = this.list.parentElement;
        this.isExternal = this.node.hasAttribute('data-dropdown-external');

        this.toggle = this.toggle.bind(this);

        this.node.addEventListener('click', this.toggle, false);
    }

    toggle() {
        if (this.isOpen()) {
            this.close();
        } else {
            this.unsetActiveList();
            this.open();
        }
    }

    isTargetInside(target) {
        return target.closest('.dropdown') || target.closest('.dropdown-list') === this.list;
    }

    isOpen() {
        return Layout.body.querySelector('.dropdown_open');
    }

    unsetActiveList() {
        [...Layout.body.querySelectorAll('.dropdown')]
            .forEach(dropdown => dropdown.classList.remove('dropdown_open'));
    }

    open() {
        DetectVisibleArea.detect(this.node, this.list);
        this.node.classList.add('dropdown_open');

        if (this.isExternal)
            this._detachList();

        this._dispatchDropdownOpenEvent();
    }

    _detachList() {
        const wrapper = document.createElement('div');
        const coords = this.node.getBoundingClientRect();
        const offsetTop = DomHelper.scrollTop() + coords.top;
        const offsetRight = DomHelper.windowWidth() - (coords.left + this.node.offsetWidth);

        wrapper.classList.add('dropdown-wrapper');
        wrapper.appendChild(this.list);
        Layout.root.appendChild(wrapper);

        wrapper.style.top = `${offsetTop}px`;
        wrapper.style.right = `${offsetRight}px`;
    }

    close() {
        setTimeout(() => { DetectVisibleArea.refresh(this.node, this.list); }, 300);
        Layout.body.querySelector('.dropdown_open').classList.remove('dropdown_open');

        if (this.isExternal)
            this._attachList();

        this._dispatchDropdownCloseEvent();
    }

    _attachList() {
        const container = this.list.parentElement;

        this.listContainer.appendChild(this.list);
        container.remove();
    }

    _dispatchDropdownOpenEvent() {
        const event = new CustomEvent('dropdown:open', {
            bubbles: true,
            cancelable: true,
            detail: {
                component: this
            }
        });

        this.node.dispatchEvent(event);
    }

    _dispatchDropdownCloseEvent() {
        const event = new CustomEvent('dropdown:close', {
            bubbles: true,
            cancelable: true,
            detail: {
                component: this
            }
        });

        this.node.dispatchEvent(event);
    }
}

export default Dropdown;
