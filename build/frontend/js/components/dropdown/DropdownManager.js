'use strict';

import Layout from 'js/helpers/Layout';

class DropdownManager {
    constructor() {
        this.currentDropdown = null;

        this.onDropdownOpen = this.onDropdownOpen.bind(this);
        this.onDropdownClose = this.onDropdownClose.bind(this);
        this.onClick = this.onClick.bind(this);

        Layout.body.addEventListener('dropdown:open', this.onDropdownOpen, false);
        Layout.body.addEventListener('dropdown:close', this.onDropdownClose, false);
        Layout.body.addEventListener('click', this.onClick, false);
    }

    onClick(e) {
        if (!this.hasActive() || this.currentDropdown.isTargetInside(e.target)) return;

        this.close();
    }

    hasActive() {
        return !!this.currentDropdown;
    }

    onDropdownOpen(e) {
        this.close();

        this.currentDropdown = e.detail.component;
    }

    onDropdownClose() {
        this.currentDropdown = null;
    }

    close() {
        if (!this.hasActive()) return;

        this.currentDropdown.close();
    }
}

export default DropdownManager;
