'use strict';

class ProgressBar {
    constructor(node) {
        this.node = node;

        if (!this.node) return;

        this.node['_component'] = this;

        this.barNode = this.node.querySelector('.progress__bar');
    }

    set(progress) {
        this.barNode.style.width = `${progress}%`;
    }
}

export default ProgressBar;
