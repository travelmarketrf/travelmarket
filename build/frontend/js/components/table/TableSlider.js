'use strict';

import Component from 'js/components/Component';

class TableSlider extends Component {
    constructor(node) {
        super(node);

        this.arrowLeftNode = this.node.querySelector('.table__arrow_left');
        this.arrowRightNode = this.node.querySelector('.table__arrow_right');
        this.headerNode = this.node.querySelector('.table__header-mobile');
        this.bodyNode = this.node.querySelector('.table__body');
        this.slidesCount = this.bodyNode.children.length;

        this._bindEvents();
        this._init();
    }

    _bindEvents() {
        this.prev = this.prev.bind(this);
        this.next = this.next.bind(this);
        this.arrowLeftNode.addEventListener('click', this.prev, false);
        this.arrowRightNode.addEventListener('click', this.next, false);
    }

    _init() {
        const activeItem = this.getSelectedRow();

        if (!activeItem)
            return this.setActive(0);

        this.setName(activeItem.getAttribute('data-name'));
        this._checkControls();
    }

    setActive(index) {
        const item = this.bodyNode.children[index];

        if (!item)
            return;

        const text = item.getAttribute('data-name');

        this._clearSelected();
        this.setName(text);

        item.classList.add('table__row_selected');

        this._checkControls();
    }

    prev() {
        this.setActive(this.getSelectedRowIndex() - 1);
    }

    next() {
        this.setActive(this.getSelectedRowIndex() + 1);
    }

    _checkControls() {
        this.isFirst() ? this._disactivateArrow(this.arrowLeftNode) : this._activateArrow(this.arrowLeftNode);
        this.isLast() ? this._disactivateArrow(this.arrowRightNode) : this._activateArrow(this.arrowRightNode);
    }

    _activateArrow(arrow) {
        arrow.classList.add('table__arrow_active');
    }

    _disactivateArrow(arrow) {
        arrow.classList.remove('table__arrow_active');
    }

    setName(name) {
        this.headerNode.innerHTML = name;
    }

    isFirst() {
        return this.getSelectedRowIndex() === 0;
    }

    isLast() {
        return this.getSelectedRowIndex() === (this.bodyNode.children.length - 1);
    }

    getSelectedRowIndex() {
        return [...this.bodyNode.children].indexOf(this.getSelectedRow());
    }

    getSelectedRow() {
        return this.bodyNode.querySelector('.table__row_selected');
    }

    _clearSelected() {
        [...this.bodyNode.querySelectorAll('.table__row_selected')].forEach(node => node.classList.remove('table__row_selected'));
    }
}

export default TableSlider;
