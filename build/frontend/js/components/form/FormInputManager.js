'use strict';

import Layout from 'js/helpers/Layout';

class FormInputManager {
    constructor() {
        this._bindEvents();
    }

    _bindEvents() {
        this.onInputChange = this.onInputChange.bind(this);
        Layout.body.addEventListener('change', this.onInputChange, false);
    }

    onInputChange(e) {
        const inputNode = e.target.closest('.form-input') || e.target.closest('.form-textarea');

        if (!inputNode) return;

        if (inputNode.value === '') {
            inputNode.classList.remove('form-input_filled');
        } else {
            inputNode.classList.add('form-input_filled');
        }
    }
}

export default FormInputManager;
