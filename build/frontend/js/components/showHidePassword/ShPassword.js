'use strict';

class ShPassword {
    constructor(node) {
        this.node = node;

        if (!this.node) return;

        this.node['_component'] = this;

        this.input = this.node.querySelector('input');
        this.eye = [...this.node.querySelectorAll('.icon-eye')];

        this.onClick = this.onClick.bind(this);

        this.node.addEventListener('click', this.onClick, false);
    }

    onClick(e) {
        const eye = e.target.closest('.icon-eye');

        if (!eye) return;
        
        this.unsetActiveEye();

        if (this.text()) {
            this.setActiveEye(0);
            this.typePassword();
        } else {
            this.setActiveEye(1);
            this.typeText();
        }
    }

    text() {
        return this.input.getAttribute('type') === 'text'
    }

    unsetActiveEye() {
        [...this.eye].forEach(eye => eye.classList.remove('icon-eye_active'));
    }

    setActiveEye(index) {
        this.eye[index].classList.add('icon-eye_active');
    }

    typePassword() {
        this.input.setAttribute('type', 'password');
    }

    typeText() {
        this.input.setAttribute('type', 'text');
    }
}

export default ShPassword;
