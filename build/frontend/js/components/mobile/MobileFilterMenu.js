'use strict';

import Layout from 'js/helpers/Layout';
import ScrollLocker from 'js/helpers/ScrollLocker';

class MobileFilterMenu {
    constructor(node) {
        this.node = node;

        this.state = {
            isOpen: false
        };

        if (!this.node) return;

        this.node['_component'] = this;

        this.filter = Layout.body.querySelector('.filter-mobile');
        this.closeBtn = this.filter.querySelector('.filter-mobile__close');
        this.overlay = this.filter.querySelector('.filter-mobile__overlay');

        this._bindEvents();
    }

    _bindEvents() {
        this.toggle = this.toggle.bind(this);

        this.node.addEventListener('click', this.toggle, false);
        this.closeBtn.addEventListener('click', this.toggle, false);
        this.overlay.addEventListener('click', this.toggle, false);
        Layout.body.addEventListener('filter:accept', this.toggle, false);
    }

    toggle() {
        this.isOpen() ? this.close() : this.open();
    }

    isOpen() {
        return this.state.isOpen;
    }

    setState(state = {}) {
        Object.assign(this.state, state);
    }

    open() {
        window.scrollTo(0, 0);

        this.filter.classList.add('filter-mobile_open');

        this.setState({ isOpen: true });

        ScrollLocker.lock();

        this._dispatchOpenEvent();
    }

    close() {
        this.filter.classList.remove('filter-mobile_open');

        this.setState({ isOpen: false });

        ScrollLocker.unlock();

        this._dispatchCloseEvent();
    }

    _dispatchOpenEvent() {
        const event = new CustomEvent('mobile-menu:open', {
            bubbles: true,
            cancelable: true,
            detail: {
                component: this,
                value: this.isOpen()
            }
        });

        Layout.body.dispatchEvent(event);
    }

    _dispatchCloseEvent() {
        const event = new CustomEvent('mobile-menu:close', {
            bubbles: true,
            cancelable: true,
            detail: {
                component: this,
                value: this.isOpen()
            }
        });

        Layout.body.dispatchEvent(event);
    }
}

export default MobileFilterMenu;
