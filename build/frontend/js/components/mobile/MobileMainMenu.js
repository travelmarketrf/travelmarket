'use strict';

import Layout from 'js/helpers/Layout';
import ScrollLocker from 'js/helpers/ScrollLocker';

class MobileMainMenu {
    constructor() {
        this.state = {
            isOpen: false
        };

        this.hamburger = Layout.body.querySelector('.btn-hamb');
        this.menuNode = Layout.body.querySelector('.menu-mobile');
        this.menuBtnClose = this.menuNode.querySelector('.menu-mobile__close');
        this.menuOverlay = this.menuNode.querySelector('.menu-mobile__overlay');

        this._bindEvents();
    }

    _bindEvents() {
        this.toggle = this.toggle.bind(this);

        this.hamburger.addEventListener('click', this.toggle, false);
        this.menuBtnClose.addEventListener('click', this.toggle, false);
        this.menuOverlay.addEventListener('click', this.toggle, false);
    }

    toggle() {
        this.isOpen() ? this.close() : this.open();
    }

    setState(state = {}) {
        Object.assign(this.state, state);
    }

    isOpen() {
        return this.state.isOpen;
    }

    open() {
        this.hamburger.classList.add('btn-hamb_active');
        this.menuNode.classList.add('menu-mobile_open');

        this.setState({ isOpen: true });

        ScrollLocker.lock();

        this._dispatchOpenEvent();
    }

    close() {
        this.hamburger.classList.remove('btn-hamb_active');
        this.menuNode.classList.remove('menu-mobile_open');

        this.setState({ isOpen: false });

        ScrollLocker.unlock();

        this._dispatchCloseEvent();
    }

    _dispatchOpenEvent() {
        const event = new CustomEvent('mobile-menu:open', {
            bubbles: true,
            cancelable: true,
            detail: {
                component: this,
                value: this.isOpen()
            }
        });

        Layout.body.dispatchEvent(event);
    }

    _dispatchCloseEvent() {
        const event = new CustomEvent('mobile-menu:close', {
            bubbles: true,
            cancelable: true,
            detail: {
                component: this,
                value: this.isOpen()
            }
        });

        Layout.body.dispatchEvent(event);
    }
}

export default MobileMainMenu;
