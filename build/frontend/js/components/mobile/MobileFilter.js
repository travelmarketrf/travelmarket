'use strict';

import Layout from 'js/helpers/Layout';
import ScrollLocker from 'js/helpers/ScrollLocker';
import DomHelper from 'js/helpers/DomHelper';
import MD from 'js/helpers/MD';

class MobileFilter {
    constructor(node) {
        this.node = node;

        if (!this.node) return;

        this.node['_component'] = this;

        this.state = {
            isOpen: true
        };

        this.filterBtn = this.node.querySelector('.mobile-filter__item-filter');
        this.mapBtn = this.node.querySelector('.mobile-filter__item-map');
        this.mapLabel = this.mapBtn.querySelector('.mobile-filter__label');
        this.mapIcon = this.mapBtn.querySelector('.mobile-filter__icon');

        this._bindEvents();
        this._init();
    }

    _init() {
        if (this.isOpen() && MD.mobile())
            ScrollLocker.lock();

        if (MD.mobile()) {
            ScrollLocker.unlock();
            this._hideMap();
        }
    }

    _bindEvents() {
        this.onMenuOpen = this.onMenuOpen.bind(this);
        this.onMenuClose = this.onMenuClose.bind(this);
        this.onMapBtnClick = this.onMapBtnClick.bind(this);

        Layout.body.addEventListener('mobile-menu:open', this.onMenuOpen, false);
        Layout.body.addEventListener('mobile-menu:close', this.onMenuClose, false);
        this.mapBtn.addEventListener('click', this.onMapBtnClick, false);
    }

    onMapBtnClick() {
        this.isOpen() ? this._hideMap() : this._showMap();
    }

    _showMap() {
        this.setState({ isOpen: true });

        this.mapLabel.textContent = 'Списком';
        this.mapIcon.innerHTML = `
            <svg xmlns="http://www.w3.org/2000/svg" width="20" height="20" viewBox="0 0 20 20" fill="none">
                <path fill-rule="evenodd" clip-rule="evenodd" d="M1 3C1 2.44772 1.44772 2 2 2H4C4.55228 2 5 2.44772 5 3V5C5 5.55228 4.55228 6 4 6H2C1.44772 6 1 5.55228 1 5V3ZM1 9C1 8.44772 1.44772 8 2 8H4C4.55228 8 5 8.44772 5 9V11C5 11.5523 4.55228 12 4 12H2C1.44772 12 1 11.5523 1 11V9ZM2 14C1.44772 14 1 14.4477 1 15V17C1 17.5523 1.44772 18 2 18H4C4.55228 18 5 17.5523 5 17V15C5 14.4477 4.55228 14 4 14H2ZM8 15C7.44772 15 7 15.4477 7 16C7 16.5523 7.44771 17 8 17H18C18.5523 17 19 16.5523 19 16C19 15.4477 18.5523 15 18 15H8ZM8 9C7.44772 9 7 9.44772 7 10C7 10.5523 7.44771 11 8 11H18C18.5523 11 19 10.5523 19 10C19 9.44772 18.5523 9 18 9H8ZM8 3C7.44772 3 7 3.44772 7 4C7 4.55228 7.44771 5 8 5H18C18.5523 5 19 4.55228 19 4C19 3.44772 18.5523 3 18 3H8Z" fill="white"/>
            </svg>
        `;

        DomHelper.setScrollTop(0);
        ScrollLocker.lock();
        this._dispatchOpenMapEvent();
    }

    _hideMap() {
        this.setState({ isOpen: false });

        this.mapLabel.textContent = 'На карте';
        this.mapIcon.innerHTML = `
            <svg width="18" height="18" viewBox="0 0 18 18" fill="none" xmlns="http://www.w3.org/2000/svg">
                <path
                    d="M8.99988 0C5.13938 0 2.00977 3.12956 2.00977 6.99017C2.00977 10.8507 6.7735 18 8.99988 18C11.2263 18 15.99 10.8507 15.99 6.99017C15.99 3.12956 12.8604 0 8.99988 0ZM8.99988 10.1941C6.96766 10.1941 5.32023 8.54662 5.32023 6.51434C5.32023 4.48217 6.96766 2.83469 8.99988 2.83469C11.0321 2.83469 12.6795 4.48217 12.6795 6.51434C12.6795 8.54662 11.0321 10.1941 8.99988 10.1941Z"
                    fill="white" />
            </svg>
        `;

        ScrollLocker.unlock();
        this._dispatchCloseMapEvent();
    }

    setState(state = {}) {
        Object.assign(this.state, state);
    }

    isOpen() {
        return this.state.isOpen;
    }

    onMenuOpen() {
        this.node.classList.add('mobile-filter_hidden');
    }

    onMenuClose() {
        this.node.classList.remove('mobile-filter_hidden');
    }

    _dispatchOpenMapEvent() {
        const event = new CustomEvent('mobile-map:open', {
            bubbles: true,
            cancelable: true,
            detail: {
                component: this,
                value: this.isOpen()
            }
        });

        Layout.body.dispatchEvent(event);
    }

    _dispatchCloseMapEvent() {
        const event = new CustomEvent('mobile-map:close', {
            bubbles: true,
            cancelable: true,
            detail: {
                component: this,
                value: this.isOpen()
            }
        });

        Layout.body.dispatchEvent(event);
    }
}

export default MobileFilter;
