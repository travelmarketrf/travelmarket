'use strict';

import CropperJS from 'cropperjs';
import Layout from 'js/helpers/Layout';
import CustomizablePopup from 'js/components/managers/CustomizablePopup';
import cropperDefaults from 'js/components/cropper/cropperDefaults';

const POPUP_NAME = 'cropper-popup';

class CropperPopup extends CustomizablePopup  {
    constructor() {
        super();

        this._imageData = null;
    }

    _bindEvents() {
        super._bindEvents();

        this._onSave = this._onSave.bind(this);
    }

    _bindSave() {
        const btnNode = this.getSaveBtnNode();

        if (btnNode)
            btnNode.addEventListener('click', this._onSave, false);
    }

    _onSave() {
        this._resolve( this._cropper.getData(true) );
        this._dispatchPopupCloseEvent();
    }

    onSave() {
        return this._promise;
    }

    open() {
        if (!this._imageData)
            throw new Error('Image data is not defined.')

        this._generatePopup();

        this._cropper = new CropperJS( this.getImageNode() , cropperDefaults);
        this._promise = new Promise(resolve => this._resolve = resolve);

        this._bindSave();
        this._dispatchPopupOpenEvent(POPUP_NAME);
    }

    _generatePopup() {
        const content = `
            <div class="popup__container">
                <div class="popup__wrapper">
                    <div class="popup__title">Добавление фото</div>

                    <div class="popup__scroll">
                        <div class="popup__description">Выбранная область будет показываться на Вашей странице.</div>

                        <div class="popup__cropper">
                            <img src="${this._imageData}" alt="Ваша фотография">
                        </div>

                        <div class="form-button form-button_height_medium form-button_width_small" data-action="save-img">Сохранить</div>
                    </div>

                    <div class="popup__close" data-action="close-popup" data-name="${POPUP_NAME}">
                        <div class="popup__circle">
                            <svg width="13" height="13" viewBox="0 0 13 13" fill="none" xmlns="http://www.w3.org/2000/svg">
                                <path fill-rule="evenodd" clip-rule="evenodd"
                                    d="M0.292893 0.293191C0.683417 -0.0973337 1.31658 -0.0973338 1.70711 0.293191L6.12026 4.70634L10.5336 0.2929C10.9241 -0.0976283 11.5573 -0.097634 11.9478 0.292887C12.3384 0.683408 12.3384 1.31657 11.9478 1.7071L7.53447 6.12055L11.9476 10.5337C12.3382 10.9242 12.3382 11.5574 11.9476 11.9479C11.5571 12.3384 10.9239 12.3384 10.5334 11.9479L6.12026 7.53476L1.70746 11.9475C1.31693 12.338 0.683763 12.338 0.293243 11.9475C-0.0972782 11.5569 -0.0972724 10.9238 0.293255 10.5333L4.70604 6.12055L0.292893 1.7074C-0.0976308 1.31688 -0.0976312 0.683715 0.292893 0.293191Z"
                                    fill="#AEB8D5" />
                            </svg>
                        </div>
                    </div>
                </div>
            </div>
        `;

        this._popupWnd = this._createWindow('cropper-popup', POPUP_NAME);
        this._popupWnd.innerHTML = content;

        Layout.root.appendChild(this._popupWnd);
    }

    setImageData(data) {
        this._imageData = data;
    }

    getImageNode() {
        if (!this._popupWnd) return;

        return this._popupWnd.querySelector('.popup__cropper > img');
    }

    getSaveBtnNode() {
        if (!this._popupWnd) return;

        return this._popupWnd.querySelector('[data-action="save-img"]');
    }

    getCropper() {
        return this._cropper;
    }
}

export default CropperPopup;
