'use strict';

import Component from 'js/components/Component';
import CropperPopup from 'js/components/cropper/CropperPopup';

class Cropper extends Component {
    constructor(node) {
        super(node);

        this._inputFile = this.node.querySelector('.user__file');
        this._popup = new CropperPopup();

        this._bindEvents();
    }

    _bindEvents() {
        this.onInputChange = this.onInputChange.bind(this);
        this._onSave = this._onSave.bind(this);
        this._reset = this._reset.bind(this);

        this._inputFile.addEventListener('change', this.onInputChange, false);
        this._inputFile.addEventListener('click', this._reset, false);
    }

    _onSave(data) {
        this._dispatchSaveEvent(data);
    }

    onInputChange() {
        this.readFile(this._inputFile)
            .then(data => {
                this._popup.setImageData(data);
                this._popup.open();

                return this._popup.onSave();
            })
            .then(this._onSave)
            .catch(err => {
                throw err;
            });
    }

    readFile(input) {
        if (!input.files || !input.files[0])
            return Promise.reject( new Error('No file has been chosen.') );

        const reader = new FileReader();

        return new Promise(resolve => {
            reader.onload = e => resolve(e.target.result);

            reader.readAsDataURL(input.files[0]);
        });
    }

    _reset(e) {
        e.target.value = '';
    }

    _dispatchSaveEvent(data) {
        const event = new CustomEvent('cropper:save', {
            bubbles: true,
            cancelable: true,
            detail: {
                input: this._inputFile,
                data
            }
        });

        this.node.dispatchEvent(event);
    }
}

export default Cropper;
