'use strict';

class HotelMinicard {
    constructor(node) {
        this.node = node;

        if (!this.node) return;

        this.node['_component'] = this;
    }

    getImages() {
        const array = [];

        [...this.node.querySelectorAll('.minicard-slider .swiper-slide')]
            .forEach(slide => array.push(slide.style.backgroundImage));

        return array;
    }
}

export default HotelMinicard;
