'use strict';

class WidgetFactory {
    static defineWidget(name, classInstanciator) {
        WidgetFactory.widgets[name] = classInstanciator;
    }

    static defineWidgets(widgets = {}) {
        WidgetFactory.widgets = widgets;
    }

    static defineFactory(name, classInstanciator) {
        WidgetFactory.factories[name] = classInstanciator;
    }

    static defineFactories(factories = {}) {
        WidgetFactory.factories = factories;
    }

    static create(node) {
        const widgetName = node.getAttribute('data-widget');

        if (!widgetName) return;

        if (WidgetFactory.factories[widgetName])
            return WidgetFactory.factories[widgetName].create(node);

        if (WidgetFactory.widgets[widgetName])
            return new WidgetFactory.widgets[widgetName](node);
    }
}

WidgetFactory.factories = {};

WidgetFactory.widgets = {};

export default WidgetFactory;
