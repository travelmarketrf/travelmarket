'use strict';

import MD from 'js/helpers/MD';

class Traveler {
    constructor(node) {
        this.node = node;

        if (!this.node) return;

        this.node['_component'] = this;
    }
}

export default Traveler;
