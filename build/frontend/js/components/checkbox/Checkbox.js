'use strict'

/*!
 * @author Chistyakov I. V. <ichistyakovv@gmail.com>
 * Licensed under the MIT license
 */

import FilterWidgetInterface from 'js/components/FilterWidgetInterface';
import Stringz from 'js/Helpers/Stringz';

/**
 * The customized checkbox.
 *
 * @class
 * @implements FilterWidgetInterface
 */
class Checkbox extends FilterWidgetInterface {
    constructor(node) {
        super();

        this.node = node;

        if (!node) return;

        this.node['_component'] = this;

        this.inputNode = this.node.querySelector('input[type="checkbox"]');
        this.state = {
            isDisabled: false
        };

        if (!this.inputNode) return;

        this._init();
        this._bindEvents();
    }

    _init() {
        if (this.node.getAttribute('data-disabled') || this.node.classList.contains('checkbox_disabled')) {
            this.disable();
        }
    }

    _bindEvents() {
        this.onClick = this.onClick.bind(this);
        this.node.addEventListener('click', this.onClick, false);
    }

    onClick(e) {
        if (e.target instanceof HTMLInputElement)
            this._dispatchChangeEvent();
    }

    disable() {
        this.setState({isDisabled: true});
        this.node.classList.add('checkbox_disabled');
    }

    enable() {
        this.setState({isDisabled: false});
        this.node.classList.remove('checkbox_disabled');
    }

    check() {
        this.inputNode.checked = true;
    }

    uncheck() {
        this.inputNode.checked = false;
    }

    isChecked() {
        return this.inputNode.checked;
    }

    isDisabled() {
        return this.state.isDisabled;
    }

    setState(state = {}) {
        Object.assign(this.state, state);
    }

    getValue() {
        return this.inputNode.value;
    }

    getName() {
        return this.inputNode.getAttribute('name') || this.inputNode.getAttribute('data-name');
    }

    _dispatchChangeEvent() {
        const event = new CustomEvent('checkbox:change', {
            bubbles: true,
            cancelable: true,
            detail: {
                component: this,
                value: this.getValue()
            }
        });
        this.node.dispatchEvent(event);
    }

    getCurrentData() {
        return [{
            name: this.getName(),
            value: Stringz.stripTags( this.isChecked() ),
            text: Stringz.stripTags( this.getValue() )
        }];
    }

    /**
     * @inheritdoc
     */
    isActive() {
        return this.isChecked();
    }

    /**
     * @inheritdoc
     */
    getNode() {
        return this.node;
    }

    /**
     * @inheritdoc
     */
    reset() {
        if (!this.isActive())
            return false;

        this.uncheck();

        return true;
    }

    /**
     * @inheritdoc
     */
    choose() {
        this.check();

        return true;
    }
}

export default Checkbox;
