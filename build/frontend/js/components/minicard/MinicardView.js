'use strict';

import Component from 'js/components/Component';
import MD from 'js/helpers/MD';

class MinicardView extends Component {
    constructor(node) {
        super(node);

        this._bindEvents();
        this.onResize();
    }

    _bindEvents() {
        this.onResize = this.onResize.bind(this);
        window.addEventListener('resize', this.onResize, false);
    }

    onResize() {
        MD.phone() ? this._showVertical() : this._showHorizontal();
    }

    getMinicards() {
        return [...this.node.querySelectorAll('.minicard')];
    }

    _showVertical() {
        this.getMinicards().forEach(node => node.classList.remove('minicard_view_horizontal', 'minicard_tight'));
    }

    _showHorizontal() {
        this.getMinicards().forEach(node => node.classList.add('minicard_view_horizontal', 'minicard_tight'));
    }
}

export default MinicardView;
