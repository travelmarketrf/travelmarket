'use strict';

import Layout from 'js/helpers/Layout';
import Widget from 'js/components/Widget';
import CustomizablePopup from 'js/components/managers/CustomizablePopup';
import MD from 'js/helpers/MD';

const POPUP_NAME = 'simple-tabs-popup';

class SimpleTabsPopup extends CustomizablePopup {
    constructor() {
        super();

        this.actions = {
            'simple-tabs-popup': 'open'
        };
    }

    open(node) {
        if (!MD.phone()) return;

        const tabNode = node.closest('[data-widget="tabs"]');

        if (!tabNode) return;

        const widet = Widget.get(tabNode);

        if (!widet) return;

        const activeTabNode = widet.getAcitveTabNode();
        const activePanelNode = widet.getAcitvePanelNode();
        const title = activeTabNode.querySelector('.simple-tabs__tab-text').textContent;
        const activePanelNodeCopy = activePanelNode.cloneNode(true);
        [...activePanelNodeCopy.querySelectorAll('.simple-tabs__list')].forEach(item => item.classList.add('simple-tabs__list_column_off'));
        const content = activePanelNodeCopy.innerHTML; // [...activePanelNode.querySelectorAll('.simple-list')].map(list => list.outerHTML).join('');

        this._generatePopup(title, content);
        this._dispatchPopupOpenEvent(POPUP_NAME);
    }

    _generatePopup(title, popupContent) {
        const content = `
            <div class="popup__container">
                <div class="popup__wrapper">
                    <div class="popup__title">${title}</div>

                    <div class="popup__scroll">
                        ${popupContent}

                        <div class="popup__close" data-action="close-popup" data-name="${POPUP_NAME}">
                            <div class="popup__circle">
                                <svg width="13" height="13" viewBox="0 0 13 13" fill="none" xmlns="http://www.w3.org/2000/svg">
                                    <path fill-rule="evenodd" clip-rule="evenodd"
                                        d="M0.292893 0.293191C0.683417 -0.0973337 1.31658 -0.0973338 1.70711 0.293191L6.12026 4.70634L10.5336 0.2929C10.9241 -0.0976283 11.5573 -0.097634 11.9478 0.292887C12.3384 0.683408 12.3384 1.31657 11.9478 1.7071L7.53447 6.12055L11.9476 10.5337C12.3382 10.9242 12.3382 11.5574 11.9476 11.9479C11.5571 12.3384 10.9239 12.3384 10.5334 11.9479L6.12026 7.53476L1.70746 11.9475C1.31693 12.338 0.683763 12.338 0.293243 11.9475C-0.0972782 11.5569 -0.0972724 10.9238 0.293255 10.5333L4.70604 6.12055L0.292893 1.7074C-0.0976308 1.31688 -0.0976312 0.683715 0.292893 0.293191Z"
                                        fill="#AEB8D5" />
                                </svg>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        `;

        const popupWnd = this._createWindow('simple-tabs-popup', POPUP_NAME);
        popupWnd.innerHTML = content;

        Layout.root.appendChild(popupWnd);
    }
}

export default SimpleTabsPopup;
