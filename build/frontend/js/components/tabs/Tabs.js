'use strict';

import Layout from 'js/helpers/Layout';

class Tabs {
    constructor(node) {
        this.node = node;

        if (!this.node) return;

        this.node['_component'] = this;

        this.onTabClick = this.onTabClick.bind(this);

        this.tabsList = this.node.querySelector('.tabs__list-fixable');
        this.contentNode = this.node.querySelector('.tabs__content');
        this.hintCount = 0;

        this.tabsList.addEventListener('click', this.onTabClick, false);
    }

    onTabClick(event) {
        const tab = event.target.closest('.tabs__tab');
        const tabIndex = this.getTabIndex(tab);

        if (!tab) return;

        if (tab.classList.contains('tabs__tab_disabled')) {
            this.hintCount < 1 ? this.generateHint(tab, 'Раздел в разработке') : null;
        } else {
            this.unsetActiveTab();
            this.setActiveTab(tab, tabIndex);
        }
    }

    getTabIndex(tab) {
        return [...this.tabsList.children].indexOf(tab);
    }

    getNode() {
        return this.node;
    }

    generateHint(tab, text) {
        const content = `
            <div class="hint__content">
                ${text}

                <div class="hint__arrow"></div>
            </div>
        `;

        const hintWnd = this.generateHintWnd();

        hintWnd.innerHTML = content;
        hintWnd.style.top = tab.getBoundingClientRect().top + 'px';
        hintWnd.style.left = tab.getBoundingClientRect().left + 150 + 'px';

        Layout.body.appendChild(hintWnd);
        this.hintCount++;

        setTimeout(() => {
            hintWnd.remove();
            this.hintCount--;
        }, 3000);
    }

    generateHintWnd() {
        const hintWnd = document.createElement('div');

        hintWnd.classList.add('hint', 'hint-tab');

        return hintWnd;
    }

    getAcitveTabNode() {
        return this.tabsList.querySelector('.tabs__tab_selected');
    }

    getAcitvePanelNode() {
        return this.contentNode.querySelector('.tabs__panel_selected');
    }

    unsetActiveTab() {
        [...this.tabsList.children].forEach(tab => tab.classList.remove('tabs__tab_selected'));
        [...this.contentNode.children].forEach(panel => panel.classList.remove('tabs__panel_selected'));
    }

    setActiveTab(tab, tabIndex) {
        tab.classList.add('tabs__tab_selected');
        this.contentNode.children[tabIndex].classList.add('tabs__panel_selected');

        this._dispatchChangeTabEvent(this.contentNode.children[tabIndex]);
    }

    _dispatchChangeTabEvent(container) {
        const event = new CustomEvent('tab:change', {
            bubbles: true,
            cancelable: true,
            detail: {
                component: this,
                container: container
            }
        });

        this.node.dispatchEvent(event);
    }
}

export default Tabs;
