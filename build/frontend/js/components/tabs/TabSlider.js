'use strict';

import ActionMapper from 'js/helpers/ActionMapper';
import Widget from 'js/components/Widget';

class TabSlider {
    constructor(node) {
        this.node = node;

        if (!this.node) return;

        this.node['_component'] = this;

        this.panels = [...this.node.querySelectorAll('.tab-slider__panel')];
        this.progressBar = null;

        this.actions = {
            'next-slide': 'nextTab',
            'prev-slide': 'prevTab'
        };

        this._bindEvents();

        Widget.onReady(Widget.findNodes(this.node, 'progress-bar'))
              .then(widgets => widgets[0] && this.setProgressBar(widgets[0]));
    }

    setProgressBar(progressBar) {
        this.progressBar = progressBar;
        this.progressBar.set(this.getProgress());
    }

    _bindEvents() {
        this.onClick = this.onClick.bind(this);

        this.node.addEventListener('click', this.onClick, false);
    }

    hasProgressBar() {
        return !!this.progressBar;
    }

    onClick(e) {
        ActionMapper.callAction(this, this.actions, e.target);
    }

    nextTab(node) {
        let tabIndex = this.getActiveTabIndex() || 0;

        this.unsetActiveTab();
        this.setActiveTab(++tabIndex);
    }

    prevTab(node) {
        let tabIndex = this.getActiveTabIndex() || 0;

        this.unsetActiveTab();
        this.setActiveTab(--tabIndex);
    }

    getActiveTabIndex() {
        const activeTab = this.getActiveTab();

        if (!activeTab) return null;

        return this.panels.indexOf(activeTab);
    }

    unsetActiveTab() {
        const activeTab = this.getActiveTab();

        if (!activeTab) return;

        activeTab.classList.remove('tab-slider__panel_selected');
    }

    getActiveTab() {
        return this.node.querySelector('.tab-slider__panel_selected');
    }

    setActiveTab(tabIndex) {
        if (tabIndex < 0)
            tabIndex = 0;

        if (tabIndex > this.panels.length - 1)
            tabIndex = this.panels.length - 1;

        this.panels[tabIndex].classList.add('tab-slider__panel_selected');

        if (this.hasProgressBar()) {
            this.progressBar.set(this.getProgress());
        }

        this._dispatchChangeTabEvent();
    }

    _dispatchChangeTabEvent() {
        const event = new CustomEvent('tab-slider:change', {
            bubbles: true,
            cancelable: true,
            detail: {
                component: this
            }
        });

        this.node.dispatchEvent(event);
    }

    getNode() {
        return this.node;
    }

    isLast() {
        return this.getActiveTabIndex() === (this.panels.length - 1);
    }

    getProgress() {
        const index = this.getActiveTabIndex() || 0;

        return (index + 1) / this.panels.length * 100;
    }

    reset() {
        this.unsetActiveTab();
        this.setActiveTab(0);
    }
}

export default TabSlider;
