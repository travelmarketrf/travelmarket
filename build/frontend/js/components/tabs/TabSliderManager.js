'use strict';

import Layout from 'js/helpers/Layout';
import Widget from 'js/components/Widget';

class TabSliderManager {
    constructor() {
        this.onPopupClose = this.onPopupClose.bind(this);

        Layout.body.addEventListener('popup:close', this.onPopupClose, false);
    }

    onPopupClose(e) {
        const tabSlider = Widget.get(e.detail.popupNode.querySelector('[data-widget="tab-slider"]'));

        if (!tabSlider) return;

        tabSlider.reset();
    }
}

export default TabSliderManager;
