'use strict';

class Gallery {
    constructor(node) {
        this.node = node;

        if (!this.node) return;

        this.node['_component'] = this;
    }

    getImages() {
        const array = [];

        [...this.node.querySelectorAll('.miniphoto__img > img')]
            .forEach(image => array.push(image.getAttribute('src')));

        return array;
    }

    getMiniphotoIndex(node) {
        return [...this.node.children].indexOf(node);
    }
}

export default Gallery;
