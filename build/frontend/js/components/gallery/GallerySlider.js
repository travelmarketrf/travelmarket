'use strict';

import Swiper from 'swiper';
import MD from 'js/helpers/MD';
import Component from 'js/components/Component';

class GallerySlider extends Component {
    constructor(node) {
        super(node);

        this.sliderPerView = 6;

        if (MD.mobile()) {
            this.sliderPerView = 5;

            if (MD.phone()) {
                this.sliderPerView = 3;
            }
        }

        this.index = this.node.getAttribute('data-start-index');

        this.thumbs = new Swiper(this.node.querySelector('.slider-gallery__bottom'), {
            spaceBetween: 0,
            slidesPerView: this.sliderPerView,
            freeMode: true,
            freeMode: true,
            watchSlidesVisibility: true,
            watchSlidesProgress: true,
            spaceBetween: 1,
            direction: 'horizontal'
        });

        this.swiper = new Swiper(this.node.querySelector('.slider-gallery__top'), {
            navigation: {
                nextEl: this.node.querySelector('.slider-gallery__bottom_arrow_next'),
                prevEl: this.node.querySelector('.slider-gallery__bottom_arrow_prev'),
            },
            thumbs: {
                swiper: this.thumbs
            }
        });

        this.swiper.slideTo(this.index, 0);
    }
}

export default GallerySlider;
