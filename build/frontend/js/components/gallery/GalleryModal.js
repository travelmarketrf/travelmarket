'use strict';

import Widget from 'js/components/Widget';
import Layout from 'js/helpers/Layout';
import CustomizablePopup from 'js/components/managers/CustomizablePopup';

const MODAL_NAME = 'gallery-modal'

class GalleryModal extends CustomizablePopup {
    constructor() {
        super();

        this.actions = {
            'gallery-modal': 'open'
        }
    }

    open(node) {
        const gallery = Widget.get(node.closest('.album-group'));

        if (!gallery) return;

        const nodeIndex = gallery.getMiniphotoIndex(node);
        const images = gallery.getImages();

        this._generateModal(images, nodeIndex);
        this._dispatchPopupOpenEvent(MODAL_NAME);
    }

    _generateModal(images, nodeIndex) {
        const modal = `
            <div class="popup__container">
                <div class="popup__wrapper">
                    <div class="slider-gallery" data-widget="gallery-slider" data-start-index=${nodeIndex}>
                        <div class="swiper-container slider-gallery__top">
                            <div class="swiper-wrapper">
                                ${images.map(url => `
                                    <div class="swiper-slide">
                                        <img src="${url}">
                                    </div>
                                `).join('')}
                            </div>
                        </div>

                        <div class="swiper-container slider-gallery__bottom">
                            <div class="swiper-wrapper">
                                ${images.map(url => `<div class="swiper-slide" style="background: url('${url}') no-repeat"></div>`).join('')}
                            </div>

                            <span class="slider-gallery__bottom_arrow slider-gallery__bottom_arrow_prev">
                                <svg width="21" height="21" viewBox="0 0 21 21" fill="none" xmlns="http://www.w3.org/2000/svg">
                                    <g clip-path="url(#clip0)">
                                        <path
                                            d="M8.47717 10.5044L16.3332 2.64805C16.5495 2.43224 16.6685 2.14371 16.6685 1.83605C16.6685 1.52822 16.5495 1.23985 16.3332 1.02371L15.6448 0.335659C15.4289 0.119171 15.1401 0 14.8325 0C14.5248 0 14.2365 0.119171 14.0203 0.335659L4.66644 9.68937C4.44944 9.9062 4.33061 10.1959 4.33146 10.5039C4.33061 10.8133 4.44927 11.1027 4.66644 11.3197L14.0116 20.6643C14.2278 20.8808 14.5161 21 14.8239 21C15.1316 21 15.42 20.8808 15.6363 20.6643L16.3245 19.9763C16.7723 19.5285 16.7723 18.7994 16.3245 18.3518L8.47717 10.5044Z"
                                            fill="white" />
                                    </g>
                                    <defs>
                                        <clipPath id="clip0">
                                            <rect width="21" height="21" fill="white" transform="matrix(-1 0 0 1 21 0)" />
                                        </clipPath>
                                    </defs>
                                </svg>
                            </span>
                            <span class="slider-gallery__bottom_arrow slider-gallery__bottom_arrow_next">
                                <svg width="21" height="21" viewBox="0 0 21 21" fill="none" xmlns="http://www.w3.org/2000/svg">
                                    <g clip-path="url(#clip1)">
                                        <path
                                            d="M8.47717 10.5044L16.3332 2.64805C16.5495 2.43224 16.6685 2.14371 16.6685 1.83605C16.6685 1.52822 16.5495 1.23985 16.3332 1.02371L15.6448 0.335659C15.4289 0.119171 15.1401 0 14.8325 0C14.5248 0 14.2365 0.119171 14.0203 0.335659L4.66644 9.68937C4.44944 9.9062 4.33061 10.1959 4.33146 10.5039C4.33061 10.8133 4.44927 11.1027 4.66644 11.3197L14.0116 20.6643C14.2278 20.8808 14.5161 21 14.8239 21C15.1316 21 15.42 20.8808 15.6363 20.6643L16.3245 19.9763C16.7723 19.5285 16.7723 18.7994 16.3245 18.3518L8.47717 10.5044Z"
                                            fill="white" />
                                    </g>
                                    <defs>
                                        <clipPath id="clip1">
                                            <rect width="21" height="21" fill="white" transform="matrix(-1 0 0 1 21 0)" />
                                        </clipPath>
                                    </defs>
                                </svg>
                            </span>
                        </div>
                    </div>

                    <div class="popup__close" data-action="close-popup" data-name="${MODAL_NAME}">
                        <div class="popup__circle">
                            <svg width="13" height="13" viewBox="0 0 13 13" fill="none" xmlns="http://www.w3.org/2000/svg">
                                <path fill-rule="evenodd" clip-rule="evenodd"
                                    d="M0.292893 0.293191C0.683417 -0.0973337 1.31658 -0.0973338 1.70711 0.293191L6.12026 4.70634L10.5336 0.2929C10.9241 -0.0976283 11.5573 -0.097634 11.9478 0.292887C12.3384 0.683408 12.3384 1.31657 11.9478 1.7071L7.53447 6.12055L11.9476 10.5337C12.3382 10.9242 12.3382 11.5574 11.9476 11.9479C11.5571 12.3384 10.9239 12.3384 10.5334 11.9479L6.12026 7.53476L1.70746 11.9475C1.31693 12.338 0.683763 12.338 0.293243 11.9475C-0.0972782 11.5569 -0.0972724 10.9238 0.293255 10.5333L4.70604 6.12055L0.292893 1.7074C-0.0976308 1.31688 -0.0976312 0.683715 0.292893 0.293191Z"
                                    fill="#AEB8D5" />
                            </svg>
                        </div>
                    </div>
                </div>
            </div>
        `

        const modalWnd = this._createWindow('gallery-modal', MODAL_NAME);
        modalWnd.innerHTML = modal;

        Layout.root.appendChild(modalWnd);
    }
}

export default GalleryModal;
