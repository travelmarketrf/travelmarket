'use strict';

import Layout from 'js/helpers/Layout';
import Cookie from 'js/helpers/Cookie';

class CheckOnVisit {
    constructor() {
        this.showOrSkip();
    }

    showOrSkip() {
        if (Cookie.get('visited')) return;

        Cookie.set('visited=true');
        this._dispatchLocationPopupOpenEvent();
    }

    _dispatchLocationPopupOpenEvent() {
        const event = new CustomEvent('popup-manager:open', {
            bubbles: true,
            cancelable: true,
            detail: {
                component: this,
                value: 'location-popup'
            }
        });

        Layout.body.dispatchEvent(event);
    }
}

export default CheckOnVisit;
