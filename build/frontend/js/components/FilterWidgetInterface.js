'use strict';

/**
 * Interface for classes that represent a filter widget.
 *
 * @interface
 * @author Chistyakov I. V. <ichistyakovv@gmail.com>
 */
class FilterWidgetInterface {

    /**
     * Whether the widget is active (checked, selected, etc.).
     *
     * @abstract
     * @returns {boolean}
     */
    isActive() {}

    /**
     * Get the root node.
     *
     * @abstract
     * @returns {HTMLElement} The root element of the widget.
     */
    getNode() {}

    /**
     * Reset the current state to default one.
     *
     * @abstract
     * @returns {boolean} The result of resetting.
     */
    reset() {}

    /**
     * Activate the widget (check, select, etc.).
     *
     * @abstract
     * @returns {boolean} The result of activating.
     */
    choose() {}
}

export default FilterWidgetInterface;
