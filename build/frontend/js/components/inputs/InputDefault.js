'use strict';

import Stringz from 'js/Helpers/Stringz';
import Component from 'js/components/Component';
import ElementFaker from 'js/helpers/ElementFaker';

class InputDefault extends Component {
    constructor(node) {
        super(node);

        this._crossNode = this.node.querySelector('.form-input__icon_cross') ||  null;
        this._input = this.node.querySelector('input');

        this._bindEvents();
    }

    _bindEvents() {
        this.remove = this.remove.bind(this);

        if (this.isRemovable())
            this._crossNode.addEventListener('click', this.remove, false);
    }

    getCurrentData() {
        return [{
            name: this.getName(),
            value: Stringz.stripTags( this.getValue() ),
            text: Stringz.stripTags( this.getLabelValue() )
        }];
    }

    getName() {
        return this._input.getAttribute('name') || this._input.getAttribute('data-name');
    }

    getValue() {
        return this._input.value;
    }

    getLabelValue() {
        return this._input.value;
    }

    getNode() {
        return this.node;
    }

    remove() {
        this._dispatchRemoveEvent();
    }

    clone() {
        const clonedNode = this.node.cloneNode(true);
        const html = ElementFaker.getHtml(clonedNode);

        return html;
    }

    makeRemovable() {
        if (this.isRemovable()) return;

        this._crossNode = document.createElement('div');

        this._crossNode.classList.add('form-input__icon', 'form-input__icon_cross');
        this._crossNode.innerHTML = `<svg width="26" height="26" viewBox="0 0 26 26" fill="none" xmlns="http://www.w3.org/2000/svg">
        <path fill-rule="evenodd" clip-rule="evenodd" d="M6.94307 6.47686C7.33359 6.08633 7.96675 6.08633 8.35728 6.47686L13.2103 11.3299L18.0718 6.46828C18.4624 6.07775 19.0955 6.07775 19.4861 6.46827C19.8766 6.85879 19.8766 7.49196 19.4861 7.88249L14.6245 12.7441L19.4776 17.5972C19.8681 17.9877 19.8681 18.6209 19.4776 19.0114C19.0871 19.4019 18.4539 19.4019 18.0634 19.0114L13.2103 14.1583L8.3494 19.0192C7.95887 19.4097 7.32571 19.4097 6.93519 19.0192C6.54467 18.6286 6.54468 17.9955 6.9352 17.605L11.7961 12.7441L6.94307 7.89107C6.55254 7.50055 6.55254 6.86738 6.94307 6.47686Z" fill="#AEB8D5"/>
    </svg>`;

        this.node.classList.add('form__field_cross');
        this.node.appendChild(this._crossNode);
        this._crossNode.addEventListener('click', this.remove, false);

        return this;
    }

    isRemovable() {
        return !!this._crossNode;
    }

    reset() {
        this._input.value = '';
        this._input.classList.remove('form-input_filled');
    }

    _dispatchRemoveEvent() {
        const event = new CustomEvent('input:remove', {
            bubbles: true,
            cancelable: true,
            detail: {
                component: this
            }
        });
        this.node.dispatchEvent(event);
    }
}

export default InputDefault;
