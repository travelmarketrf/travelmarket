'use strict';

class Months {
    constructor(node) {
        this.node = node;

        if (!this.node) return;

        this.node['_component'] = this;

        this.onClick = this.onClick.bind(this);

        this.node.addEventListener('click', this.onClick, false);
    }

    onClick(e) {
        const item = e.target.closest('.months__item');

        if (!item) return;

        this.unsetActiveItem();
        this.setActiveItem(item);
    }

    unsetActiveItem() {
        [...this.node.querySelectorAll('.months__item')].forEach(item => item.classList.remove('months__item_active'));
    }

    setActiveItem(item) {
        item.classList.add('months__item_active');
    }
}

export default Months;
