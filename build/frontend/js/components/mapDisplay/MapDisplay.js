'use strict';

import Layout from 'js/helpers/Layout';
import Widget from 'js/components/Widget';
import MD from 'js/helpers/MD';
import ScrollLocker from 'js/helpers/ScrollLocker';

class MapDisplay {
    constructor() {
        this.toggle = this.toggle.bind(this);
        this.onResize = this.onResize.bind(this);

        this.map = Layout.body.querySelector('.aside-map');
        this.minicardGrid = Layout.body.querySelector('.minicard-grid');
        this.layoutCenter = Layout.body.querySelector('.col-dynamic-layout__center');
        this.layoutRight = Layout.body.querySelector('.col-dynamic-layout__right');

        Layout.body.addEventListener('input:change', this.toggle, false);
        Layout.body.addEventListener('mobile-map:open', this.toggle, false);
        Layout.body.addEventListener('mobile-map:close', this.toggle, false);
        window.addEventListener('resize', this.onResize, false);

        this._init();
    }

    _init() {
        this._detectCardsOrientation();

        if (MD.betweenResolution()) {
            this._hideMap();
        }
    }

    _detectCardsOrientation() {
        if (!MD.mobile()) {
            this.horizontalCard();
        } else {
            this.verticalCard();
        }
    }

    onResize() {
        setTimeout(() => this._detectCardsOrientation(), 50);
    }

    horizontalCard() {
        this.getMinicards().forEach(minicard => minicard.classList.add('minicard_view_horizontal'));

        this.getSliders().forEach(slider => {
            slider.node.classList.add('minicard-slider_view_horizontal');
            slider.update();
        });
    }

    verticalCard() {
        this.getMinicards().forEach(minicard => minicard.classList.remove('minicard_view_horizontal'));

        this.getSliders().forEach(slider => {
            slider.node.classList.remove('minicard-slider_view_horizontal');
            slider.update();
        });
    }

    getMinicards() {
        return [...this.minicardGrid.querySelectorAll('.minicard')];
    }

    getSliders() {
        return [...this.minicardGrid.querySelectorAll('[data-widget="minicard-slider"]')]
            .map(node => Widget.get(node));
    }

    toggle(e) {
        const state = e.detail.value;

        if (state) {
            this._showMap();
        } else {
            this._hideMap();
        }
    }

    _hideMap() {
        this.map.classList.add('aside-map_hidden');
        this.layoutCenter.classList.add('col-dynamic-layout__center_max');
        this.layoutRight.classList.add('col-dynamic-layout__right_hidden');

        if (!MD.mobile()) {
            this.minicardGrid.classList.add('minicard-grid_map_off');

            if (!MD.betweenResolution()) {
                this.verticalCard();
            }
        }

        this._dispatchHideMapEvent();
    }

    _showMap() {
        this.minicardGrid.classList.remove('minicard-grid_map_off');
        this.map.classList.remove('aside-map_hidden');
        this.layoutCenter.classList.remove('col-dynamic-layout__center_max');
        this.layoutRight.classList.remove('col-dynamic-layout__right_hidden');

        if (!MD.mobile()) {
            this.horizontalCard();
        }
    }

    _dispatchHideMapEvent() {
        const event = new CustomEvent('map:hide', {
            bubbles: true,
            cancelable: true,
            detail: {
                component: this
            }
        });

        Layout.body.dispatchEvent(event);
    }
}

export default MapDisplay;
