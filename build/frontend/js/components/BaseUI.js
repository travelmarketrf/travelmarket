'use strict';

import Layout from 'js/helpers/Layout';
import Widget from 'js/components/Widget';
import WidgetFactory from 'js/components/WidgetFactory';
import GlobalExports from 'js/helpers/GlobalExports';
import Preloader from 'js/components/preloader/Preloader';
import SimplePreloader from 'js/components/preloader/SimplePreloader';

class BaseUI {
    constructor(config) {
        this.config = config;
        this.preloder = new Preloader();
        this.simplePreloader = new SimplePreloader();

        WidgetFactory.defineWidgets(this.config.widgets);
        WidgetFactory.defineFactories(this.config.factories);

        GlobalExports.add('preloader', this.preloder);
        GlobalExports.add('sPreloader', this.simplePreloader);

        this._bindEvents();
    }

    _bindEvents() {
        this.onRefresh = this.onRefresh.bind(this);
        Layout.body.addEventListener('ui:refresh', this.onRefresh);
    }

    onRefresh(e) {
        this.refresh(e.detail.container);

        if (e.detail.cb)
            e.detail.cb();
    }

    refresh(container) {
        container = container || Layout.body;
        Widget
            .findNodes(container)
            .forEach(node => Widget.instantiate(node));
    }
}

export default BaseUI;
