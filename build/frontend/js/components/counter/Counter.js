'use strict';

import Component from 'js/components/Component';
import Declinations from 'js/components/declinations/Declinations';

class Counter extends Component {
    constructor(node) {
        super(node);

        this.counterNode = this.node.querySelector('.selection__counter');
        this.minusNode = this.node.querySelector('.selection__button-minus');
        this.plusNode = this.node.querySelector('.selection__button-plus');
        this.textNode = this.node.querySelector('.selection__text');
        this.input = this.node.querySelector('.selection__input');

        this.count = this._getInitialValue();
        this.step = 1;

        this._init();
        this._bindEvents();
    }

    _init() {
        this.declText = this.node.getAttribute('data-decl') ? JSON.parse(this.node.getAttribute('data-decl')) : null;
        this.node.removeAttribute('data-decl');
        this.declinations = new Declinations(this.declText);
        this._setValue(this.count);
    }

    _getInitialValue() {
        const val = parseInt(this.input.value, 10);

        return !isNaN(val) ? val : 0;
    }

    _bindEvents() {
        this.onMinusClick = this.onMinusClick.bind(this);
        this.onPlusClick = this.onPlusClick.bind(this);
        this.minusNode.addEventListener('click', this.onMinusClick, false);
        this.plusNode.addEventListener('click', this.onPlusClick, false);
    }

    onMinusClick() {
        if (this.count - this.step < 0) return;

        this.dec(this.step);
        this._dispatchCountChangeEvent();
    }

    setStep(step) {
        this.step = step;
    }

    onPlusClick() {
        this.inc(this.step);
        this._dispatchCountChangeEvent();
    }

    hasTextLable() {
        return !!this.textNode;
    }

    setText(text) {
        if (!this.hasTextLable()) return;

        this.textNode.innerHTML = text;
    }

    getText() {
        if (!this.hasTextLable())
            return `${this.counterNode.textContent}`;

        return `${this.counterNode.textContent} ${this.textNode.textContent}`;
    }

    inc(step) {
        this.count += step;
        this._setValue(this.count);
    }

    dec(step) {
        this.count -= step;
        this._setValue(this.count);
    }

    getValue() {
        return this.count;
    }

    _setValue(value) {
        this.count = value;
        this.input.value = value;
        this.counterNode.innerHTML = value;
        this.setText(this.declinations.trans(value));
    }

    reset() {
        this._setValue(0);
    }

    _dispatchCountChangeEvent() {
        const event = new CustomEvent('counter:change', {
            bubbles: true,
            cancelable: true,
            detail: {
                component: this,
                value: this.getValue()
            }
        });

        this.node.dispatchEvent(event);
    }
}

export default Counter;
