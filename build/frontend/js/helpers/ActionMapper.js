'use strict';

class ActionMapper {
    static callAction(instance, map, node) {
        const actionNode = node.closest('[data-action]');

        if (!actionNode) return;

        const actionName = actionNode.getAttribute('data-action');

        if (!actionName || !map[actionName] || !instance[map[actionName]]) return;

        instance[map[actionName]](actionNode);
    }
}

export default ActionMapper;
