'use strict';

class Cookie {
    static get(name) {
        const matches = document.cookie.match(new RegExp(
            '(?:^|; )' + name.replace(/([\.$?*|{}\(\)\[\]\\\/\+^])/g, '\\$1') + '=([^;]*)'
        ));

        return matches ? true : false; // decodeURIComponent(matches[1])
    }

    static set(name) {
        let date = new Date();
        date.setDate(date.getDate() + 30);
        let expires = date.toUTCString()

        document.cookie = name + ';' + 'expires=' + expires;
    }

    static read(name) {
        let cookieName = `${name}=`;
        let spl = document.cookie.split(';');

        for (let i = 0; i < spl.length; i++) {
            let cookie = spl[i];

            while (cookie.charAt(0) == '') {
                cookie = cookie.substring(1, cookie.length);
            }

            if (cookie.indexOf(cookieName) == 0) {
                return cookie.substring(cookieName.length, cookie.length);
            }
        }

        return null;
    }
}

export default Cookie;
