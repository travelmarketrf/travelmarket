'use strict';

import Layout from 'js/helpers/Layout';

let COUNTER = 0;

class ScrollLocker {
    static lock() {
        COUNTER++;

        Layout.body.classList.add('scroll-lock');
    }

    static unlock() {
        COUNTER--;

        if (COUNTER < 0) {
            COUNTER = 0;
        }

        if (COUNTER === 0) {
            Layout.body.classList.remove('scroll-lock');
        }
    }
}

export default ScrollLocker;
