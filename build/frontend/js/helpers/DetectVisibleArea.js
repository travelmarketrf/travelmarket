'use strict';

class DetectVisibleArea {
    static detect(block, element, node = window) {
        const difference = DetectVisibleArea.calcDifference(block, element);

        if (DetectVisibleArea.isOutOfScreen(block, element, node)) {
            element.style.top = -element.offsetHeight - difference + 'px';

            return true;
        }

        DetectVisibleArea.refresh(element);

        return false;
    }

    static refresh(element) {
        element.style.top = 0;
    }

    static isOutOfScreen(block, element, node = window) {
        const difference = DetectVisibleArea.calcDifference(block, element);

        if (node.innerHeight < block.getBoundingClientRect().top + difference + element.offsetHeight)
            return true;

        return false;
    }

    static calcDifference(block, element) {
        const offset = getComputedStyle(element).top;

        element.style.top = 0;

        const diff = element.getBoundingClientRect().top - block.getBoundingClientRect().top;

        element.style.top = offset;

        return diff;
    }
}

export default DetectVisibleArea;
