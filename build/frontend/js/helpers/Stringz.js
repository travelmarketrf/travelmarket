'use strict';

class Stringz {
    static stripTags(html) {
        const node = document.createElement('div');
        node.innerHTML = html;
        return node.textContent;
    }
}

export default Stringz;
