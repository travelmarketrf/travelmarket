'use strict';

class DomHelper {
    static documentHeight() {
        const scrollHeight = Math.max(
          document.body.scrollHeight, document.documentElement.scrollHeight,
          document.body.offsetHeight, document.documentElement.offsetHeight,
          document.body.clientHeight, document.documentElement.clientHeight
        );

        return scrollHeight;
    }

    static documentWidth() {
        const scrollWidth = Math.max(
          document.body.scrollWidth, document.documentElement.scrollWidth,
          document.body.offsetWidth, document.documentElement.offsetWidth,
          document.body.clientWidth, document.documentElement.clientWidth
        );

        return scrollWidth;
    }

    static windowHeight() {
        return document.documentElement.clientHeight;
    }

    static windowWidth() {
        return document.documentElement.clientWidth;
    }

    static scrollLeft() {
        return window.pageXOffset || document.documentElement.scrollLeft;
    }

    static scrollTop() {
        return window.pageYOffset || document.documentElement.scrollTop;
    }

    static setScrollTop(value) {
        document.documentElement.scrollTop = value;
        document.body.scrollTop = value;
    }

    static getScrollDirection(e) {
        return Math.max(-1, Math.min(1, this.getScrollDelta(e)));
    }

    static getScrollDelta(e) {
        return (e.wheelDelta || -e.detail || -e.deltaY);
    }

    static getScrollbarSizes(node) {
        const prevOverflow = node.style.overflow;
        const prevOverflowX = node.style.overflowX;
        const prevOverflowY = node.style.overflowY;

        node.style.overflow = 'scroll';
        node.style.overflowX = 'scroll';
        node.style.overflowY = 'scroll';

        const height = node.offsetHeight - node.clientHeight;
        const width = node.offsetWidth - node.clientWidth;

        node.style.overflow = prevOverflow;
        node.style.overflowX = prevOverflowX;
        node.style.overflowY = prevOverflowY;

        return {width, height};
    }
}

export default DomHelper;
