'use strict';

class ElementFaker {
    static getHtml(srcNode) {
        const node = document.createElement('div');

        node.style.display = 'none';
        node.appendChild(srcNode);
        document.body.appendChild(node);

        const html = srcNode.outerHTML;

        node.remove();

        return html;
    }
}

export default ElementFaker;
