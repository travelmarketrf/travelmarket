'use strict';

import Layout from 'js/helpers/Layout';

class EventHelper {
    static refreshUI(node, container) {
        node = node || Layout.body;

        return new Promise(resolve => {
            const event = new CustomEvent('ui:refresh', {
                bubbles: true,
                cancelable: true,
                detail: {container, cb: () => resolve()}
            });
    
            node.dispatchEvent(event);
        });
    }
}

export default EventHelper;
