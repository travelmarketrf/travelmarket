'use strict';

export default {
    body: document.body,
    root: document.querySelector('#root'),
    page: document.querySelector('.page'),
    header: document.querySelector('header'),
    headerBottom: document.querySelector('.header__bottom'),
};
