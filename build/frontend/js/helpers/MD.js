'use strict';

import DomHelper from 'js/helpers/DomHelper';

const PHONE_WIDTH = 767;
const MOBILE_WIDTH = 1024;
const BETWEEN_WIDTH = 1600;
const DESKTOP_LOW = 1300;

class MD {
    static mobile() {
        return DomHelper.windowWidth() <= MOBILE_WIDTH;
    }

    static deskLow() {
        return DomHelper.windowWidth() <= DESKTOP_LOW;
    }

    static phone() {
        return DomHelper.windowWidth() <= PHONE_WIDTH;
    }

    static betweenResolution() {
        return DomHelper.windowWidth() <= BETWEEN_WIDTH;
    }

    static getRatio() {
        return DomHelper.windowWidth() / DomHelper.windowHeight();
    };

    static isVertical() {
        return MD.getRatio() < 1;
    };

    static isHorizontal() {
        return MD.getRatio() > 1 && DomHelper.windowWidth() > 500;
    };
}

export default MD;
