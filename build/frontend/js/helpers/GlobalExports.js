'use strcit';

import Widget from 'js/components/Widget';

class GlobalExports {
    static export() {
        for (let name in GlobalExports.items) {
            window[name] = GlobalExports.items[name];
        }
    }

    static add(name, item) {
        GlobalExports.items[name] = item;
    }
}

GlobalExports.items = {
    'Widget': Widget
};

export default GlobalExports;
