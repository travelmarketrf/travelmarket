'use strict';

class Ajax {
    static get(url) {
        return Ajax._fetch(url);
    }

    static post(url, body = null) {
        return Ajax._fetch(url, 'POST', body);
    }

    static _fetch(url, method = 'GET', body = null) {
        return fetch(url, {
            method: method,
            headers: {
                'Content-Type': 'application/json',
                'x-requested-with': 'XMLHttpRequest'
            },
            body: body
        })
        .then(result => {
            return result.json();
        });
    }
}

export default Ajax;
