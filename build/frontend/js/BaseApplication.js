'use strict';

import Layout from 'js/helpers/Layout';

class BaseApplication {
    run() {
        const event = new CustomEvent('app:init', {
            bubbles: true,
            cancelable: true,
            detail: {}
        });

        Layout.body.dispatchEvent(event);
    }
}

export default BaseApplication;
