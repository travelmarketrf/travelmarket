'use strict';

import Component from 'js/components/Component';
import Widget from 'js/components/Widget';

class ContactsMap extends Component {
    constructor(node) {
        super(node);

        this.mapNode = this.node.querySelector('.map');
        this.addressNode = this.node.querySelector('.agency__contacts-address');
        this.mainOfficeNode = this.node.querySelector('.agency__contacts-office');
        this.phoneNode = this.node.querySelector('.agency__contacts-phone');
        this.emailNode = this.node.querySelector('.agency__contacts-email');
        this.worktimeNode = this.node.querySelector('.agency__contacts-worktime');

        this._map = null;

        Widget.onReady([this.mapNode]).then(widgets => this._init(widgets));
    }

    _init(widgets) {
        this._map = widgets[0];

        this._bindEvents();
    }

    _bindEvents() {
        this.onMarkerSet = this.onMarkerSet.bind(this);
        this.node.addEventListener('map:set-marker', this.onMarkerSet, false);
    }

    onMarkerSet(e) {
        const data = this._map.getMarkerDataById(e.detail.id);

        if (!data) return;

        this.addressNode.innerHTML = data.content.text;
        this.phoneNode.innerHTML = data.content.phone;
        this.emailNode.innerHTML = data.content.email;
        this.worktimeNode.innerHTML = data.content.worktime;

        if (data.content.isMain) {
            this.mainOfficeNode.classList.remove('agency__contacts-office_hidden');
        } else {
            this.mainOfficeNode.classList.add('agency__contacts-office_hidden');
        }
    }
}

export default ContactsMap;
