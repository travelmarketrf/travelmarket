'use strict';

import Select from 'js/components/select/Select';
import MinicardSlider from 'js/components/sliders/MinicardSlider';
import Switch from 'js/components/switch/Switch';
import EmbeddableSelect from 'js/components/select/EmbeddableSelect';
import Tabs from 'js/components/tabs/Tabs';
import SliderRange from 'js/components/range/SliderRange';
import Counter from 'js/components/counter/Counter';
import SelectDefault from 'js/components/select/SelectDefault';
import Accordeon from 'js/components/accordeon/Accordeon';
import Checkbox from 'js/components/checkbox/Checkbox';
import CalendarRange from 'js/components/calendar/CalendarRange';
import FoldableList from 'js/components/foldables/FoldableList';
import CustomRadio from 'js/components/customRadio/CustomRadio';
import SelectRadioInteraction from 'js/components/selectRadioInteraction/SelectRadioInteraction';
import StarsRating from 'js/components/starsRating/StarsRating';
import Filter from 'js/components/filter/Filter';
import SortList from 'js/components/filter/SortList';
import SliderGraph from 'js/components/filter/SliderGraph';
import Radio from 'js/components/radio/Radio';
import TabSlider from 'js/components/tabs/TabSlider';
import ProgressBar from 'js/components/progressBar/ProgressBar';
import MobileFilter from 'js/components/mobile/MobileFilter';
import Review from 'js/components/review/Review';
import MobileFilterMenu from 'js/components/mobile/MobileFilterMenu';
import Dropdown from 'js/components/dropdown/Dropdown';
import MinicardView from 'js/components/minicard/MinicardView';
import ResizeMap from 'js/components/map/ResizeMap';
import Assessment from 'js/components/assessment/Assessment';
import ContactsMap from 'js/app/main/components/agency/ContactsMap';
import Traveler from 'js/components/traveler/Traveler';
import SwitchOnReview from 'js/components/review/SwitchOnReview';
import Favorites from 'js/components/favorites/Favorites';
import SimpleFilter from 'js/components/filter/SimpleFilter';
import TableSlider from 'js/components/table/TableSlider';
import ShPassword from 'js/components/showHidePassword/ShPassword';
import CardSliderGallery from 'js/components/sliders/CardSliderGallery';
import SuggestionSlider from 'js/components/sliders/SuggestionSlider';
import HotelMinicard from 'js/components/hotel/HotelMinicard';
import SwitchMenu from 'js/components/switchMenu/SwitchMenu';
import ProfileMenu from 'js/components/profileMenu/ProfileMenu';
import Months from 'js/components/months/Months';
import StickyBlock from 'js/components/stickyBlock/StickyBlock';
import Gallery from 'js/components/gallery/Gallery';
import Specialist from 'js/components/specialist/Specialist';
import GallerySlider from 'js/components/gallery/GallerySlider';

export default {
    'select': Select,
    'minicard-slider': MinicardSlider,
    'profile-menu': ProfileMenu,
    'switch': Switch,
    'tabs': Tabs,
    'foldable-list': FoldableList,
    'slider-range': SliderRange,
    'select-embeddable': EmbeddableSelect,
    'counter': Counter,
    'select-default': SelectDefault,
    'accordeon': Accordeon,
    'checkbox': Checkbox,
    'calendar-range': CalendarRange,
    'select-radio-interaction': SelectRadioInteraction,
    'custom-radio': CustomRadio,
    'stars-rating': StarsRating,
    'filter': Filter,
    'simple-radio': Radio,
    'tab-slider': TabSlider,
    'progress-bar': ProgressBar,
    'sort-list': SortList,
    'slider-graph': SliderGraph,
    'mobile-filter': MobileFilter,
    'review': Review,
    'mobile-filter-menu': MobileFilterMenu,
    'dropdown': Dropdown,
    'minicard-view': MinicardView,
    'resize-map': ResizeMap,
    'assessment': Assessment,
    'contacts-map': ContactsMap,
    'traveler': Traveler,
    'switch-review': SwitchOnReview,
    'favorites': Favorites,
    'simple-filter': SimpleFilter,
    'table-slider': TableSlider,
    'sh-password': ShPassword,
    'slider-gallery': CardSliderGallery,
    'suggestion-slider': SuggestionSlider,
    'hotel-minicard': HotelMinicard,
    'switch-menu': SwitchMenu,
    'months': Months,
    'sticky-block': StickyBlock,
    'gallery': Gallery,
    'specialist': Specialist,
    'gallery-slider': GallerySlider
};
