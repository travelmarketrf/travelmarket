'use strict';

import MainModules from 'js/modules/pages/main/MainModules';
import TourSearchModules from 'js/modules/pages/main/TourSearchModules';
import TourCardModules from 'js/modules/pages/main/TourCardModules';
import HotelCardModules from 'js/modules/pages/main/HotelCardModules';
import TourCardCruiseModules from 'js/modules/pages/main/TourCardCruiseModules';
import TourChooseModules from 'js/modules/pages/main/TourChooseModules';
import DirectionsModules from 'js/modules/pages/main/DirectionsModules';
import AgencyModules from 'js/modules/pages/main/AgencyModules';
import TourAgencyRatingModules from 'js/modules/pages/main/TourAgencyRatingModules';
import CountryCardModules from 'js/modules/pages/main/CountryCardModules';
import TourCardCompositeModules from 'js/modules/pages/main/TourCardCompositeModules';
import HotelChooseModules from 'js/modules/pages/main/HotelChooseModules';
import EventChooseModules from 'js/modules/pages/main/EventChooseModules';

export default {
    'main': MainModules,
    'tour-search': TourSearchModules,
    'tour-card': TourCardModules,
    'tour-card-cruise': TourCardCruiseModules,
    'tour-choose': TourChooseModules,
    'directions': DirectionsModules,
    'agency': AgencyModules,
    'tour-agency-rating': TourAgencyRatingModules,
    'country-card': CountryCardModules,
    'tour-card-composite': TourCardCompositeModules,
    'hotel-choose': HotelChooseModules,
    'hotel-card': HotelCardModules,
    'event-choose': EventChooseModules
};
