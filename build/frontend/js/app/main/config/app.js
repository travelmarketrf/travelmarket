'use strict';

import widgets from 'js/app/main/config/widgets';
import modules from 'js/app/main/config/modules';
import factories from 'js/app/main/config/factories';

export default {
    factories: factories,
    widgets: widgets,
    modules: modules
};
