'use strict';

import BaseUI from 'js/components/BaseUI';
import Layout from 'js/helpers/Layout';
import SelectManager from 'js/components/select/SelectManager';
import FixableHeaderManager from 'js/components/managers/FixableHeaderManager';
import PopupManager from 'js/components/managers/PopupManager';
import FixableBlockManager from 'js/components/managers/FixableBlockManager';
import TabsManager from 'js/components/managers/TabsManager';
import MapsManager from 'js/components/map/MapsManager';
import CheckOnVisit from 'js/components/checkOnVisit/CheckOnVisit';
import DropdownManager from 'js/components/dropdown/DropdownManager';
import CookieNotification from 'js/components/cookieNotification/CookieNotification';
import WidgetOverlayInterceptor from 'js/components/widgetOverlayInterceptor/WidgetOverlayInterceptor';
import TabSliderManager from 'js/components/tabs/TabSliderManager';
import MobileMainMenu from 'js/components/mobile/MobileMainMenu';
import ReviewPopup from 'js/components/review/ReviewPopup';
import HotelMinicardPopup from 'js/components/hotel/HotelMinicardPopup';
import FormInputManager from 'js/components/form/FormInputManager';
import SimpleTabsPopup from 'js/components/tabs/SimpleTabsPopup';
import TravelerPopup from 'js/components/traveler/TravelerPopup';
import Preloader from 'js/components/preloader/Preloader';
import Resize from 'js/components/resize/Resize';
import GalleryModal from 'js/components/gallery/GalleryModal';

class UI extends BaseUI {
    constructor(config) {
        super(config);

        this.selectManager = new SelectManager();
        this.fixableHeaderManager = new FixableHeaderManager();
        this.fixableBlockManager = new FixableBlockManager();
        this.popupManager = new PopupManager();
        this.tabsManager = new TabsManager();
        this.mapsManager = new MapsManager();
        this.checkOnVisit = new CheckOnVisit();
        this.cookieNotification = new CookieNotification();
        this.dropdownManager = new DropdownManager();
        this.widgetOverlayInterceptor = new WidgetOverlayInterceptor();
        this.tabSliderManager = new TabSliderManager();
        this.mobileMenu = new MobileMainMenu();
        this.reviewPopup = new ReviewPopup();
        this.hotelMinicardPopup = new HotelMinicardPopup();
        this.formInputManager = new FormInputManager();
        this.simpleTabsPopup = new SimpleTabsPopup();
        this.travelerPopup = new TravelerPopup();
        this.resize = new Resize();
        this.galleryModal = new GalleryModal();

        this.refresh();
    }

    _bindEvents() {
        super._bindEvents();

        this.onPopupOpen = this.onPopupOpen.bind(this);
        Layout.body.addEventListener('popup:open', this.onPopupOpen, false);
    }

    onPopupOpen() {
        if (!this.mobileMenu.isOpen())
            return;

        this.mobileMenu.close();
    }

    refresh(container) {
        super.refresh(container);

        this.mapsManager.refresh();
    }
}

export default UI;
