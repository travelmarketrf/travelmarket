'use strict';

import Dropdown from 'js/components/dropdown/Dropdown';
import ProfileMenu from 'js/components/profileMenu/ProfileMenu';
import TabSlider from 'js/components/tabs/TabSlider';
import ProgressBar from 'js/components/progressBar/ProgressBar';
import CardSelection from 'js/components/cardSelection/CardSelection';
import SubmitActivator from 'js/components/submitActivator/SubmitActivator';
import Tabs from 'js/components/tabs/Tabs';
import SelectDefault from 'js/components/select/SelectDefault';
import Checkbox from 'js/components/checkbox/Checkbox';
import StarsRating from 'js/components/starsRating/StarsRating';
import EmbeddableSelect from 'js/components/select/EmbeddableSelect';
import CalendarRange from 'js/components/calendar/CalendarRange';
import CalendarSingle from 'js/components/calendar/CalendarSingle';
import Counter from 'js/components/counter/Counter';
import CustomRadio from 'js/components/customRadio/CustomRadio';
import TinyEditor from 'js/components/editors/TinyEditor';
import Accordeon from 'js/components/accordeon/Accordeon';
import TourRoute from 'js/app/profile/components/route/TourRoute';
import Switch from 'js/components/switch/Switch';
import SelectCalendar from 'js/components/select/SelectCalendar';
import InputDefault from 'js/components/inputs/InputDefault';
import ScrollbarCompensator from 'js/components/scrollbar/ScrollbarCompensator';
import ConnectLinks from 'js/components/connectLinks/ConnectLinks';
import SimpleFilter from 'js/components/filter/SimpleFilter';
import SortablePhotos from 'js/components/draggable/SortablePhotos';
import MinicardSlider from 'js/components/sliders/MinicardSlider';
import ShPassword from 'js/components/showHidePassword/ShPassword';
import Cropper from 'js/components/cropper/Cropper';
import SwitchMenu from 'js/components/switchMenu/SwitchMenu';
import ChocolateMinicard from 'js/components/chocolateMinicard/ChocolateMinicard';
import CreativeTable from 'js/components/creativeTable/CreativeTable';
import ListDefault from 'js/components/list/ListDefault';
import TextCounter from 'js/components/textCounter/TextCounter';
import FillingLink from 'js/components/connectLinks/FillingLink';
import Chat from 'js/components/chat/Chat';
import ChatDialog from 'js/components/chat/ChatDialog';
import ChatMobile from 'js/components/chat/ChatMobile';
import Select from 'js/components/select/Select';
import SelectCheckbox from 'js/components/select/SelectCheckbox';
import SliderRange from 'js/components/range/SliderRange';
import Filter from 'js/components/filter/Filter';
import FoldableList from 'js/components/foldables/FoldableList';
import MobileFilterMenu from 'js/components/mobile/MobileFilterMenu';
import Review from 'js/components/review/Review';
import Assessment from 'js/components/assessment/Assessment';
import SuggestionSlider from "../../../components/sliders/SuggestionSlider";

export default {
    'dropdown': Dropdown,
    'profile-menu': ProfileMenu,
    'tab-slider': TabSlider,
    'progress-bar': ProgressBar,
    'card-selection': CardSelection,
    'submit-activator': SubmitActivator,
    'tabs': Tabs,
    'select-default': SelectDefault,
    'checkbox': Checkbox,
    'stars-rating': StarsRating,
    'select-embeddable': EmbeddableSelect,
    'suggestion-slider': SuggestionSlider,
    'calendar-range': CalendarRange,
    'calendar-single': CalendarSingle,
    'counter': Counter,
    'custom-radio': CustomRadio,
    'tiny-editor': TinyEditor,
    'accordeon': Accordeon,
    'tour-route': TourRoute,
    'switch': Switch,
    'chat': Chat,
    'select-calendar': SelectCalendar,
    'input-default': InputDefault,
    'scrollbar-compensator': ScrollbarCompensator,
    'connect-links': ConnectLinks,
    'simple-filter': SimpleFilter,
    'minicard-slider': MinicardSlider,
    'sortable-photos': SortablePhotos,
    'sh-password': ShPassword,
    'cropper': Cropper,
    'switch-menu': SwitchMenu,
    'chocolate-minicard': ChocolateMinicard,
    'creative-table': CreativeTable,
    'list-default': ListDefault,
    'text-counter': TextCounter,
    'chat-dialog': ChatDialog,
    'chat-mobile': ChatMobile,
    'filling-link': FillingLink,
    'select': Select,
    'select-checkbox': SelectCheckbox,
    'slider-range': SliderRange,
    'filter': Filter,
    'foldable-list': FoldableList,
    'mobile-filter-menu': MobileFilterMenu,
    'review': Review,
    'assessment': Assessment
};
