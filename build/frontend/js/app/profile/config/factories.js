'use strict';

import MaskFactory from 'js/components/mask/MaskFactory';

export default {
    'mask': MaskFactory,
};
