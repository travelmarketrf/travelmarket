'use strict';

import widgets from 'js/app/profile/config/widgets';
import modules from 'js/app/profile/config/modules';
import factories from 'js/app/profile/config/factories';

export default {
    factories: factories,
    widgets: widgets,
    modules: modules
};
