'use strict';

import MessagesModules from 'js/modules/pages/profile/MessagesModules';

export default {
    'messages': MessagesModules
};
