'use strict';

import BaseUI from 'js/components/BaseUI';
import DropdownManager from 'js/components/dropdown/DropdownManager';
import SelectManager from 'js/components/select/SelectManager';
import WidgetOverlayInterceptor from 'js/components/widgetOverlayInterceptor/WidgetOverlayInterceptor';
import PopupManager from 'js/components/managers/PopupManager';
import FormInputManager from 'js/components/form/FormInputManager';
import MapsManager from 'js/components/map/MapsManager';
import AddingFields from 'js/components/addingFields/AddingFields';
import MobileMainMenu from 'js/components/mobile/MobileMainMenu';
import ReviewPopup from 'js/components/review/ReviewPopup';
import FixableBlockManager from 'js/components/managers/FixableBlockManager';
import GalleryModal from 'js/components/gallery/GalleryModal';
import Resize from 'js/components/resize/Resize';
import FixableHeaderManager from 'js/components/managers/FixableHeaderManager';

class UI extends BaseUI {
    constructor(config) {
        super(config);

        this.dropdownManager = new DropdownManager();
        this.selectManager = new SelectManager();
        this.widgetOverlayInterceptor = new WidgetOverlayInterceptor();
        this.popupManager = new PopupManager();
        this.formInputManager = new FormInputManager();
        this.mapsManager = new MapsManager();
        this.addingFields = new AddingFields();
        this.mobileMenu = new MobileMainMenu();
        this.reviewPopup = new ReviewPopup();
        this.fixableBlockManager = new FixableBlockManager();
        this.galleryModal = new GalleryModal();
        this.resize = new Resize();
        this.fixableHeaderManager = new FixableHeaderManager();

        this.refresh();
    }

    refresh(container) {
        super.refresh(container);

        this.mapsManager.refresh();
    }
}

export default UI;
