'use strict';

import BaseApplication from 'js/BaseApplication';
import UI from 'js/app/profile/UI';
import PageModule from 'js/modules/PageModule';
import GlobalExports from 'js/helpers/GlobalExports';
import config from 'js/app/profile/config/app';

class Application extends BaseApplication {
    constructor() {
        super();

        this.ui = new UI({ widgets: config.widgets, factories: config.factories });
        this.pageModule = new PageModule(config.modules);

        // make instances available in Window object
        GlobalExports.add('uiManager', this.ui);
        GlobalExports.add('pageModule', this.pageModule.get());
        GlobalExports.export();
    }
}

export default (new Application()).run();
