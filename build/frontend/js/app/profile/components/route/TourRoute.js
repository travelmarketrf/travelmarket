'use strict';

import Widget from 'js/components/Widget';
import EventHelper from 'js/helpers/EventHelper';
import SelectDefault from 'js/components/select/SelectDefault';

class TourRoute {
    constructor(node) {
        this.node = node;

        if (!this.node) return;

        this.node['_component'] = this;

        this.inputsNode = this.node.querySelector('.input-grid');
        this.inputsContainer = this.node.querySelector('.input-grid__container');
        this.btnAdd = this.node.querySelector('.btn-simple');
        this.mapNode = this.node.querySelector('.route__map');
        this._map = null;

        this.counterId = 1;

        Widget.onReady([this.mapNode]).then(widgets => this._init(widgets));
    }

    _init(widgets) {
        this._map = widgets[0];

        const selects = this.getSelects();

        for (let i = 0; i < selects.length; i++) {
            const id = this.counterId++;
            const coordStr = selects[i].getInputValueByName('coord[]');
            const counter = i + 1;

            if (coordStr) {
                const [lat, lon] = coordStr.split(',');

                this._updateOrGenerateMarker(id, {lat: parseFloat(lat), lon: parseFloat(lon)}, counter.toString());
            }

            selects[i].setKey('id', id)
                      .setCounter(counter)
                      .setInputName('point[]');
        }

        this._map.fitMarkers();

        this._checkSelectCount();
        this._bindEvents();
    }

    _bindEvents() {
        this.onPointRemove = this.onPointRemove.bind(this);
        this.onPointAdd = this.onPointAdd.bind(this);
        this.onPointInput = this.onPointInput.bind(this);
        this.onPointChange = this.onPointChange.bind(this);
        this.onMarkerAdd = this.onMarkerAdd.bind(this);
        this.onMarkerMove = this.onMarkerMove.bind(this);

        this.inputsContainer.addEventListener('select-default:input', this.onPointInput, false);
        this.inputsContainer.addEventListener('select:remove', this.onPointRemove, false);
        this.inputsContainer.addEventListener('select-default:change', this.onPointChange, false);
        this.mapNode.addEventListener('map:click', this.onMarkerAdd, false);
        this.mapNode.addEventListener('map:marker-drag-end', this.onMarkerMove, false);
        this.btnAdd.addEventListener('click', this.onPointAdd, false);
    }

    onPointRemove(e) {
        e.preventDefault();

        const select = e.detail.component;
        const counter = select.getCounter();
        const id = select.getKey('id');

        [...this.inputsContainer.children][counter-1].remove();

        this._map.removeMarkerById(id);

        const selects = this.getSelects();

        for (let i = 0; i < selects.length; i++) {
            const counter = i + 1;
            const id = selects[i].getKey('id');

            this._map.setLabelTextByMarkerId(id, counter.toString());
            selects[i].setCounter(counter);
        }

        this._checkSelectCount();
    }

    onPointAdd(e) {
        const selectHTML = SelectDefault.createEmpty();
        const groupNode = document.createElement('div');
        const count = this.getSelectCount() + 1;

        groupNode.classList.add('form-group');
        groupNode.innerHTML = selectHTML;

        this.inputsContainer.appendChild(groupNode);

        EventHelper.refreshUI(null, this.inputsContainer)
                    .then(() => {
                        const select = Widget.find(groupNode, 'select-default')[0];

                        if (!select) return;

                        select.hideArrow()
                            .makeCountable()
                            .makeRemovable()
                            .makeEditable()
                            .disableAutoOpen()
                            .setInputName(`point[]`)
                            .setCounter(count)
                            .setKey('id', this.counterId++);

                        this._checkSelectCount();
                    });
    }

    onPointChange(e) {
        const select = e.detail.component;
        const placeId = select.getActiveItem().getAttribute('data-place-id');
        const id = select.getKey('id');

        this._map.getPlaceDetails(placeId, id)
                .then(coord => {
                    if (!coord) return;

                    this._updateOrGenerateMarker(id, coord, select.getCounter());
                    this._map.panToMarkerId(id);
                    select.setInputValueByName('coord[]', `${coord.lat},${coord.lon}`);
                });
    }

    _checkSelectCount() {
        if (this.getSelectCount() > 0) {
            this.inputsNode.classList.remove('input-grid_empty');
        } else {
            this.inputsNode.classList.add('input-grid_empty');
        }
    }

    _updateOrGenerateMarker(id, coord, label) {
        if (this._map.hasMarker(id)) {
            this._map.moveMarkerById(id, coord.lat, coord.lon);
        } else {
            this._map.addMarker({
                id: id,
                location: {
                    lat: coord.lat,
                    lon: coord.lon,
                },
                label: label
            });
        }
    }

    _convertToSelectDataFormat(data) {
        const items = [];

        for (let i = 0; i < data.length; i++) {
            items.push({
                name: data[i].description,
                value: `${data[i].description}`,
                data: {
                    'place-id': `${data[i].place_id}`
                }
            });
        }

        return {items};
    }

    onPointInput(e) {
        const select = e.detail.component;
        const value = select.getValue();
        const id = select.getKey('id');

        this._map.search(value, id)
                .then(data => {
                    const points = this._convertToSelectDataFormat(data);

                    select.load(points);
                    select.open();
                })
                .catch(err => {
                    if (err.message === 'ZERO_RESULTS') {
                        select.close();
                        return;
                    };

                    throw err;
                });
    }

    onMarkerAdd(e) {
        const select = this.getLastSelect();
        const id = select.getKey('id');
        const coord = e.detail.coord;

        if (!select) return;

        this._updateOrGenerateMarker(id, coord, select.getCounter());
        select.setInputValueByName('coord[]', `${coord.lat},${coord.lon}`);
    }

    onMarkerMove(e) {
        const select = this._getSelectById(e.detail.id);
        const coord = e.detail.coord;

        if (!select) return;

        select.setInputValueByName('coord[]', `${coord.lat},${coord.lon}`);
    }

    getSelectCount() {
        return this.inputsContainer.children.length;
    }

    getSelects() {
        return Widget.find(this.inputsContainer, 'select-default');
    }

    _getSelectById(id) {
        const selects = this.getSelects();

        for (let i = 0; i < selects.length; i++) {
            if (id === selects[i].getKey('id'))
                return selects[i];
        }

        return null;
    }

    getLastSelect() {
        if (!this.inputsContainer.lastElementChild)
            return null;

        return Widget.find(this.inputsContainer.lastElementChild, 'select-default')[0];
    }
}

export default TourRoute;
