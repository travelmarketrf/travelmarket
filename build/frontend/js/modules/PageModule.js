'use strict';

class PageModule {
    constructor(modules = {}) {
        this.module = null;
        this.modules = modules;

        this._init();
    }

    _init() {
        const moduleName = this.getName();

        if (this.modules[moduleName])
            this.module = new this.modules[moduleName];
    }

    getName() {
        const moduleNode = document.querySelector('[data-module]');
        return moduleNode ? moduleNode.getAttribute('data-module') : null;
    }

    get() {
        return this.module;
    }
}

export default PageModule;
