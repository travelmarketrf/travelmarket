'use strict';

import EventHelper from 'js/helpers/EventHelper';
import MapDisplay from 'js/components/mapDisplay/MapDisplay';
import TourFilter from 'js/components/filter/TourFilter';

class TourChooseModules {
    constructor() {
        this.mapDisplay = new MapDisplay();
        this.tourFilter = new TourFilter();
    }

    refresh() {
        EventHelper.refreshUI();
    }
}

export default TourChooseModules;
