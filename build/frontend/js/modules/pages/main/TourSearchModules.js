'use strict';

import EventHelper from 'js/helpers/EventHelper';

class TourSearchModules {
    constructor() {

    }

    refresh() {
        EventHelper.refreshUI();
    }
}

export default TourSearchModules;
