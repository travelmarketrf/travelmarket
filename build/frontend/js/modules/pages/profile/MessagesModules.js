'use strict';

import ScrollLocker from 'js/helpers/ScrollLocker';
import Chat from 'js/components/chat/Chat';

class MessagesModules {
    constructor() {
        ScrollLocker.lock();

        this.chat = new Chat();
    }
}

export default MessagesModules;
