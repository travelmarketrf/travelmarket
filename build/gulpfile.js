'use strict';

const gulp = require('gulp');
const debug = require('gulp-debug');
const basePath = '';

const isDev = !process.env.NODE_ENV || process.env.NODE_ENV == 'dev' || process.env.NODE_ENV == 'devBackend' || process.argv.includes('--dev');

function lazyRequireTask(taskName, pathName, options) {
    options = options || {};
    options.taskName = taskName;

    gulp.task(taskName, function(callback) {
        let task = require(pathName).call(this, options);

        return task(callback);
    });
}

lazyRequireTask('styles:main', './' + basePath + 'tasks/styles', {
        src: basePath + 'frontend/sass/main.scss',
        dest: basePath + 'public/css'
});

lazyRequireTask('styles:profile', './' + basePath + 'tasks/styles', {
        src: basePath + 'frontend/sass/profile.scss',
        dest: basePath + 'public/css'
});

gulp.task('styles', gulp.parallel('styles:main', 'styles:profile'));

lazyRequireTask('clean', './' + basePath + 'tasks/clean', {
    resources: [basePath + 'public', basePath + 'tmp', basePath + 'manifest',]
});

lazyRequireTask('assets', './' + basePath + 'tasks/assets', {
    src: basePath + 'frontend/assets/**/*',
    dest: basePath + 'public'
});

lazyRequireTask('tinymce:themes', './' + basePath + 'tasks/assets', {
    src: basePath + 'node_modules/tinymce/themes/**/*',
    dest: basePath + 'public/tiny/themes'
});

lazyRequireTask('tinymce:plugins', './' + basePath + 'tasks/assets', {
    src: basePath + 'node_modules/tinymce/plugins/**/*',
    dest: basePath + 'public/tiny/plugins'
});

lazyRequireTask('tinymce:skins', './' + basePath + 'tasks/assets', {
    src: basePath + 'node_modules/tinymce/skins/**/*',
    dest: basePath + 'public/tiny/skins/oxide'
});

gulp.task('tinymce', gulp.parallel('tinymce:themes', 'tinymce:skins', 'tinymce:plugins'));

lazyRequireTask('html', './' + basePath + 'tasks/html', {
    src: [basePath + 'frontend/html/profile/[^^_]*.html', basePath + 'frontend/html/[^^_]*.html'],
    dest: basePath + 'public'
});

lazyRequireTask('html:main', './' + basePath + 'tasks/html', {
    src: basePath + 'frontend/html/[^^_]*.html',
    dest: basePath + 'public/'
});

lazyRequireTask('html:profile', './' + basePath + 'tasks/html', {
    src: basePath + 'frontend/html/profile/**/[^^_]*.html',
    dest: basePath + 'public/profile'
});

// gulp.task('html', gulp.parallel('html:main', 'html:profile'));

lazyRequireTask('sprite', './' + basePath + 'tasks/sprite', {
    src: basePath + 'frontend/sass/**/*.png',
    destSprite: basePath + 'public/img',
    destScss: basePath + 'tmp/styles',
    name: 'sprite',
    cssPath: '/img/'
});

lazyRequireTask('sprite:rev', './' + basePath + 'tasks/sprite-rev', {
    destScss: basePath + 'tmp/styles',
    name: 'sprite',
});

lazyRequireTask('webpack', './' + basePath + 'tasks/webpack', {
    dirname: __dirname
});

lazyRequireTask('build:deploy', './' + basePath + 'tasks/build-deploy', {
    src: basePath + 'public/**/*',
    dest: '../public_html',
    destManifestFolder: '../'
});

lazyRequireTask('clean:deployed', './' + basePath + 'tasks/clean', {
    resources: [
        '../manifest',
        '../public_html/js/*.js',
        '../public_html/css/combined*.css',
        '../public_html/css/maps',
        '../public_html/img',
        '../public_html/fonts'
    ],
    delOptions: {
        force: true
    }
});

lazyRequireTask('serve', './' + basePath + 'tasks/serve', {
    bsOptions: {
        port: 3017,
        server: {
            baseDir: basePath + 'public'
        },
        watchOptions: {
            awaitWriteFinish : true
        }
    },
    path: basePath + 'public/**/*'
});

gulp.task('build', gulp.series(
    'clean', 'sprite', 'sprite:rev',
    gulp.parallel('styles', 'assets', 'tinymce', 'webpack'),
    'html')
);

gulp.task('build:main', gulp.series(
    'clean',
    gulp.parallel('styles', 'assets', 'tinymce', 'webpack'),
    gulp.parallel('html:main', 'html:profile'))
);

gulp.task('build:profile', gulp.series(
    'clean',
    gulp.parallel('styles', 'assets', 'tinymce', 'webpack'),
    gulp.parallel('html:main', 'html:profile'))
);

gulp.task('watch:main', function() {
    gulp.watch(basePath + 'frontend/sass/**/*.scss', gulp.series('styles:main'));
    gulp.watch(basePath + 'frontend/assets/**/*', gulp.series('assets'));
    gulp.watch(basePath + 'frontend/html/**/*.html', gulp.series('html:main'));
});

gulp.task('watch:profile', function() {
    gulp.watch(basePath + 'frontend/sass/**/*.scss', gulp.series('styles:profile'));
    gulp.watch(basePath + 'frontend/assets/**/*', gulp.series('assets'));
    gulp.watch(basePath + 'frontend/html/**/*.html', gulp.series('html:profile'));
});

gulp.task('watch', function() {
    if (process.env.NODE_ENV != 'devBackend') {
        gulp.watch(basePath + 'frontend/sass/**/*.scss', gulp.series('styles'));
        gulp.watch(basePath + 'frontend/sass/**/*.png', gulp.series('sprite'));
        gulp.watch(basePath + 'frontend/assets/**/*', gulp.series('assets'));
        gulp.watch(basePath + 'frontend/html/**/*.html', gulp.series('html'));
    } else {
        gulp.watch(basePath + 'frontend/sass/**/*.scss', gulp.series('styles', 'build:deploy'));
        gulp.watch(basePath + 'frontend/sass/**/*.png', gulp.series('sprite', 'build:deploy'));
        gulp.watch(basePath + 'frontend/assets/**/*', gulp.series('assets', 'build:deploy'));
        gulp.watch(basePath + 'public/**/*.js', gulp.series('build:deploy'));
    }
});

gulp.task('dev:main',
    gulp.series('build:main', gulp.parallel('watch:main', 'serve'))
);

gulp.task('dev:profile',
    gulp.series('build:profile', gulp.parallel('watch:profile', 'serve'))
);

gulp.task('dev',
    gulp.series('build'/*, gulp.parallel('watch', 'serve')*/)
);

gulp.task('deploy',
    gulp.series('clean:deployed', 'build', 'build:deploy')
);

gulp.task('continuous',
    gulp.series('clean:deployed', 'build', 'build:deploy', 'watch')
);
