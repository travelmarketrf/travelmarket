'use strict';

const gulp = require('gulp');
const gp = require('gulp-load-plugins')();

const isDev = !process.env.NODE_ENV || process.env.NODE_ENV == 'dev' || process.env.NODE_ENV == 'devBackend';

module.exports = function(options) {

    return function() {
        const stream = gulp.src(options.src)
            .pipe(gp.fileInclude({
                prefix: '@@',
                basepath: 'frontend/html/'
            }))
            .pipe(gp.if(!isDev, gp.revReplace({
                manifest: gulp.src('manifest/css.json', {allowEmpty: true})
            })))
            .pipe(gulp.dest(options.dest));

        return stream;
    };

};