'use strict';

const gulp = require('gulp');
const gp = require('gulp-load-plugins')();
const buffer = require('vinyl-buffer');
const merge = require('merge-stream');
const spritesmith = require('gulp.spritesmith');

const isDev = !process.env.NODE_ENV || process.env.NODE_ENV == 'dev' || process.env.NODE_ENV == 'devBackend';

module.exports = function(options) {

    return function() {
        let imgName = options.name + '.png';

        let spriteData = gulp.src(options.src).pipe(spritesmith({
            imgName: imgName,
            cssName: options.name + '.scss',
            imgPath: options.cssPath + imgName,
            padding: 2
        }));

        let imgStream = spriteData.img
            .pipe(buffer())
            .pipe(gp.if(!isDev, gp.rev()))
            .pipe(gulp.dest(options.destSprite))
            .pipe(gp.if(!isDev, gp.rev.manifest('sprite.json')))
            .pipe(gp.if(!isDev, gulp.dest('manifest')));

        let scssStream = spriteData.css
            .pipe(gulp.dest(options.destScss));

        // Return a merged stream to handle both `end` events 
        return merge(imgStream, scssStream);
    };

};