'use strict';

const gulp = require('gulp');
const gp = require('gulp-load-plugins')();

const isDev = !process.env.NODE_ENV || process.env.NODE_ENV == 'dev' || process.env.NODE_ENV == 'devBackend';

module.exports = function(options) {

    return function() {
        return gulp.src(options.src, {since: gulp.lastRun(options.taskName)})
            .pipe(gp.newer(options.dest))
            .pipe(gulp.dest(options.dest));
    };

};