'use strict';

const gulp = require('gulp');
const gp = require('gulp-load-plugins')();

const isDev = !process.env.NODE_ENV || process.env.NODE_ENV == 'dev' || process.env.NODE_ENV == 'devBackend';

module.exports = function(options) {

    return function() {
        return gulp.src(options.src)
            .pipe(gp.if(isDev, gp.sourcemaps.init()))
            .pipe(gp.sass({
                includePaths: ['tmp/styles/']
            }))
            .on('error', gp.notify.onError(function(err) {
                return {
                    title: options.taskName,
                    message: err.message
                };
            }))
            .pipe(gp.autoprefixer({
                browsers: ['last 2 versions', 'ie >= 11'],
                grid: 'autoplace'
            }))
            .pipe(gp.if(isDev, gp.sourcemaps.write('./maps')))
            .pipe(gp.if(!isDev, gp.cssnano(options.cssnano)))
            .pipe(gp.if(!isDev, gp.rev()))
            .pipe(gulp.dest(options.dest))
            .pipe(gp.if(!isDev, gp.rev.manifest('css.json')))
            .pipe(gp.if(!isDev, gulp.dest('manifest')));
    };

};
