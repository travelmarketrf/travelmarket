'use strict';

const gulp = require('gulp');
const gp = require('gulp-load-plugins')();

const isDev = !process.env.NODE_ENV || process.env.NODE_ENV == 'dev' || process.env.NODE_ENV == 'devBackend';

module.exports = function(options) {

    return function(callback) {
        if (isDev) {
            callback();
            return;
        }

        return gulp.src(options.destScss + '/*.scss')
            .pipe(gp.revReplace({
                manifest: gulp.src('manifest/' + options.name + '.json', {allowEmpty: true}),
                replaceInExtensions: ['.scss']
            }))
            .pipe(gulp.dest(options.destScss));
    };
};