'use strict';

const gulp = require('gulp');
const del = require('del');

const isDev = !process.env.NODE_ENV || process.env.NODE_ENV == 'dev' || process.env.NODE_ENV == 'devBackend';

module.exports = function(options) {
    if (options.config && isDev) {
        options.config.webpackWatch = false;
    }

    return function(callback) {
        let sourcesCount = options.resources.length;
        let deletedSourcesCount = 0;

        options.resources.forEach(function(resource) {
            del(resource, options.delOptions)
            .then(paths => {
                deletedSourcesCount++;

                if (sourcesCount == deletedSourcesCount)
                    callback();
            });
        });
    };
};
