'use strict';

const gulp = require('gulp');

const isDev = !process.env.NODE_ENV || process.env.NODE_ENV == 'dev' || process.env.NODE_ENV == 'devBackend';

module.exports = function(options) {

    return function() {
        gulp.src('manifest/*', {allowEmpty: true})
            .pipe(gulp.dest(options.destManifestFolder + 'manifest'));

        return gulp.src([options.src, '!/**/*.html'], {allowEmpty: true})
                .pipe(gulp.dest(options.dest));
    }
};