'use strict';

const gulp = require('gulp');
const browserSync = require('browser-sync').create();

module.exports = function(options) {

    return function() {
        browserSync.init(options.bsOptions);

        browserSync.watch(options.path).on('change', browserSync.reload);
    };

};