'use strict';

const gulp = require('gulp');
const webpack = require('webpack');
const path = require('path');
const notifier = require('node-notifier');
const gulplog = require('gulplog');
const AssetsPlugin = require('assets-webpack-plugin');
const TerserPlugin = require('terser-webpack-plugin');
const fs = require('fs');

const isDev = !process.env.NODE_ENV || process.env.NODE_ENV == 'dev' || process.env.NODE_ENV == 'devBackend';

module.exports = function(opts) {

    const options = {
        // context: path.resolve(opts.dirname, 'frontend/js'),

        profile: true,

        entry: {
            'app-main': './frontend/js/app/main/Application',
            'app-profile': './frontend/js/app/profile/Application'
        },

        output: {
            path: path.resolve(opts.dirname, 'public/js'),
            publicPath: '/js/',
            filename: isDev ? '[name].js' : '[name]-[chunkhash:10].js'
            // add library to global scope
            // library: '[name]'
        },

        watch: isDev,

        optimization: {
            minimizer: [
                new TerserPlugin({
                    cache: true,
                    parallel: true,
                    sourceMap: true, // Must be set to true if using source-maps in production
                    terserOptions: {
                        // https://github.com/webpack-contrib/terser-webpack-plugin#terseroptions
                    }
                }),
            ],
        },

        mode: isDev ? 'development' : 'production',

        devtool: isDev ? 'source-map' : false,

        plugins: [
            new webpack.NoEmitOnErrorsPlugin(),
            // new webpack.ProvidePlugin({
            //    "$":"jquery",
            //    "jQuery":"jquery",
            //    "window.jQuery":"jquery"
            // })
        ],

        resolve: {
            extensions: ['.js', '.pug', '.json'],
            alias: {
                js: path.resolve(__dirname, '../frontend/js'),
                'jquery': 'jquery'
            }
        },

        module: {

            rules: [{
                    test: /\.js$/,
                    loader: 'babel-loader',
                    query: {
                        plugins: [
                            ['@babel/plugin-syntax-dynamic-import']
                        ],
                        presets: [
                            ['@babel/preset-env']
                        ]
                    }
                },
                {
                    test: /\.pug$/,
                    include: path.resolve(opts.dirname, 'frontend/js'),
                    loader: 'pug-loader'
                }
            ]
        }
    };

    if (!isDev) {
        options.plugins.push(
            new AssetsPlugin({
                filename: 'webpack.json',
                path: path.resolve(opts.dirname, 'manifest'),
                processOutput(assets) {
                    for (let key in assets) {
                        assets[key + '.js'] = assets[key].js.slice(options.output.publicPath.length);
                        delete assets[key];
                    }

                    return JSON.stringify(assets);
                }
            })
        );
    }

    return function(callback) {
        webpack(options, function(err, stats) {
            fs.writeFileSync(path.join(__dirname, "../stats.json"), JSON.stringify(stats.toJson()));

            if (!err) { // if no hard error
                // then try to get soft error
                err = stats.toJson().errors[0];
            }

            if (err) {
                notifier.notify({
                    title: 'webpack',
                    message: err
                });

                gulplog.error(err);
            } else {
                gulplog.info(stats.toString({
                    colors: true
                }));
            }

            if (!options.watch && err) {
                callback(err);
            } else {
                callback();
            }
        });
    };
}
